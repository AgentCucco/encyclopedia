-- Packs the data needed to create a Sprite into a table formatted for Encyclopedia.
-- This function used to return a Sprite, but now Sprite creation is more limited to save memory.
function Encyclopedia.RegisterSprite(gfxroot, anmtoplay, anmframe, newspr, layer, returnObject_DEPRECATED)
	return {Anm2Root = gfxroot, SprRoot = newspr, SprLayer = layer, AnmName = anmtoplay, AnmFrame = anmframe}
end

-- Loads the anm2/spritesheet/animation from an Encyclopedia sprite data table into a given Sprite object.
function Encyclopedia.LoadSprite(sprite, spriteData)
	local gfxroot = tostring(spriteData.Anm2Root)
	
	if sprite:GetFilename() ~= gfxroot then
		sprite:Load(gfxroot, true)
	end
	
	local anmframe = spriteData.AnmFrame
	local anmtoplay = spriteData.AnmName and tostring(spriteData.AnmName) or sprite:GetDefaultAnimationName()
	local newspr = spriteData.SprRoot
	local layer = spriteData.SprLayer
	
	if anmframe then
		sprite:SetFrame(anmtoplay, anmframe)
		sprite:Stop()
	else
		sprite:Play(anmtoplay, true)
	end
	
	if newspr then
		if layer == true then
			for i = 0, sprite:GetLayerCount() - 1 do
				sprite:ReplaceSpritesheet(i, newspr)
			end
		else
			sprite:ReplaceSpritesheet(layer or 0, newspr)
		end
		sprite:LoadGraphics()
	end
end

-- Returns a Sprite object. Works like the old RegisterSprite function.
function Encyclopedia.CreateSprite(gfxroot, anmtoplay, anmframe, newspr, layer)
	local SpriteData = Encyclopedia.RegisterSprite(gfxroot, anmtoplay, anmframe, newspr, layer)
	local NewSprite = Sprite()
	Encyclopedia.LoadSprite(NewSprite, SpriteData)
	return NewSprite
end

-- Gets a specific Sprite from the cache.
-- If the current Sprite in the cache does not match the provided spriteData, it will be reloaded using that data.
-- Primarily used to keep only 16 Sprite objects in the scrolling menus, but can be used for other stuff
-- as long as the numeric keys are only used for one purpose at any given time.
local SpriteCache = {}
function Encyclopedia.GetCachedSprite(key, spriteData)
	if type(spriteData) == "userdata" then
		-- Probably got passed an existing sprite object. Just use it.
		return spriteData
	end
	
	if not SpriteCache[key] then
		SpriteCache[key] = {}
	end
	
	local tab = SpriteCache[key]
	
	if not tab.Spr then
		tab.Spr = Sprite()
	end
	
	local sprite = tab.Spr
	
	local anmtoplay = spriteData.AnmName and tostring(spriteData.AnmName) or sprite:GetDefaultAnimationName()
	local anmframe = spriteData.AnmFrame
	local allowanimation = (anmframe == nil)
	
	if sprite:GetFilename() ~= spriteData.Anm2Root or tab.Png ~= spriteData.SprRoot then
		Encyclopedia.LoadSprite(sprite, spriteData)
		tab.Png = spriteData.SprRoot
	elseif not allowanimation then
		sprite:SetFrame(anmtoplay, anmframe)
		sprite:Stop()
	elseif not sprite:IsPlaying(anmtoplay) then
		sprite:Play(anmtoplay, true)
	end
	
	return sprite
end

local ItemConfig = Isaac.GetItemConfig()
function Encyclopedia.GetMaxCollectibleID()
    local id = CollectibleType.NUM_COLLECTIBLES - 1
    local step = 16
    while step > 0 do
        if ItemConfig:GetCollectible(id+step) ~= nil then
            id = id + step
        else
            step = step // 2
        end
    end
    
    return id
end

function Encyclopedia.GetMaxTrinketID()
    local id = TrinketType.NUM_TRINKETS - 1
    local step = 16
    while step > 0 do
        if ItemConfig:GetTrinket(id + step) ~= nil then
            id = id + step
        else
            step = step // 2
        end
    end
    
    return id
end

function Encyclopedia.GetMaxCardID()
    local id = Card.NUM_CARDS - 1
    local step = 16
    while step > 0 do
        if ItemConfig:GetCard(id + step) ~= nil then
            id = id + step
        else
            step = step // 2
        end
    end
    
    return id
end

function Encyclopedia.GetMaxPillID()
    local id = PillEffect.NUM_PILL_EFFECTS - 1
    local step = 16
    while step > 0 do
        if ItemConfig:GetPillEffect(id + step) ~= nil then
            id = id + step
        else
            step = step // 2
        end
    end
    
    return id
end

function Encyclopedia.pairsByKeys(t, f)
	local a = {}
	for n in pairs(t) do table.insert(a, n) end
	table.sort(a, f)
	local i = 0 
	return function()
		i = i + 1
		if a[i] == nil then return nil
		else return a[i], t[a[i]]
		end
	end
end
