local dssmod = RegisterMod("Dead Sea Scrolls (Encyclopedia)", 1)
local game = Game()
local sfx = SFXManager()
local dssmenu = DeadSeaScrollsMenu

local mfdat = {}
do

mfdat['a'] = { 0, 4, 7, 11 };
mfdat['b'] = { 1, 4, 8, 12 };
mfdat['c'] = { 2, 4, 7, 10 };
mfdat['d'] = { 3, 4, 8, 12 };
mfdat['e'] = { 4, 4, 7, 10 };
mfdat['f'] = { 5, 4, 6, 9 };
mfdat['g'] = { 6, 5, 8, 12 };
mfdat['h'] = { 7, 4, 8, 11 };
mfdat['i'] = { 8, 1, 3, 4 };
mfdat['j'] = { 9, 4, 7, 11 };
mfdat['k'] = { 10, 4, 6, 9 };
mfdat['l'] = { 11, 4, 8, 10 };
mfdat['m'] = { 12, 5, 8, 13 };
mfdat['n'] = { 13, 4, 8, 10 };
mfdat['o'] = { 14, 5, 10, 12 };
mfdat['p'] = { 15, 4, 7, 10 };
mfdat['q'] = { 16, 5, 9, 13 };
mfdat['r'] = { 17, 4, 7, 10 };
mfdat['s'] = { 18, 4, 6, 10 };
mfdat['t'] = { 19, 4, 7, 10 };
mfdat['u'] = { 20, 4, 7, 13 };
mfdat['v'] = { 21, 5, 8, 13 };
mfdat['w'] = { 22, 5, 11, 16 };
mfdat['x'] = { 23, 4, 6, 12 };
mfdat['y'] = { 24, 4, 7, 10 };
mfdat['z'] = { 25, 4, 6, 9 };
mfdat['A'] = { 0, 4, 7, 11 };
mfdat['B'] = { 1, 4, 8, 12 };
mfdat['C'] = { 2, 4, 7, 10 };
mfdat['D'] = { 3, 4, 8, 12 };
mfdat['E'] = { 4, 4, 7, 10 };
mfdat['F'] = { 5, 4, 6, 9 };
mfdat['G'] = { 6, 5, 8, 12 };
mfdat['H'] = { 7, 4, 8, 11 };
mfdat['I'] = { 8, 1, 3, 4 };
mfdat['J'] = { 9, 4, 7, 11 };
mfdat['K'] = { 10, 4, 6, 9 };
mfdat['L'] = { 11, 4, 8, 10 };
mfdat['M'] = { 12, 5, 8, 13 };
mfdat['N'] = { 13, 4, 8, 10 };
mfdat['O'] = { 14, 5, 10, 12 };
mfdat['P'] = { 15, 4, 7, 10 };
mfdat['Q'] = { 16, 5, 9, 13 };
mfdat['R'] = { 17, 4, 7, 10 };
mfdat['S'] = { 18, 4, 6, 10 };
mfdat['T'] = { 19, 4, 7, 10 };
mfdat['U'] = { 20, 4, 7, 13 };
mfdat['V'] = { 21, 5, 8, 13 };
mfdat['W'] = { 22, 5, 11, 16 };
mfdat['X'] = { 23, 4, 6, 12 };
mfdat['Y'] = { 24, 4, 7, 10 };
mfdat['Z'] = { 25, 4, 6, 9 };
mfdat['0'] = { 26, 4, 8, 12 };
mfdat['1'] = { 27, 4, 8, 10 };
mfdat['2'] = { 28, 4, 8, 10 };
mfdat['3'] = { 29, 4, 8, 10 };
mfdat['4'] = { 30, 4, 7, 10 };
mfdat['5'] = { 31, 4, 8, 9 };
mfdat['6'] = { 32, 4, 8, 10 };
mfdat['7'] = { 33, 4, 8, 10 };
mfdat['8'] = { 34, 4, 8, 9 };
mfdat['9'] = { 35, 4, 8, 9 };
mfdat["'"] = { 36, 1, 2, 3 };
mfdat["’"] = { 36, 1, 2, 3 };
mfdat['"'] = { 37, 3, 4, 5 };
mfdat[':'] = { 38, 1, 3, 4 };
mfdat['/'] = { 39, 3, 6, 8 };
mfdat['.'] = { 40, 1, 2, 4 };
mfdat[','] = { 41, 2, 3, 4 };
mfdat['!'] = { 42, 2, 4, 6 };
mfdat['?'] = { 43, 3, 6, 8 };
mfdat['['] = { 44, 2, 4, 6 };
mfdat[']'] = { 45, 2, 4, 6 };
mfdat['('] = { 44, 2, 4, 6 };
mfdat[')'] = { 45, 2, 4, 6 };
mfdat['$'] = { 46, 4, 6, 8 };
mfdat["¢"] = { 47, 5, 6, 8 };
mfdat['+'] = { 48, 5, 6, 8 };
mfdat['-'] = { 49, 4, 6, 10 };
mfdat["×"] = { 50, 5, 6, 8 };
mfdat["÷"] = { 51, 5, 6, 8 };
mfdat['%'] = { 52, 4, 6, 8 };
mfdat['_'] = { 54, 2, 4, 5 };
mfdat[' '] = { 54, 4, 6, 8 };
mfdat['='] = { 53, 5, 8, 12 };
mfdat['^'] = { 55, 3, 4, 5 };
mfdat['<'] = { 56, 5, 7, 10 };
mfdat['>'] = { 57, 5, 7, 10 };

end

Encyclopedia.mfdat = mfdat

local menusounds = {
  Pop2 = {Sound = Isaac.GetSoundIdByName("deadseascrolls_pop"), PitchVariance = .1},
  Pop3 = {Sound = Isaac.GetSoundIdByName("deadseascrolls_pop"), Pitch = .8, PitchVariance = .1},
  Open = {Sound = Isaac.GetSoundIdByName("deadseascrolls_whoosh"), Volume = .5, PitchVariance = .1},
  Close = {Sound = Isaac.GetSoundIdByName("deadseascrolls_whoosh"), Volume = .5, Pitch = .8, PitchVariance = .1}
}

local MenuProvider

local PlaySound
PlaySound = function(...) -- A simpler method to play sounds, allows ordered or paired tables.
    local args = {...}

    for i = 1, 6 do -- table.remove won't work to move values down if values inbetween are nil
        if args[i] == nil then
            args[i] = -1111
        else
            allNil = false
        end
    end

    local npc, tbl

    if type(args[1]) == "userdata" and args[1].Type then
        npc = args[1]:ToNPC()
        table.remove(args, 1)
    end

    if type(args[1]) == "table" then
        tbl = args[1]
        table.remove(args, 1)
        if type(tbl[1]) == "table" then
            for _, sound in ipairs(tbl) do
                if npc then
                    PlaySound(npc, sound)
                else
                    PlaySound(sound)
                end
            end

            return
        end
    elseif args[1] == -1111 then
        return
    end

    local soundArgs = {}
    for i, v in ipairs(args) do
        if v == -1111 then
            args[i] = nil
        end

        soundArgs[i] = args[i]
    end

    if tbl then
        if #tbl > 0 then
            soundArgs = tbl
        else
            soundArgs = {tbl.Sound, tbl.Volume, tbl.Delay, tbl.Loop, tbl.Pitch}
        end

        -- If there are any remaining args after npc and table are removed, they override volume, delay, loop, and pitch
        for i = 1, 4 do
            if args[i] ~= nil then
                soundArgs[i + 1] = args[i]
            end
        end
    end

    soundArgs[2] = soundArgs[2] or 1
    soundArgs[3] = soundArgs[3] or 0
    soundArgs[4] = soundArgs[4] or false
    soundArgs[5] = soundArgs[5] or 1

    if tbl and tbl.PitchVariance then
        local variance = math.random()
        if tbl.NegativeVariance then
            variance = variance - 0.5
        end

        soundArgs[5] = soundArgs[5] + variance * tbl.PitchVariance
    end

    if npc then
        npc:PlaySound(table.unpack(soundArgs))
    else
        sfx:Play(table.unpack(soundArgs))
    end
end

local MenuProvider

local function getScreenBottomRight()
    return game:GetRoom():GetRenderSurfaceTopLeft() * 2 + Vector(442,286)
end

local function getScreenCenterPosition()
    return getScreenBottomRight() / 2
end

local function approach(aa, bb, cc)
	cc = cc or 1
	if bb > aa then
		return math.min(aa + cc, bb)
	elseif bb < aa then
		return math.max(aa - cc, bb)
	else
		return bb
	end
end

local function Lerp(aa, bb, cc)
    return (aa + (bb - aa) * cc)
end

local function IsIn(tbl, val)
    for _, v in ipairs(tbl) do
        if v == val then
            return true
        end
    end

    return false
end

local function KeysShareVals(tbl1, tbl2)
    if not tbl1 or not tbl2 then
        return false
    end

    for k, v in pairs(tbl1) do
        if tbl2[k] ~= v then
            return false
        end
    end

    for k, v in pairs(tbl2) do
        if tbl1[k] ~= v then
            return false
        end
    end

    return true
end

local function percentify(num, multi, max, wholeOnly)
    num = math.floor(num * multi)
    local str = tostring(num)
    if num >= 10000 then
        return max
    elseif num >= 1000 then
        str = string.sub(str, 1, 2) .. "." .. string.sub(str, 3, 4)
    elseif num >= 100 then
        str = "0" .. string.sub(str, 1, 1) .. "." .. string.sub(str, 2, 3)
    elseif num >= 10 then
        str = "00." .. str
    else
        str = "00.0" .. str
    end

    if wholeOnly then
        str = string.sub(str, 1, 2)
        if string.sub(str, 1, 1) == "0" then
            str = string.sub(str, 2, 2)
        end
    end

    str = str .. "%"
    return str
end

local function roundOff(num, multiplier)
    multiplier = multiplier or 100
    return math.floor(num * multiplier) / multiplier
end

local function SafeKeyboardTriggered(key, controllerIndex)
    return Input.IsButtonTriggered(key, controllerIndex) and not Input.IsButtonTriggered(key % 32, controllerIndex)
end

local function AnyKeyboardTriggered(key, controllerIndex)
    if SafeKeyboardTriggered(key, controllerIndex) then
        return true
    end

    for i = 0, 4 do
        if SafeKeyboardTriggered(key, i) then
            return true
        end
    end

    return false
end

local function SafeKeyboardPressed(key, controllerIndex)
    return Input.IsButtonPressed(key, controllerIndex) and not Input.IsButtonPressed(key % 32, controllerIndex)
end

local inputButtonNames = {
    [-1] = "none",

    [0] = "dpad left",
    [1] = "dpad right",
    [2] = "dpad up",
    [3] = "dpad down",
    [4] = "action down",
    [5] = "action right",
    [6] = "action left",
    [7] = "action up",
    [8] = "left bumper",
    [9] = "left trigger",
    [10] = "left stick",
    [11] = "right bumper",
    [12] = "right trigger",
    [13] = "right stick",
    [14] = "select",
    [15] = "start",

    [Keyboard.KEY_KP_0] = "numpad 0",
    [Keyboard.KEY_KP_1] = "numpad 1",
    [Keyboard.KEY_KP_2] = "numpad 2",
    [Keyboard.KEY_KP_3] = "numpad 3",
    [Keyboard.KEY_KP_4] = "numpad 4",
    [Keyboard.KEY_KP_5] = "numpad 5",
    [Keyboard.KEY_KP_6] = "numpad 6",
    [Keyboard.KEY_KP_7] = "numpad 7",
    [Keyboard.KEY_KP_8] = "numpad 8",
    [Keyboard.KEY_KP_9] = "numpad 9",
    [Keyboard.KEY_KP_DECIMAL] = "numpad decimal",
    [Keyboard.KEY_KP_DIVIDE] = "numpad divide",
    [Keyboard.KEY_KP_MULTIPLY] = "numpad multiply",
    [Keyboard.KEY_KP_SUBTRACT] = "numpad subtract",
    [Keyboard.KEY_KP_ADD] = "numpad add",
    [Keyboard.KEY_KP_ENTER] = "numpad enter",
    [Keyboard.KEY_KP_EQUAL] = "numpad equal",
}

for k, v in pairs(Keyboard) do
    if not inputButtonNames[v] then
        local name = string.sub(k, 5)
        name = name:gsub("_", " ")
        name = name:lower()
        inputButtonNames[v] = name
    end
end

local function GetInputtedButtons(controllerIndex, press)
    local func = Input.IsButtonTriggered
    if press then
        func = Input.IsButtonPressed
    end

    local inputs = {}
    for i = 0, 15 do
        if func(i, controllerIndex) then
            inputs[#inputs + 1] = i
        end
    end

    for name, key in pairs(Keyboard) do
        if func(key, controllerIndex) and not func(key % 32, controllerIndex) then
            inputs[#inputs + 1] = key
        end
    end

    return inputs
end

local menuinput
local function InitializeInput()
    if not menuinput then
        menuinput = {
            raw = {
                up = -100, down = -100, left = -100, right = -100,
            },
            menu = {
                up = false, down = false, left = false, right = false,
                toggle = false, confirm = false, back = false, keybinding = false
            },
            reading = false,
        }
    end
end

function dssmod.getInput(pnum)
    local player = Isaac.GetPlayer(pnum)

    InitializeInput()

    local input = menuinput
    local indx = player.ControllerIndex

    local raw = input.raw
    local menu = input.menu
    if not game:IsPaused() then
        local moveinput = player:GetMovementInput()
        local moveinputang = moveinput:GetAngleDegrees()
        local digitalmovedir = math.floor(4 + (moveinputang + 45) / 90) % 4
        local movelen = moveinput:Length()

        if (movelen > .3 and digitalmovedir == 0) or
        SafeKeyboardPressed(Keyboard.KEY_RIGHT, indx) or SafeKeyboardPressed(Keyboard.KEY_D, indx) then
            raw.right = math.max(raw.right, 0) + 1
        else
            raw.right = math.max(-100, math.min(1, raw.right) - 1)
        end

        if (movelen > .3 and digitalmovedir) == 1 or
        SafeKeyboardPressed(Keyboard.KEY_DOWN, indx) or SafeKeyboardPressed(Keyboard.KEY_S, indx) then
            raw.down = math.max(raw.down, 0) + 1
        else
            raw.down = math.max(-100, math.min(1, raw.down) - 1)
        end
        if (movelen > .3 and digitalmovedir) == 2 or
        SafeKeyboardPressed(Keyboard.KEY_LEFT, indx) or SafeKeyboardPressed(Keyboard.KEY_A, indx) then
            raw.left = math.max(raw.left, 0) + 1
        else
            raw.left = math.max(-100, math.min(1, raw.left) - 1)
        end
        if (movelen > .3 and digitalmovedir) == 3 or
        SafeKeyboardPressed(Keyboard.KEY_UP, indx) or SafeKeyboardPressed(Keyboard.KEY_W, indx) then
            raw.up = math.max(raw.up, 0) + 1
        else
            raw.up = math.max(-100, math.min(1, raw.up) - 1)
        end

        local dssmenu = DeadSeaScrollsMenu
        local ctog = dssmenu.GetGamepadToggleSetting() or 1

        local baseKey, safeKey = dssmenu.GetMenuKeybindSetting(), Keyboard.KEY_F1

        --toggle
        menu.toggle = AnyKeyboardTriggered(safeKey, indx) or
                      (ctog <= 2 and Input.IsButtonTriggered(10, indx)) or
                      ((ctog == 1 or ctog == 3) and Input.IsButtonTriggered(13, indx)) or
                      (ctog == 4 and Input.IsButtonTriggered(10, indx) and Input.IsButtonPressed(13, indx)) or
                      (ctog == 4 and Input.IsButtonTriggered(13, indx) and Input.IsButtonPressed(10, indx)) or
                      (ctog == 5 and Input.IsButtonTriggered(14, indx)) or
                      (ctog == 6 and Input.IsButtonPressed(12, indx) and Input.IsButtonTriggered(14, indx))

        if baseKey ~= -1 then
            menu.toggle = menu.toggle or AnyKeyboardTriggered(baseKey, indx)
        end

        --confirm
        menu.confirm = SafeKeyboardTriggered(Keyboard.KEY_ENTER, indx) or
                       SafeKeyboardTriggered(Keyboard.KEY_SPACE, indx) or
                       SafeKeyboardTriggered(Keyboard.KEY_E, indx) or
                       Input.IsButtonTriggered(4, indx) or
                       Input.IsButtonTriggered(7, indx)

        --back
        menu.back = SafeKeyboardTriggered(Keyboard.KEY_BACKSPACE, indx) or
                    SafeKeyboardTriggered(Keyboard.KEY_Q, indx) or
                    Input.IsButtonTriggered(5, indx) or
                    Input.IsButtonTriggered(6, indx)

        --directions
        menu.up = raw.up == 1 or (raw.up >= 18 and raw.up % 6 == 0)
        menu.down = raw.down == 1 or (raw.down >= 18 and raw.down % 6 == 0)
        menu.left = raw.left == 1 or (raw.left >= 18 and raw.left % 6 == 0)
        menu.right = raw.right == 1 or (raw.right >= 18 and raw.right % 6 == 0)

        menu.keybind = nil
        if menu.keybinding then
            menu.toggle = false
            menu.confirm = false
            menu.back = false
            menu.up = false
            menu.down = false
            menu.left = false
            menu.right = false
            local buttons = GetInputtedButtons(indx)
            if #buttons == 1 then
                menu.keybind = buttons[1]
            end
        end
    end
end

function dssmod.setOption(variable, setting, button, item, directorykey)
	if setting then
        if variable then
            directorykey.Settings[variable] = setting
            directorykey.SettingsChanged = true
        end

        if button and button.changefunc then
            button.changefunc(button, item)
        end
	end
end

local function hsvToRgb(h, s, v, a) --credit EmmanuelOga
  local r, g, b

  local i = math.floor(h * 6);
  local f = h * 6 - i;
  local p = v * (1 - s);
  local q = v * (1 - f * s);
  local t = v * (1 - (1 - f) * s);

  i = i % 6

  if i == 0 then r, g, b = v, t, p
  elseif i == 1 then r, g, b = q, v, p
  elseif i == 2 then r, g, b = p, v, t
  elseif i == 3 then r, g, b = p, q, v
  elseif i == 4 then r, g, b = t, p, v
  elseif i == 5 then r, g, b = v, p, q
  end

  return Color(r, g, b, a, 0, 0, 0)
end

local function bselToXY(bsel, gridx, buttons)
    local x, y = 1, 1
    local bselX, bselY
    local maxX = {}
    for i, button in ipairs(buttons) do
        if i == bsel then
            bselX, bselY = x, y
        end

        if i == #buttons then
            maxX[y] = x
            return bselX, bselY, maxX, y
        end

        local prevX = x
        x = x + (button.fullrow and gridx or 1)
        if x > gridx then
            maxX[y] = prevX
            y = y + 1
            x = 1
        end
    end
end

local function xyToBsel(x, y, gridx, buttons)
    local x2, y2 = 1, 1
    for i, button in ipairs(buttons) do
        if x2 == x and y2 == y then
            return i
        end

        x2 = x2 + (button.fullrow and gridx or 1)
        if x2 > gridx then
            y2 = y2 + 1
            x2 = 1
        end
    end
end

local menuroots = {
    main = {
        offset = Vector(-42, 10),
        titleoffset = Vector(0, -74), -- (0, -74)
        bounds = {-86, -62, 86, 79}, -- min X, min Y, max X, max Y
        height = 141,
        topspacing = 16,
        bottomspacing = 0,
        scrollersymX = -94,
        scrollersymYTop = -46,
        scrollersymYBottom = 78
    },
    tooltip = {
        offset = Vector(132, 6),
        --bounds = {-59, -52, 58, 64},
        topspacing = 0,
        bottomspacing = 0,
        height = 118
    }
}

local fontspacers = {8, 12, 16}
local menuStringBuffer = 0

local function getMenuStringLength(str, fsize)
    fsize = fsize + 1
    local length = 0
    local chr = {}
	
    for i = 1, string.len(str) do
		if menuStringBuffer == 0 then
			local err = true
			local sub
			
			for j = 0, 2 do
				sub = string.sub(str, i, i + j)
				local sub2 = string.sub(str, i + j, i + j)
				
				if mfdat[sub] then
					menuStringBuffer = j
					err = false
					break
				elseif mfdat[sub2] then
					menuStringBuffer = j - 1
					break
				end
			end
			
			if err then
				local errMsg = ""
				Isaac.DebugString(str)
				
				for j = 1, string.len(str) do
					local sub = string.sub(str, j, j)
					errMsg = errMsg .. sub .. " | "
				end
				
				errMsg = errMsg .. "end"
				
				Isaac.DebugString(errMsg)
				Isaac.DebugString("Invalid character " .. sub .. "!")
				sub = '!'
			end

			local len = mfdat[sub][fsize]
			table.insert(chr, {sub, length})
			length = length + len + 1
		else
			menuStringBuffer = menuStringBuffer - 1
		end
    end

    return length, chr
end

function dssmod.generateDynamicSet(base, selected, fsize, clr, shine, nocursor)
    local dssmenu = DeadSeaScrollsMenu
    local menupal = dssmenu.GetPalette()
    local rainbow = menupal.Rainbow
    fsize = base.fsize or fsize or 2
    local clr1 = menupal[2]
    local clr2 = menupal[3]
    local useclr = base.clr or clr or 2
    useclr = menupal[useclr]
    local shine = base.shine or shine

    local height = 0
    local width = {} -- since buttons are arranged vertically, only the widest part of the button should count for width

    local dynamicset = {type = 'dynamicset', set = {}, pos = base.pos or Vector(0, 0)}

    local modules = {}
    if base.strpair then
        local part = base.strpair[1]
        modules[#modules + 1] = {type = 'str', str = part.str, height = 0, halign = -1, color = useclr, alpha = part.alpha, shine = shine}

        local part = base.strpair[2]
        modules[#modules + 1] = {type = 'str', str = part.str, halign = 1, color = clr2, alpha = part.alpha, shine = shine, select = false}
    elseif base.str then
        --modules[#modules + 1] = {type = 'str', str = base.str, color = useclr, alpha = 1, shine = shine}
		modules[#modules + 1] = {type = 'str', str = base.str, halign = base.halign, color = useclr, alpha = 1, shine = shine}

        if base.substr then
            local subsize = base.substr.size or math.max(1, fsize - 1)
            modules[#modules + 1] = {type = 'str', str = base.substr.str, size = subsize, color = clr2, alpha = base.substr.alpha or .8, shine = shine, pos = Vector(0, -2), select = false}
        end
    elseif base.strset then
        for i, str in ipairs(base.strset) do
            local newstr = {type = 'str', str = str}
            if i ~= 1 then
                newstr.select = false
            end

            modules[#modules + 1] = newstr
        end
    elseif base.spr then
        modules[#modules + 1] = {type = 'spr', fontcolor = useclr, color = base.spr.color, sprite = base.spr.sprite, center = base.spr.center, centerx = base.spr.centerx, centery = base.spr.centery, width = base.spr.width, height = base.spr.height, float = base.spr.float, shadow = base.spr.shadow, invisible = base.spr.invisible, scale = base.spr.scale}
    end

    if base.variable or base.setting then
        local sizedown = math.max(1, fsize - 1)
        local setting = {type = 'str', settingscursor = not base.keybind, size = sizedown, color = clr2, alpha = .8, shine = shine, pos = Vector(0, -2), select = false}
        setting.min = base.min
        setting.max = base.max
        setting.setting = base.setting
        if base.slider then
            setting.slider = true
            setting.increment = base.increment
            setting.str = ''
            for i = 1, math.ceil(base.max / base.increment) do
                setting.str = setting.str..'i'
            end
        elseif base.choices then
            setting.choices = #base.choices
            setting.str = base.choices[base.setting]
        elseif base.max then
            setting.str = (base.pref or '')..base.setting..(base.suf or '')
        elseif base.keybind then
            if base.keybinding then
                setting.str = '[awaiting input]'
            else
                setting.str = '[' .. inputButtonNames[base.setting] .. ']'
            end
        end

        modules[#modules + 1] = setting
    end

    for _, module in ipairs(modules) do
        if module.type == 'str' then
            if module.size == nil then module.size = fsize end
            if module.height == nil then module.height = fontspacers[module.size] end

            local fullstr = module.str
            if type(module.str) == "table" then
                fullstr = ''
                for _, val in ipairs(module.str) do
                    fullstr = fullstr .. val.str
                end
            end

            local length, chr = getMenuStringLength(fullstr, module.size)
            module.len = length
            module.chr = chr
            width[#width + 1] = length
        else
            width[#width + 1] = module.width
        end

        if module.nocursor == nil then module.nocursor = nocursor end
        if module.colorselect == nil then module.colorselect = base.colorselect end
        if module.select == nil then module.select = selected end
        if module.usemenuclr == nil then module.usemenuclr = base.usemenuclr end
        if module.usecolorize == nil then module.usecolorize = base.usecolorize end
        if module.palcolor == nil then module.palcolor = base.palcolor end
        if module.glowcolor == nil then module.glowcolor = base.glowcolor end
        if module.glowtime == nil then module.glowtime = base.glowtime end
        if module.noclip == nil then module.noclip = base.noclip end
        if module.cursoroff == nil then module.cursoroff = base.cursoroff end

        module.ref = base

        height = height + module.height
        module.rainbow = rainbow or nil
        table.insert(dynamicset.set, module)
    end

    dynamicset.width = math.max(table.unpack(width))
    dynamicset.height = height

    if base.fullrow then dynamicset.fullrow = true end

    return dynamicset
end

function dssmod.generateMenuDraw(item, scenter, input, root, tbl)
    local dssmenu = DeadSeaScrollsMenu
    local menupal = dssmenu.GetPalette()
    local rainbow = menupal.Rainbow

    local drawings = {}
    local mroot = menuroots[root]
    local valign = item.valign or 0
    local halign = item.halign or 0
    local fsize = item.fsize or 3
    local spacers = {8, 12, 16}
    local spacer = spacers[fsize]
    local nocursor = (item.nocursor or item.scroller)
    local width = 82
    local seloff = 0

    local dynamicset = {
        type = 'dynamicset',
        set = {},
        valign = valign,
        halign = halign,
        width = width,
        height = 0,
        pos = Vector(0, 0),
        centeritems = item.centeritems
    }

    if item.gridx then
        dynamicset.gridx = item.gridx
        dynamicset.widest = 0
        dynamicset.highest = 0
    end

    --buttons
    local bsel = item.bsel
    if item.buttons then
        for i, btn in ipairs(item.buttons) do
            if not btn.forcenodisplay then
                local btnset = dssmod.generateDynamicSet(btn, bsel == i, fsize, item.clr, item.shine, nocursor)

                if dynamicset.widest then
                    if btnset.width > dynamicset.widest then
                        dynamicset.widest = btnset.width
                    end
                end

                if dynamicset.highest then
                    if btnset.height > dynamicset.highest then
                        dynamicset.highest = btnset.height
                    end
                end

                table.insert(dynamicset.set, btnset)

                dynamicset.height = dynamicset.height + btnset.height

                if bsel == i then
                    seloff = dynamicset.height - btnset.height / 2
                end
            end
        end
    end

    --tooltip
    if item.strset or item.strpair or item.str or item.spr then
        local itemset = dssmod.generateDynamicSet(item)
        dynamicset.height = dynamicset.height + itemset.height
        table.insert(dynamicset.set, itemset)
    end

    if dynamicset.gridx then
        dynamicset.height = 0

        local gridx, gridy = 1, 1
        local rowDrawings = {}
        for i, drawing in ipairs(dynamicset.set) do
            if drawing.fullrow then
                if #rowDrawings > 0 then
                    rowDrawings = {}
                    gridy = gridy + 1
                end

                gridx = math.ceil(dynamicset.gridx / 2)
                drawing.halign = -2
            end

            drawing.gridxpos = gridx
            drawing.gridypos = gridy

            rowDrawings[#rowDrawings + 1] = drawing

            local highestInRow, widestInRow, bselInRow
            for _, rowDrawing in ipairs(rowDrawings) do
                if not highestInRow or rowDrawing.height > highestInRow then
                    highestInRow = rowDrawing.height
                end

                if not widestInRow or rowDrawing.width > widestInRow then
                    widestInRow = rowDrawing.width
                end

                bselInRow = bselInRow or rowDrawing.bselinrow or bsel == i
            end

            for _, rowDrawing in ipairs(rowDrawings) do
                rowDrawing.highestinrow = highestInRow
                rowDrawing.widestinrow = widestInRow
                rowDrawing.bselinrow = bselInRow
            end

            gridx = gridx + 1
            if gridx > dynamicset.gridx or i == #dynamicset.set or drawing.fullrow or (dynamicset.set[i + 1] and dynamicset.set[i + 1].fullrow) then
                dynamicset.height = dynamicset.height + highestInRow
                if bselInRow then
                    seloff = dynamicset.height - highestInRow / 2
                end

                rowDrawings = {}
                gridy = gridy + 1
                gridx = 1
            end
        end
    end

    local yOffset = -(dynamicset.height / 2)

    if mroot.bounds then
        if yOffset < mroot.bounds[2] + mroot.topspacing then
            yOffset = mroot.bounds[2] + mroot.topspacing
        end
    end

    if not item.noscroll then
        if item.scroller then
            item.scroll = item.scroll or 0
            if input.down then
                item.scroll = item.scroll + 16
            elseif input.up then
                item.scroll = item.scroll - 16
            end

            local halfheight = dynamicset.height / 2
            item.scroll = math.max(mroot.height / 2, math.min(item.scroll, dynamicset.height - mroot.height / 2))
            --item.scroll = math.min(math.max(item.scroll, 80), height - 48)
            seloff = item.scroll
        end

        if dynamicset.height > mroot.height - (mroot.topspacing + mroot.bottomspacing) then
            seloff = -seloff + mroot.height / 2
            seloff = math.max(-dynamicset.height + mroot.height - mroot.bottomspacing, math.min(0, seloff))
            if item.vscroll then
                item.vscroll = Lerp(item.vscroll, seloff, .2)
            else
                item.vscroll = seloff
            end
            dynamicset.pos = Vector(0, item.vscroll)
        end
    end

    dynamicset.pos = dynamicset.pos + Vector(0, yOffset) --+ Vector(0, 64 * valign)
    table.insert(drawings, dynamicset)

    --scroll indicator
    if item.scroller and item.scroll then
        local jumpy = (game:GetFrameCount() % 20) / 10
        if item.scroll > mroot.height / 2 then
            local sym = {type = 'sym', frame = 9, pos = Vector(mroot.scrollersymX, mroot.scrollersymYTop - jumpy)}
            table.insert(drawings, sym)
        end

        if item.scroll < dynamicset.height - mroot.height / 2 then
            local sym = {type = 'sym', frame = 10, pos = Vector(mroot.scrollersymX, mroot.scrollersymYBottom + jumpy)}
            table.insert(drawings, sym)
        end
    end

    --title
    if item.title then
        local title = {type = 'str', str = item.title, size = 3, color = menupal[3], pos = mroot.titleoffset, halign = 0, underline = true, bounds = false}
        title.rainbow = rainbow or nil
        table.insert(drawings, title)
    end

    for _, drawing in ipairs(drawings) do
        if drawing.bounds == nil then drawing.bounds = mroot.bounds end
        if drawing.root == nil then drawing.root = scenter + mroot.offset end
    end

    return drawings
end

function Encyclopedia.RenderText(tab)
	local dssmenu = DeadSeaScrollsMenu
	local tbl = dssmenu.OpenedMenu
	local dtype = tab.type
	local scale = tab.scale or Vector(1, 1)
	local root = tab.root or getScreenCenterPosition()
	local pos = tab.pos or Encyclopedia.ZeroV
	local mpos = Isaac.WorldToScreen(Input.GetMousePosition(true)) * 2
    local dssmenu = DeadSeaScrollsMenu
    local uispr = tbl.MenuSprites or dssmenu.GetDefaultMenuSprites()
	local font = uispr.Font
	local menupal = dssmenu.GetPalette()
	local update = tab.update or false
	local alpha = tab.alpha or 1
	local color = tab.color or (tab.sprite and not tab.usemenuclr and Color(1, 1, 1, 1, 0, 0, 0)) or menupal[tab.palcolor or 2]
    local fontcolor = tab.fontcolor or color
    local shine = tab.shine or 0
    local bottomcutoff = false
	if tab.rainbow then
		color = hsvToRgb((pos.Y % 256) / 255, 1, 1, 1)
	end

    if tab.colorselect and not tab.select then
        alpha = alpha / 2
    end

	color = Color(color.R, color.G, color.B, alpha, shine, shine, shine)
    if tab.usecolorize then
        local r, g, b = color.R, color.G, color.B
        color = Color(1, 1, 1, alpha, shine, shine, shine)
        color:SetColorize(r, g, b, 1)
    end

    fontcolor = Color(color.R, color.G, color.B, alpha, shine, shine, shine)
	local fnames = {'12', '16', '24'}
	local scaler = {8, 12, 16}
	
	tab.size = tab.size or 1
	tab.str = tab.str or 'nostring'
	tab.halign = tab.halign or 0
	tab.valign = tab.valign or 0

	local str = tab.str
	local idx = tab.size + 1
	if not tab.chr or not tab.len then
		tab.len, tab.chr = getMenuStringLength(str, tab.size)
	end

	--drawing string
	local fname = fnames[tab.size]
	local myscale = scaler[tab.size]
	font.Color = fontcolor
	font.Scale = scale
	local xoff = ((tab.halign == 0 and tab.len / -2) or (tab.halign == 1 and tab.len * -1) or 0) + ((tab.parentwidth or 82) * tab.halign)
	if tab.halign == -2 then
		xoff = 0
	end

	local yoff = ((tab.valign == 0 and -.5) or (tab.valign == 1 and -1) or 0) * (myscale)
	if tab.valign == -2 then
		yoff = 0
	end

	local clipt = 0
	local clipb = 0
	local usepos = pos + Vector(xoff, yoff)

	local wtf = tab.size == 1 and -8 or tab.size == 2 and -4 or 0
	if tab.bounds then
		clipt = math.min(math.max(0, tab.bounds[2] - (usepos.Y)), myscale) -- myscale
		clipb = math.min(math.max(0, (usepos.Y + wtf) - tab.bounds[4]), myscale)
	end
	
	for i, chr in ipairs(tab.chr) do
		local fpos = usepos + Vector(chr[2], 0)
		if tab.slider then
			local iii = math.ceil(tab.max / tab.increment)
			if i > iii * (tab.setting / tab.max) then
				font.Color = Color(color.R, color.G, color.B, tab.alpha / 2, 0, 0, 0)
			end
		end

		if (not tab.bounds) or (clipt < myscale and clipb < myscale) then
			if tab.hintletter then
				if tab.hintletter == chr[1] then
					font.Color = Color(color.R, color.G, color.B, alpha * 0.83, 0, 0, 0)
				else
					font.Color = Color(color.R, color.G, color.B, alpha, 0, 0, 0)
				end
			end
			
			font:SetFrame(fname, mfdat[chr[1] ][1])
			font:Render(root + fpos, Vector(0, clipt), Vector(0, clipb))
		end
	end
end

local OldTimer
local OverwrittenPause = false
local AddedPauseCallback = false
function Encyclopedia:OverridePause(player, hook, action)
	if not AddedPauseCallback then return nil end

	if OverwrittenPause then
		OverwrittenPause = false
		AddedPauseCallback = false
		return
	end

	if action == ButtonAction.ACTION_SHOOTRIGHT then
		OverwrittenPause = true
		for _, ember in ipairs(Isaac.FindByType(EntityType.ENTITY_EFFECT, EffectVariant.FALLING_EMBER, -1)) do
			if ember:Exists() then
				ember:Remove()
			end
		end
		if REPENTANCE then
			for _, rain in ipairs(Isaac.FindByType(EntityType.ENTITY_EFFECT, EffectVariant.RAIN_DROP, -1)) do
				if rain:Exists() then
					rain:Remove()
				end
			end
		end
		return 0.75
	end
end
Encyclopedia:AddCallback(ModCallbacks.MC_INPUT_ACTION, Encyclopedia.OverridePause, InputHook.IS_ACTION_PRESSED)

function Encyclopedia.FreezeGame(unfreeze)
	if unfreeze then
		OldTimer = nil
		if not AddedPauseCallback then
			AddedPauseCallback = true
		end
	else
		if not OldTimer then
			OldTimer = game.TimeCounter
		end
		if REPENTANCE then
			Isaac.GetPlayer(0):UseActiveItem(CollectibleType.COLLECTIBLE_PAUSE, UseFlag.USE_NOANIM)
		else
			Isaac.GetPlayer(0):UseActiveItem(CollectibleType.COLLECTIBLE_PAUSE, false, false, true, false, 0)
		end
		game.TimeCounter = OldTimer
	end
end

function dssmod.drawMenu(tbl, tab)
	local dtype = tab.type
	local scale = tab.scale or Vector(1, 1)
	local root = tab.root or getScreenCenterPosition()
	local pos = tab.pos or Vector(0, 0)
	local mpos = Isaac.WorldToScreen(Input.GetMousePosition(true)) * 2
    local dssmenu = DeadSeaScrollsMenu
    local uispr = tbl.MenuSprites or dssmenu.GetDefaultMenuSprites()
	local font = uispr.Font
	local menuspr = uispr.Face
	local menupal = dssmenu.GetPalette()
	local update = tab.update or false
	local alpha = tab.alpha or 1
	local color = tab.color or (tab.sprite and not tab.usemenuclr and Color(1, 1, 1, 1, 0, 0, 0)) or menupal[tab.palcolor or 2]
    local fontcolor = tab.fontcolor or color
    if type(fontcolor) == "number" then
        fontcolor = menupal[fontcolor]
    end

    local shine = tab.shine or 0
    local bottomcutoff = false

    if tab.glowcolor then
        local glowLength = tab.glowtime or 60
        local glowTime = game:GetFrameCount() % glowLength
        local percent
        if glowTime <= (glowLength / 2) then
            percent = glowTime / (glowLength / 2)
        else
            percent = (glowTime - (glowLength / 2)) / (glowLength / 2)
            percent = 1 - percent
        end

        if type(tab.glowcolor) == "number" then
            color = Color.Lerp(color, menupal[tab.glowcolor], percent)
            fontcolor = Color.Lerp(fontcolor, menupal[tab.glowcolor], percent)
        else
            color = Color.Lerp(color, tab.glowcolor, percent)
            fontcolor = Color.Lerp(fontcolor, tab.glowcolor, percent)
        end
    end

    if tab.rainbow then
        local hue = pos.Y % 256
        if tab.glowcolor then
            local glowLength = tab.glowtime or 60
            local glowTime = game:GetFrameCount() % glowLength
            hue = hue + (glowTime / glowLength) * 255
            hue = hue % 256
        end

        color = hsvToRgb(hue / 255, 1, 1, 1)
        fontcolor = hsvToRgb(hue / 255, 1, 1, 1)
	end

    if tab.colorselect and not tab.select then
        alpha = alpha / 2
    end

	color = Color(color.R, color.G, color.B, alpha, shine, shine, shine)
    if tab.usecolorize then
        local r, g, b = color.R, color.G, color.B
        color = Color(1, 1, 1, alpha, shine, shine, shine)
        color:SetColorize(r, g, b, 1)
    end

    fontcolor = Color(fontcolor.R, fontcolor.G, fontcolor.B, alpha, shine, shine, shine)
	local fnames = {'12', '16', '24'}
	local scaler = {8, 12, 16}

    local selectCursorPos
    local settingsCursorXPlace

	if dtype == 'dot' then
		menuspr.Color = color
		menuspr:SetFrame("Sym", 6)
		menuspr:Render(root + pos, Vector(0, 0), Vector(0, 0))
	elseif dtype == 'sym' then
		menuspr.Color = color
		menuspr:SetFrame("Sym", tab.frame or 6)
		menuspr:Render(root + pos, Vector(0, 0), Vector(0, 0))
	elseif dtype == 'spr' then
		local uspr = tab.sprite
        local floaty = 0
        if tab.float then
            floaty = Vector(0, tab.float[1]):Rotated((game:GetFrameCount() * tab.float[2]) % 360)
            floaty = floaty.Y
        end

        if (tab.center or tab.centerx) and tab.width then
            pos = pos - Vector(tab.width / 2 * scale.X, 0)
        end

        if (tab.center or tab.centery) and tab.height then
            pos = pos - Vector(0, tab.height / 2 * scale.Y)
        end

        pos = pos + Vector(0, floaty)

        local clipt = 0
        local clipb = 0
        if tab.bounds and not tab.noclip then
            clipt = math.min(math.max(0, tab.bounds[2] - pos.Y))
            clipb = math.min(math.max(0, (pos.Y + tab.height - 16) - tab.bounds[4]))
        end

        if clipt + clipb >= tab.height then
            bottomcutoff = clipb >= tab.height
        else
    		uspr.Scale = scale
    		if tab.shadow then
    			uspr.Color = Color(0, 0, 0, alpha / 2, 0, 0, 0)
    			uspr:Render(root + pos + scale, Vector(0, clipt), Vector(0, clipb))
    		end

            if not tab.invisible then
          		uspr.Color = color

                tab.ref.renderedtopos = root + pos
          		uspr:Render(root + pos, Vector(0, clipt), Vector(0, clipb))
            end
        end

        selectCursorPos = pos + Vector(-12, tab.height / 2 * scale.Y)
	elseif dtype == 'str' then
		tab.size = tab.size or 1
		tab.str = tab.str or 'nostring'
		tab.halign = tab.halign or 0
		tab.valign = tab.valign or 0

		local str = tab.str
        local fullstr = str
        if type(str) == "table" then
            fullstr = ''
            for _, val in ipairs(str) do
                fullstr = fullstr .. val.str
            end
        end

		local idx = tab.size + 1
        if not tab.chr or not tab.len then
            tab.len, tab.chr = getMenuStringLength(fullstr, tab.size)
        end

		--drawing string
		local fname = fnames[tab.size]
		local myscale = scaler[tab.size]
		font.Scale = scale
		local xoff = ((tab.halign == 0 and tab.len / -2) or (tab.halign == 1 and tab.len * -1) or 0) + ((tab.parentwidth or 82) * tab.halign)
        if tab.halign == -2 then
            xoff = 0
        end

        local yoff = ((tab.valign == 0 and -.5) or (tab.valign == 1 and -1) or 0) * (myscale)
        if tab.valign == -2 then
            yoff = 0
        end

		local clipt = 0
		local clipb = 0
		local usepos = pos + Vector(xoff, yoff)

		local wtf = tab.size == 1 and -8 or tab.size == 2 and -4 or 0
		if tab.bounds and not tab.noclip then
			clipt = math.min(math.max(0, tab.bounds[2] - (usepos.Y)), myscale) -- myscale
			clipb = math.min(math.max(0, (usepos.Y + wtf) - tab.bounds[4]), myscale)
		end

        if clipt + clipb >= myscale then
            bottomcutoff = clipb >= myscale
        else
    		for i, chr in ipairs(tab.chr) do
                local substr
                local usecolor = fontcolor
                if type(str) == "table" then
                    local j = 0
                    for _, val in ipairs(str) do
                        if not substr and i <= (j + string.len(val.str)) then
                            substr = val
                        end

                        j = j + string.len(val.str)
                    end
                end

                if substr then
                    if substr.color then
                        if type(substr.color) == "number" then
                            usecolor = menupal[substr.color]
                        else
                            usecolor = substr.color
                        end
                    end
                end

                font.Color = usecolor

    			local fpos = usepos + Vector(chr[2], 0)
                if tab.slider then
                    local iii = math.ceil(tab.max / tab.increment)
                    if i > iii * (tab.setting / tab.max) then
                        font.Color = Color(usecolor.R, usecolor.G, usecolor.B, tab.alpha / 2, 0, 0, 0)
                    end
                end

    			if (not tab.bounds) or (clipt < myscale and clipb < myscale) then
                    if tab.hintletter then
                        if tab.hintletter == chr[1] then
                            font.Color = Color(usecolor.R, usecolor.G, usecolor.B, alpha * 0.83, 0, 0, 0)
                        else
                            font.Color = Color(usecolor.R, usecolor.G, usecolor.B, alpha, 0, 0, 0)
                        end
                    end

    				font:SetFrame(fname, mfdat[chr[1] ][1])
    				font:Render(root + fpos, Vector(0, clipt), Vector(0, clipb))
    			end
    		end

    		--underline
    		if tab.underline then
    			menuspr:SetFrame("Sym", 0)
    			menuspr.Color = fontcolor
    			menuspr:Render(root + pos + Vector(0, 16), Vector(0, 0), Vector(0, 0))
    		end
        end

        selectCursorPos = pos + Vector(xoff - 12, 3 - tab.size)
        if tab.size == 1 then
            selectCursorPos = pos + Vector(xoff - 6, 1 - tab.size)
        end

        settingsCursorXPlace = math.max(40, -xoff + 10)
	elseif dtype == 'dynamicset' then
		local yy = 0

        if tab.gridx and tab.centeritems then
            yy = yy + tab.highest / 2
        end

		for i, drawing in ipairs(tab.set) do
			drawing.root = root
			drawing.bounds = tab.bounds

            if tab.gridx then
                local totalwidth = tab.gridx * drawing.widestinrow
                local x = drawing.gridxpos - 1
                local xPos = (-totalwidth / 2) + Lerp(0, totalwidth - drawing.widestinrow, x / (tab.gridx - 1))
                drawing.pos = (drawing.pos or Vector(0, 0)) + pos + Vector(xPos, yy)

                if tab.centeritems then
                    local widthdiff = drawing.widestinrow - drawing.width
                    local heightdiff = drawing.highestinrow - drawing.height
                    drawing.pos = drawing.pos + Vector(widthdiff / 2 + drawing.width / 2, -heightdiff / 2 - drawing.height / 2)
                end

                if tab.set[i + 1] and tab.set[i + 1].gridypos > drawing.gridypos then
                    yy = yy + drawing.highestinrow
                end
            else
    			drawing.pos = (drawing.pos or Vector(0, 0)) + pos + Vector(0, yy)
                yy = yy + (drawing.height or 16)
            end

            drawing.halign = drawing.halign or tab.halign
			drawing.width = drawing.width or tab.width or 82
            drawing.parentwidth = tab.parentwidth or tab.width or drawing.width or 82
		end

		local yo = 0--(((tab.valign or -1) + 1) * yy) / -2

		for i, drawing in ipairs(tab.set) do
			drawing.pos = drawing.pos + Vector(0, yo)
			if dssmod.drawMenu(tbl, drawing) then -- returns true if cutoff
                return true
            end
		end
	end

    -- draw selected / choice arrows
    --selected
    if tab.select and not tab.nocursor and selectCursorPos then
        if tab.size == 1 then
            menuspr:SetFrame("Sym", 2)
        else
            menuspr:SetFrame("Sym", 1)
        end

        if tab.cursoroff then
            selectCursorPos = selectCursorPos + tab.cursoroff
        end

        menuspr.Color = fontcolor

        local clipt = math.min(math.max(0, tab.bounds[2] - (selectCursorPos.Y - 8)), 16)
        local clipb = math.min(math.max(0, (selectCursorPos.Y - 8) - tab.bounds[4]), 16)
        menuspr:Render(root + selectCursorPos, Vector(0, clipt), Vector(0, clipb))
    end

    -- choices
    if tab.settingscursor and settingsCursorXPlace then
        menuspr.Color = fontcolor
        if (tab.choices and tab.setting > 1) or (tab.max and tab.setting > (tab.min or 0)) then
            menuspr:SetFrame("Sym", 8)

            local sympos = pos + Vector(-settingsCursorXPlace, -1)
            local clipt = math.min(math.max(0, tab.bounds[2] - (sympos.Y - 8)), 16)
            local clipb = math.min(math.max(0, (sympos.Y - 8) - tab.bounds[4]), 16)
            menuspr:Render(root + sympos, Vector(0, clipt), Vector(0, clipb))
        end

        if (tab.choices and tab.setting < tab.choices) or (tab.max and tab.setting < tab.max) then
            menuspr:SetFrame("Sym", 7)

            local sympos = pos + Vector(settingsCursorXPlace, -1)
            local clipt = math.min(math.max(0, tab.bounds[2] - (sympos.Y - 8)), 16)
            local clipb = math.min(math.max(0, (sympos.Y - 8) - tab.bounds[4]), 16)
            menuspr:Render(root + sympos, Vector(0, clipt), Vector(0, clipb))
        end
    end
	
	-- Pausing the game.
	Encyclopedia.FreezeGame()

    if bottomcutoff then
        return true
    end
end

function dssmod.runMenu(tbl)
    local directory = tbl.Directory
    local directorykey = tbl.DirectoryKey
    local scenter = getScreenCenterPosition()

    local dssmenu = DeadSeaScrollsMenu
    local uispr = tbl.MenuSprites or dssmenu.GetDefaultMenuSprites()
    local menuspr = {uispr.Shadow, uispr.Back, uispr.Face, uispr.Border}
    local menumask = uispr.Mask
    local font = uispr.Font

	local menupal = dssmenu.GetPalette()
	local rainbow = menupal.Rainbow
	local root1 = scenter + Vector(-42, 10)
	local root2 = scenter + Vector(132, 6)

    -- initialize sprites
    directorykey.Idle = false
	if not directorykey.Init then
		for i = 1, 4 do
			menuspr[i]:Play("Dissapear") --(sic)
			menuspr[i]:SetLastFrame()
            menuspr[i]:Stop()
		end

        if not directorykey.OpenAnimation then
            directorykey.Idle = true
            directorykey.MaskAlpha = 0
        end

		directorykey.Init = true
	end
	
	if ModConfigMenu and ModConfigMenu.IsVisible then
		ModConfigMenu.CloseConfigMenu()
	end

    --background sprite
    if directorykey.OpenAnimation then
        directorykey.OpenAnimation = false
        for i = 1, 4 do
            menuspr[i]:Play("Appear", true)
        end
    end

    if directorykey.CloseAnimation then
        directorykey.MaskAlpha = approach(directorykey.MaskAlpha, 1, .25)
        directorykey.Item = directory[directorykey.Main]
        directorykey.Path = {}

        if directorykey.MaskAlpha == 1 then
            directorykey.CloseAnimation = false
            for i = 1, 4 do
                menuspr[i]:Play("Dissapear", true)
            end
        end
    end

    if tbl.Exiting and not directorykey.CloseAnimation and not menuspr[3]:IsPlaying("Dissapear") then
        tbl.Exiting = nil
        return
    elseif not menuspr[3]:IsPlaying("Appear") and not menuspr[3]:IsPlaying("Dissapear") then
        for i = 1, 4 do
            menuspr[i]:Play("Idle")
        end

        if not tbl.Exiting then
            directorykey.MaskAlpha = approach(directorykey.MaskAlpha, 0, .25)
            directorykey.Idle = true
        end
    end

	menuspr[2].Color = menupal[1]
	menuspr[3].Color = menupal[1]
	for i = 1, 3 do
		menuspr[i]:Render(root1, Vector(0, 0), Vector(0, 0))
		menuspr[i]:Update()
	end
    --menuspr[4]:Render(root1, Vector(0, 0), Vector(0, 0))
    --menuspr[4]:Update()

    if directorykey.Idle then
    	--INTERACTION
    	local item = directorykey.Item
        local itemswitched = false

        if item ~= directorykey.PreviousItem then
            itemswitched = true

            if item.generate then
                item.generate(item, tbl)
            end

            directorykey.PreviousItem = item
        end

        if item.update then
            item.update(item, tbl)
        end

        local input = menuinput.menu
    	local buttons = item.buttons
    	local pages = item.pages
    	local bsel = item.bsel or 1
    	local psel = item.psel or 1
    	local action = false
        local func = nil
        local changefunc = nil
        local prevbutton = nil
    	local dest = false
    	local button = false
    	local draw = true
        local allnosel = false

    	--buttons
    	if buttons and #buttons > 0 then
    		--button selection
    		item.bsel = math.min((item.bsel or 1), #buttons)

            allnosel = true
            for i, button in ipairs(buttons) do
                if button.generate and itemswitched then
                    button.generate(button, item, tbl)
                end

                if button.update then
                    button.update(button, item, tbl)
                end

                if button.display ~= nil or button.displayif then
                    if button.display == false or (button.displayif and not button.displayif(button, item, tbl)) then
                        button.nosel = true
                        button.forcenodisplay = true
                    elseif button.forcenodisplay then
                        button.nosel = nil
                        button.forcenodisplay = nil
                    end
                end

                if not button.nosel then
                    if allnosel and item.bsel < i then -- select the first selectable button if the currently selected button isn't selectable ex 1
                        item.bsel = i
                    end

                    allnosel = false
                end
            end

            local prevbsel = item.bsel
            if buttons[item.bsel].changefunc then
                prevbutton = buttons[item.bsel]
                changefunc = buttons[item.bsel].changefunc
            end

			if not Encyclopedia.SearchBoxOpen then
				if allnosel then
					item.bsel = 1
				elseif item.gridx then
					local firstLoop = true
					local tryKeepX, tryKeepY
					while buttons[item.bsel].nosel or firstLoop do
						local x, y, maxX, maxY = bselToXY(item.bsel, item.gridx, buttons)
						--[[
						local x = ((item.bsel - 1) % item.gridx) + 1
						local y = math.ceil(item.bsel / item.gridx)
						local maxY = math.ceil(#buttons / item.gridx)
						local maxX = ((#buttons - 1) % item.gridx) + 1 -- on maxY]]
						if tryKeepX then
							x = tryKeepX
							tryKeepX = nil
						end

						if tryKeepY then
							y = tryKeepY
							tryKeepY = nil
						end

						if input.up then
							y = y - 1
						elseif input.down then
							y = y + 1
						elseif input.left then
							x = x - 1
						elseif input.right then
							x = x + 1
						end

						local prevX, prevY = x, y

						if y < 1 then
							y = maxY
						elseif y > maxY then
							y = 1
						end

						maxX = maxX[y]
						if x < 1 then
							x = maxX
						elseif x > maxX then
							if input.down or input.up then
								x = maxX
							else
								x = 1
							end
						end

						item.bsel = xyToBsel(x, y, item.gridx, buttons)
						if buttons[item.bsel].nosel then
							if input.up or input.down then
								tryKeepX = prevX
							elseif input.left or input.right then
								tryKeepY = prevY
							end
						end

						--(y - 1) * item.gridx + x
						firstLoop = nil
					end
				else
					if input.up then
						item.bsel = ((item.bsel - 2) % #buttons) + 1
						while item.buttons[item.bsel].nosel do
							item.bsel = ((item.bsel - 2) % #buttons) + 1
						end
					elseif input.down or item.buttons[item.bsel].nosel then
						item.bsel = (item.bsel % #buttons) + 1
						while item.buttons[item.bsel].nosel do
							item.bsel = (item.bsel % #buttons) + 1
						end
					end
				end
            end

    		bsel = item.bsel
            if bsel == prevbsel then
                prevbutton = nil
                changefunc = nil
            end

    		dest = directory[buttons[bsel].dest]
    		button = item.buttons[bsel]

    		--button confirmation
			if not Encyclopedia.SearchBoxOpen then
				if input.confirm and not itemswitched and not allnosel then
					if button then
						PlaySound(menusounds.Pop2)
						if button.action then
							action = button.action
						end

						if button.func then
							func = button.func
						end
					end

					if dest then
						if not item.removefrompath then
							table.insert(directorykey.Path, item)
						end

						directorykey.Item = dest
					end
				end
    		end

    		--button choice selection
    		if (button.variable or button.setting) and not allnosel then
    			if button.choices then
    				button.setting = button.setting or 1
    				if (input.right or input.dright) and button.setting < #button.choices then
    					button.setting = button.setting + 1
    					sfx:Play(SoundEffect.SOUND_PLOP, 1, 0, false, .9 + (.2 * (#button.choices / (#button.choices - (button.setting - 1)))))
    					dssmod.setOption(button.variable, button.setting, button, directorykey.Item, directorykey)
    				elseif (input.left or input.dleft) and button.setting > 1 then
    					button.setting = button.setting - 1
    					sfx:Play(SoundEffect.SOUND_PLOP, 1, 0, false, .9 + (.2 * (#button.choices / (#button.choices - (button.setting - 1)))))
    					dssmod.setOption(button.variable, button.setting, button, directorykey.Item, directorykey)
    				elseif input.confirm then
    					button.setting = (button.setting % #button.choices) + 1
    					sfx:Play(SoundEffect.SOUND_PLOP, 1, 0, false, .9 + (.2 * (#button.choices / (#button.choices - (button.setting - 1)))))
    					dssmod.setOption(button.variable, button.setting, button, directorykey.Item, directorykey)
    				end
    			elseif button.max then
    				local inc, min, max = button.increment or 1, button.min or 0, button.max
    				local pop = false
    				button.setting = button.setting or 0
    				if (input.right or input.dright or input.confirm) then
    					if button.setting < max then
    						button.setting = math.min(button.setting + inc, max)
    						pop = true
    					elseif input.confirm then
    						button.setting = button.min or 0
    						pop = true
    					end
    				elseif (input.left or input.dleft) and button.setting > min then
    					button.setting = math.max(button.setting - inc, min)
    					pop = true
    				end

    				if pop then
    					dssmod.setOption(button.variable, button.setting, button, directorykey.Item, directorykey)
    					sfx:Play(SoundEffect.SOUND_PLOP, 1, 0, false, .9 + (.2 * (button.setting / button.max)))
    				end
                elseif button.keybind then
                    if input.keybinding then
                        if input.keybind then
                            if (input.keybind == Keyboard.KEY_ESCAPE or input.keybind == Keyboard.KEY_BACKSPACE) and button.setting ~= -1 then
                                input.keybind = -1
                            end

                            button.setting = input.keybind
                            dssmod.setOption(button.variable, button.setting, button, directorykey.Item, directorykey)
                            input.keybinding = nil
                            button.keybinding = nil
                        end
                    elseif input.confirm then
                        button.keybinding = true
                        input.keybinding = true
                    end
    			end
    		end
    	end

    	--pages
		if not Encyclopedia.SearchBoxOpen then
			if pages and pages > 0 then
				item.psel = math.min((item.psel or 1), pages)
				if input.left then
					item.psel = ((item.psel - 2) % pages) + 1
				elseif input.right then
					item.psel = (item.psel % pages) + 1
				end
				psel = item.psel
			end
    	end

        --BUTTON FUNCTIONS
        if func then
            func(button, directorykey.Item)
        end

        if changefunc then
            changefunc(prevbutton, directorykey.Item)
        end

    	--drawing
    	if draw then
            local drawings = dssmod.generateMenuDraw(item, scenter, input, "main", tbl)

            local tooltip = item.tooltip
            if button and button.tooltip then
                tooltip = button.tooltip
            end

            if tooltip then
                if tooltip.update then
                    tooltip.update(tooltip, item, button)
                end

                local drawings2 = dssmod.generateMenuDraw(tooltip, scenter, input, "tooltip")
                for _, drawing in ipairs(drawings2) do
                    drawings[#drawings + 1] = drawing
                end
            end

    		--draw
    		for i, drawing in ipairs(drawings) do
    			dssmod.drawMenu(tbl, drawing)
    		end
    	end

        --menu regressing
		if not Encyclopedia.SearchBoxOpen then
			if (input.back or input.toggle) and not itemswitched then
				if #directorykey.Path > 0 then
					PlaySound(menusounds.Pop3)
					local backItem = directorykey.Path[#directorykey.Path]
					directorykey.Path[#directorykey.Path] = nil
					if backItem.menuname then
						local newPath = {}
						for _, val in ipairs(directorykey.Path) do
							newPath[#newPath + 1] = val
						end

						dssmenu.OpenMenu(backItem.menuname)
						local menu = dssmenu.Menus[backItem.menuname]
						menu.DirectoryKey.Item = backItem
						menu.DirectoryKey.Path = newPath
						menu.DirectoryKey.PreviousItem = nil
					else
						directorykey.Item = backItem
					end
				elseif not directorykey.PreventClosing then
					dssmenu.CloseMenu()
				end
			end
    	end

        --BUTTON ACTIONS
        if action then
            if action == 'resume' then
                dssmenu.CloseMenu(true)
            elseif action == "openmenu" then
                dssmenu.OpenMenu(button.menu)
            end
        end
    end

    --border
    menuspr[4]:Render(root1, Vector(0, 0), Vector(0, 0))
    menuspr[4]:Update()

    --fade mask
    if directorykey.Idle and directorykey.MaskAlpha > 0 then
        local useclr = menupal[1]
        menumask.Color = Color(useclr.R, useclr.G, useclr.B, directorykey.MaskAlpha, 0, 0, 0)
        menumask:Play("Idle", true)
        menumask:Render(root1, Vector(0, 0), Vector(0, 0))
    end

    if directorykey.Idle then
        local item = directorykey.Item
        if item.postrender then
            item.postrender(item, tbl)
        end
    end
end

function dssmod.checkMenu()
    if not menuinput then
        return false
    end

	local input = menuinput.menu
	if not input then
		return false
	end

    local dssmenu = DeadSeaScrollsMenu
    if input.toggle and not dssmenu.IsOpen() then
        if dssmenu.IsMenuSafe() then
            if dssmenu.CanOpenGlobalMenu() then
                dssmenu.OpenMenu("Menu")
            else -- if only one mod is using the menu, open its menu and ignore this one
                for k, menu in pairs(dssmenu.Menus) do -- this is non-specific to simplify copying, less to swap
                    if k ~= "Menu" then
                        dssmenu.OpenMenu(k)
                    end
                end
            end
        else
            sfx:Play(SoundEffect.SOUND_BOSS2INTRO_ERRORBUZZ, .75, 0, false, 1.5)
        end
    end

    if dssmenu.OpenedMenu then
        if dssmenu.ExitingMenu and not dssmenu.OpenedMenu.Exiting then
            dssmenu.ExitingMenu = nil
            dssmenu.OpenedMenu = nil
        else
            dssmenu.OpenedMenu.Run(dssmenu.OpenedMenu)
        end
    else
        dssmenu.ExitingMenu = nil
    end
end

-- MOD-SPECIFIC VERSION OF CORE HANDLING STARTS HERE
local HiddenDescs = false
function dssmod.openMenu(tbl, openedFromNothing)
	Encyclopedia.SearchBoxOpen = false
    if openedFromNothing then
        tbl.DirectoryKey.OpenAnimation = true
    end

    tbl.DirectoryKey.PreviousItem = nil

    for k, item in pairs(tbl.Directory) do
        if item.buttons then
            for _, button in ipairs(item.buttons) do
                if button.load then
                    local setting = button.load(button, item, tbl)
                    button.setting = setting
                    if button.variable then
                        tbl.DirectoryKey.Settings[button.variable] = setting
                    end
                end
            end
        end
    end
	
	if EID then
		HiddenDescs = EID.isHidden
		EID.isHidden = true
	end
end

function dssmod.closeMenu(tbl, fullClose, noAnimate)
    if fullClose and not noAnimate then
        tbl.Exiting = true
        tbl.DirectoryKey.CloseAnimation = true
    end

    tbl.DirectoryKey.Path = {}
    tbl.DirectoryKey.Main = 'main'
    tbl.DirectoryKey.PreventClosing = false
    tbl.DirectoryKey.Item = tbl.Directory[tbl.DirectoryKey.Main]

    if tbl.DirectoryKey.SettingsChanged then
        tbl.DirectoryKey.SettingsChanged = false
        for k, item in pairs(tbl.Directory) do
            if item.buttons then
                for _, button in ipairs(item.buttons) do
                    if button.store and button.variable then
                        button.store(tbl.DirectoryKey.Settings[button.variable] or button.setting, button, item, tbl)
                    end
                end
            end
        end

        MenuProvider.SaveSaveData()
    end
	
	-- Unpausing the game.
	Encyclopedia.FreezeGame(true)
	if EID then EID.isHidden = HiddenDescs end
end

--POST RENDER
local ValidKeys = {
	[Keyboard.KEY_0] = "0",
	[Keyboard.KEY_1] = "1",
	[Keyboard.KEY_2] = "2",
	[Keyboard.KEY_3] = "3",
	[Keyboard.KEY_4] = "4",
	[Keyboard.KEY_5] = "s",
	[Keyboard.KEY_6] = "6",
	[Keyboard.KEY_7] = "7",
	[Keyboard.KEY_8] = "8",
	[Keyboard.KEY_9] = "9",
	[Keyboard.KEY_A] = "a",
	[Keyboard.KEY_B] = "b",
	[Keyboard.KEY_C] = "c",
	[Keyboard.KEY_D] = "d",
	[Keyboard.KEY_E] = "e",
	[Keyboard.KEY_F] = "f",
	[Keyboard.KEY_G] = "g",
	[Keyboard.KEY_H] = "h",
	[Keyboard.KEY_I] = "1",
	[Keyboard.KEY_J] = "j",
	[Keyboard.KEY_K] = "k",
	[Keyboard.KEY_L] = "l",
	[Keyboard.KEY_M] = "m",
	[Keyboard.KEY_N] = "n",
	[Keyboard.KEY_O] = "0",
	[Keyboard.KEY_P] = "p",
	[Keyboard.KEY_Q] = "q",
	[Keyboard.KEY_R] = "r",
	[Keyboard.KEY_S] = "s",
	[Keyboard.KEY_T] = "t",
	[Keyboard.KEY_U] = "v",
	[Keyboard.KEY_V] = "v",
	[Keyboard.KEY_W] = "w",
	[Keyboard.KEY_X] = "x",
	[Keyboard.KEY_Y] = "y",
	[Keyboard.KEY_Z] = "z",
	[Keyboard.KEY_KP_0] = "0",
	[Keyboard.KEY_KP_1] = "1",
	[Keyboard.KEY_KP_2] = "2",
	[Keyboard.KEY_KP_3] = "3",
	[Keyboard.KEY_KP_4] = "4",
	[Keyboard.KEY_KP_5] = "s",
	[Keyboard.KEY_KP_6] = "6",
	[Keyboard.KEY_KP_7] = "7",
	[Keyboard.KEY_KP_8] = "8",
	[Keyboard.KEY_KP_9] = "9",
}

local validJoystick = {
	[1] = "or either stick ",
	[2] = "or left stick ",
	[3] = "or right stick ",
	[4] = "or both sticks ",
	[5] = "or [select] ",
	[6] = "or [rt] + [select] ",
	[7] = "",
}

local TempestaFont = Font()
TempestaFont:Load("font/pftempestasevencondensed.fnt")
local displayedDSSMenuMsg = false

function dssmod:post_render()
    local dssmenu = DeadSeaScrollsMenu
    local isCore = MenuProvider.IsMenuCore()
    local isOpen = dssmenu.IsOpen()
    if isCore or isOpen then
        dssmod.getInput(0)
    end

    if not isCore and dssmenu and openToggle ~= isOpen then -- If not in control of certain settings, be sure to store them!
        openToggle = isOpen

        local change

        local palSetting = dssmenu.GetPaletteSetting()
        if palSetting ~= MenuProvider.GetPaletteSetting() then
            change = true
            MenuProvider.SavePaletteSetting(palSetting)
        end

        if not REPENTANCE then
            local hudSetting = dssmenu.GetHudOffsetSetting()
            if hudSetting ~= MenuProvider.GetHudOffsetSetting() then
                change = true
                MenuProvider.SaveHudOffsetSetting(hudSetting)
            end
        end

        local gamepadSetting = dssmenu.GetGamepadToggleSetting()
        if gamepadSetting ~= MenuProvider.GetGamepadToggleSetting() then
            change = true
            MenuProvider.SaveGamepadToggleSetting(gamepadSetting)
        end

        local keybindSetting = dssmenu.GetMenuKeybindSetting()
        if keybindSetting ~= MenuProvider.GetMenuKeybindSetting() then
            change = true
            MenuProvider.SaveMenuKeybindSetting(keybindSetting)
        end

        local menusNotified, knownNotified = dssmenu.GetMenusNotified(), MenuProvider.GetMenusNotified()
        if not KeysShareVals(menusNotified, knownNotified) then
            change = true
            MenuProvider.SaveMenusNotified(menusNotified)
        end

        local menusPoppedUp, knownPoppedUp = dssmenu.GetMenusPoppedUp(), MenuProvider.GetMenusPoppedUp()
        if not KeysShareVals(menusPoppedUp, knownPoppedUp) then
            change = true
            MenuProvider.SaveMenusPoppedUp(menusPoppedUp)
        end

        if change then
            MenuProvider.SaveSaveData()
        end
    end
	
	if displayedDSSMenuMsg == false
	and MenuProvider.GetStartPopUpSetting() == 1
	then
		displayedDSSMenuMsg = 120
	end
	
	if isCore and displayedDSSMenuMsg ~= true then
		if type(displayedDSSMenuMsg) ~= "number" then return end
		
		if displayedDSSMenuMsg <= 0 then
			displayedDSSMenuMsg = true
		else
			local RenderPos = getScreenBottomRight()
			local key = string.upper(ValidKeys[MenuProvider.GetMenuKeybindSetting()] or "c")
			local joystick = validJoystick[MenuProvider.GetGamepadToggleSetting()]
			local str = "Press [" .. key .. "]/[F1] " .. joystick .. "to open DSS Menu."
			local strColor = KColor(1, 1, 0, (math.min(displayedDSSMenuMsg, 60)/60) * 0.5)
			
			TempestaFont:DrawString(str, (RenderPos.X / 2) - (TempestaFont:GetStringWidthUTF8(str) / 2), RenderPos.Y - 42, strColor, 0, true)
			
			if (not game:IsPaused())
			and game:GetRoom():GetFrameCount() > 0
			and Isaac.GetFrameCount() % 2 == 0
			then
				displayedDSSMenuMsg = displayedDSSMenuMsg - 1
			end
		end
	end
end

dssmod:AddCallback(ModCallbacks.MC_POST_RENDER, dssmod.post_render)

-- These buttons will be included in this mod's menu if it is the only active menu, or in the global menu if it exists and this mod is managing it
local function sharedButtonDisplayCondition(button, item, tbl)
    return tbl.Name == "Menu" or not dssmenu.CanOpenGlobalMenu()
end

local hudOffsetButton = {
    str = 'hud offset',
    increment = 1, max = 10,
    variable = "HudOffset",
    slider = true,
    setting = 0,
    load = function()
        return DeadSeaScrollsMenu.GetHudOffsetSetting()
    end,
    store = function(var)
        DeadSeaScrollsMenu.SaveHudOffsetSetting(var)
    end,
    displayif = function(btn, item, tbl)
        return not REPENTANCE and sharedButtonDisplayCondition(btn, item, tbl)
    end,
    tooltip = {strset = {'be sure to', 'match the', 'setting', 'in the', 'pause menu'}}
}

local gamepadToggleButton = {
    str = 'gamepad toggle',
    choices = {'either stick', 'left stick', 'right stick', 'both sticks', '[select]', '[rt] + [select]', 'keybind only'},
    variable = 'ControllerToggle',
    tooltip = {strset = {'to open', 'and close', 'this menu with', 'a controller','','[f1] always', 'works'}},
    setting = 1,
    load = function()
        return DeadSeaScrollsMenu.GetGamepadToggleSetting()
    end,
    store = function(var)
        DeadSeaScrollsMenu.SaveGamepadToggleSetting(var)
    end,
    displayif = sharedButtonDisplayCondition
}

local menuKeybindButton = {
    str = 'menu keybind',
    tooltip = {strset = {'rebinds key', 'used to open', 'this menu.', '[f1] always', 'works.'}},
    variable = 'MenuKeybind',
    keybind = true,
    setting = Keyboard.KEY_C,
    load = function()
        return DeadSeaScrollsMenu.GetMenuKeybindSetting()
    end,
    store = function(var)
        DeadSeaScrollsMenu.SaveMenuKeybindSetting(var)
    end,
    changefunc = function(button)
        DeadSeaScrollsMenu.SaveMenuKeybindSetting(button.setting)
    end,
    displayif = sharedButtonDisplayCondition
}

local paletteButton = {
    str = 'menu palette',
    variable = "MenuPalette",
    setting = 1,
    load = function()
        return DeadSeaScrollsMenu.GetPaletteSetting()
    end,
    store = function(var)
        DeadSeaScrollsMenu.SavePaletteSetting(var)
    end,
    changefunc = function(button)
        DeadSeaScrollsMenu.SavePaletteSetting(button.setting)
    end,
    displayif = sharedButtonDisplayCondition,
    generate = function(button, item, tbl)
        local dssmenu = DeadSeaScrollsMenu
        if not button.generated or button.generated ~= #dssmenu.Palettes then
            button.setting = math.min(button.setting, #dssmenu.Palettes)
            button.generated = #dssmenu.Palettes
            button.choices = {}
            for _, palette in ipairs(dssmenu.Palettes) do
                button.choices[#button.choices + 1] = palette.Name
            end
        end
    end
}

local startPopupButton = {
    str = 'start popup',
	choices = {'enabled', 'disabled'},
    variable = 'StartPopup',
    tooltip = {strset = {'display some', 'instructions', 'to open the', 'DSS menu', 'at the start', 'of a run'}},
    setting = 1,
    load = function()
		return DeadSeaScrollsMenu.GetStartPopUpSetting()
    end,
    store = function(var)
        DeadSeaScrollsMenu.SaveStartPopUpSetting(var)
    end,
    changefunc = function(button)
        DeadSeaScrollsMenu.SaveStartPopUpSetting(var)
    end,
    displayif = sharedButtonDisplayCondition
}

local function changelogsGenerate(item, tbl)
    item.buttons = {}

    if not item.logtable then
        item.logtable = DeadSeaScrollsMenu.Changelogs
    end

    local dir = tbl.Directory
    for _, key in ipairs(item.logtable.List) do
        local glowcolor = (DeadSeaScrollsMenu.DoesLogWantNotification(key) and 3) or nil
        if type(key) == "table" then
            item.buttons[#item.buttons + 1] = {
                str = key.Name,
                dest = key.Key,
                glowcolor = glowcolor
            }
        else
            item.buttons[#item.buttons + 1] = {
                str = dir[key].title,
                dest = key,
                glowcolor = glowcolor
            }
        end
    end
end

local changelogsButton = {
    str = 'changelogs',
    menudest = 'Menu',
    dest = 'changelogs',
    generate = function(btn)
        if DeadSeaScrollsMenu.DoesLogWantNotification(DeadSeaScrollsMenu.Changelogs) then
            btn.glowcolor = 3
        else
            btn.glowcolor = nil
        end
    end,
    displayif = function(btn, item, tbl)
        if sharedButtonDisplayCondition(btn, item, tbl) then
            return #DeadSeaScrollsMenu.Changelogs.List > 0
        end

        return false
    end
}


local menuOpenToolTip = {strset = {'toggle menu', '', 'keyboard:', '[c] or [f1]', '','controller:', 'press analog'}, fsize = 2}
local function InitializeMenuCore()
    if not dssmenu.Palettes then
        dssmenu.Palettes = {}
    end

    if not dssmenu.ExistingPalettes then
        dssmenu.ExistingPalettes = {}
    end

    if not dssmenu.Menus then
        dssmenu.Menus = {}
    end

    if not dssmenu.Changelogs then
        dssmenu.Changelogs = {Key = 'changelogs', Name = 'changelogs', List = {}}
    end

    if not dssmenu.ChangelogItems then
        dssmenu.ChangelogItems = {}
    end

    if not dssmenu.QueuedMenus then
        dssmenu.QueuedMenus = {}
    end

    function dssmenu.AddPalettes(palettes)
        for _, palette in ipairs(palettes) do
            if not dssmenu.ExistingPalettes[palette.Name] then
                dssmenu.ExistingPalettes[palette.Name] = true

                for i, color in ipairs(palette) do
                    if type(color) == "table" then
                        palette[i] = Color(color[1] / 255, color[2] / 255, color[3] / 255, 1, 0, 0, 0)
                    else
                        palette[i] = color
                    end
                end

                dssmenu.Palettes[#dssmenu.Palettes + 1] = palette
            end
        end
    end

    local function stringLineIterator(s)
        if s:sub(-1)~="\n" then s=s.."\n" end
        return s:gmatch("(.-)\n")
    end

    dssmenu.AddPalettes({
        {
            Name = "classic",
            {199, 178, 154}, -- Back
            {54, 47, 45}, -- Text
            {94, 57, 61}, -- Highlight Text
        },
        {
            Name = "soy milk",
            {255, 237, 206},
            {134, 109, 103},
            {73, 56, 67},
        },
        {
            Name = "phd",
            {224, 208, 208},
            {84, 43, 39},
            {118, 66, 72},
        },
        {
            Name = "faded polaroid",
            {219, 199, 188},
            {111, 81, 63},
            {86, 29, 37},
        },
        {
            Name = "missing page 2",
            {178, 112, 110},
            {40, 0, 0},
            {63, 13, 18},
        },
        {
            Name = "???",
            {77, 98, 139},
            {29, 36, 52},
            {156, 200, 205},
        },
        {
            Name = "succubus",
            {51, 51, 51},
            {12, 12, 12},
            {81, 10, 22},
        },
        {
            Name = "birthright",
            {214, 186, 155},
            {38, 30, 22},
            {112, 7, 0},
        },
        {
            Name = "impish",
            {170, 142, 214},
            {47, 34, 68},
            {56, 3, 6},
        },
        {
            Name = "queasy",
            {87, 125, 73},
            {32, 38, 28},
            {56, 55, 23},
        },
        {
            Name = "fruitcake",
            Rainbow = true,
            {243, 226, 226},
            {54, 47, 45},
            {64, 57, 50},
        },
        {
            Name = "delirious",
            {255, 255, 255},
            {254, 240, 53},
            {139, 104, 104},
        },
        {
            Name = "searing",
            {255, 255, 255},
            {117, 120, 125},
            {114, 137, 218},
        },
    })

    function dssmenu.GetPaletteSetting()
        local palette = MenuProvider.GetPaletteSetting()
        if palette and dssmenu.Palettes[palette] then
            return palette
        else
            MenuProvider.SavePaletteSetting(1)
            MenuProvider.SaveSaveData()
            return 1
        end
    end

    function dssmenu.SavePaletteSetting(var)
        MenuProvider.SavePaletteSetting(var)
        MenuProvider.SaveSaveData()
    end

    function dssmenu.GetPalette()
        return dssmenu.Palettes[dssmenu.GetPaletteSetting()]
    end

    function dssmenu.GetHudOffsetSetting()
        if REPENTANCE then
            return Options.HUDOffset * 10
        else
            local hudOffset = MenuProvider.GetHudOffsetSetting()
            if hudOffset then
                return hudOffset
            else
                MenuProvider.SaveHudOffsetSetting(0)
                MenuProvider.SaveSaveData()
                return 0
            end
        end
    end

    function dssmenu.SaveHudOffsetSetting(var)
        if not REPENTANCE then
            MenuProvider.SaveHudOffsetSetting(var)
            MenuProvider.SaveSaveData()
        end
    end

    function dssmenu.GetGamepadToggleSetting()
        local gamepadToggle = MenuProvider.GetGamepadToggleSetting()
        if gamepadToggle then
            return gamepadToggle
        else
            MenuProvider.SaveGamepadToggleSetting(1)
            MenuProvider.SaveSaveData()
            return 1
        end
    end

    function dssmenu.SaveGamepadToggleSetting(var)
        MenuProvider.SaveGamepadToggleSetting(var)
        MenuProvider.SaveSaveData()
    end

    function dssmenu.GetMenuKeybindSetting()
        local menuKeybind = MenuProvider.GetMenuKeybindSetting()
        if menuKeybind then
            return menuKeybind
        else
            MenuProvider.SaveMenuKeybindSetting(Keyboard.KEY_C)
            MenuProvider.SaveSaveData()
            return Keyboard.KEY_C
        end
    end

    function dssmenu.SaveMenuKeybindSetting(var)
        MenuProvider.SaveMenuKeybindSetting(var)
        MenuProvider.SaveSaveData()
    end

    function dssmenu.GetMenusNotified()
        local menusNotified = MenuProvider.GetMenusNotified()
        if menusNotified then
            return menusNotified
        else
            MenuProvider.SaveMenusNotified({})
            MenuProvider.SaveSaveData()
            return {}
        end
    end

    function dssmenu.SaveMenusNotified(var)
        MenuProvider.SaveMenusNotified(var)
        MenuProvider.SaveSaveData()
    end

    function dssmenu.GetMenusPoppedUp()
        local menusNotified = MenuProvider.GetMenusPoppedUp()
        if menusNotified then
            return menusNotified
        else
            MenuProvider.SaveMenusPoppedUp({})
            MenuProvider.SaveSaveData()
            return {}
        end
    end

    function dssmenu.SaveMenusPoppedUp(var)
        MenuProvider.SaveMenusPoppedUp(var)
        MenuProvider.SaveSaveData()
    end

    function dssmenu.GetStartPopUpSetting()
        local popupSetting = MenuProvider.GetStartPopUpSetting()
        if popupSetting then
            return popupSetting
        else
            MenuProvider.SaveStartPopUpSetting(1)
            MenuProvider.SaveSaveData()
            return 1
        end
    end

    function dssmenu.SaveStartPopUpSetting(var)
        MenuProvider.SaveStartPopUpSetting(var)
        MenuProvider.SaveSaveData()
    end

    function dssmenu.GetDefaultMenuSprites()
        if not dssmenu.MenuSprites then
            dssmenu.MenuSprites = {
                Shadow = Sprite(),
                Back = Sprite(),
                Face = Sprite(),
                Mask = Sprite(),
                Border = Sprite(),
                Font = Sprite()
            }

            dssmenu.MenuSprites.Back:Load("gfx/ui/deadseascrolls/menu_back.anm2", true)
            dssmenu.MenuSprites.Face:Load("gfx/ui/deadseascrolls/menu_face.anm2", true)
            dssmenu.MenuSprites.Mask:Load("gfx/ui/deadseascrolls/menu_mask.anm2", true)
            dssmenu.MenuSprites.Border:Load("gfx/ui/deadseascrolls/menu_border.anm2", true)
            dssmenu.MenuSprites.Font:Load("gfx/ui/deadseascrolls/menu_font.anm2", true)
            dssmenu.MenuSprites.Shadow:Load("gfx/ui/deadseascrolls/menu_shadow.anm2", true)
        end

        return dssmenu.MenuSprites
    end

    function dssmenu.IsMenuSafe()
		if ModConfigMenu and ModConfigMenu.Visible then return false end
	
        local roomHasDanger = false
    	for _, entity in pairs(Isaac.GetRoomEntities()) do
    		if ((entity:IsActiveEnemy() and not entity:HasEntityFlags(EntityFlag.FLAG_FRIENDLY)) and entity:IsVulnerableEnemy()) -- Extra check to not cuck the menu at random.
    		or entity.Type == EntityType.ENTITY_PROJECTILE and entity:ToProjectile().ProjectileFlags & ProjectileFlags.CANT_HIT_PLAYER == 0
    		or entity.Type == EntityType.ENTITY_BOMBDROP then
    			roomHasDanger = true
    			break
    		end
    	end

    	if game:GetRoom():IsClear() and not roomHasDanger then
    		return true
    	end

    	return false
    end

    local dssdirectory = {
        main = {
            title = 'dead sea scrolls',
            buttons = {
                {str = 'resume game', action = 'resume'},
                {str = 'menu settings', dest = 'menusettings'},
                changelogsButton
            },
            tooltip = menuOpenToolTip
        },
        menusettings = {
            title = 'menu settings',
            buttons = {
                hudOffsetButton,
                gamepadToggleButton,
                menuKeybindButton,
                paletteButton,
                startPopupButton,
            },
            tooltip = menuOpenToolTip
        },
        changelogs = {
            title = 'changelogs',
            fsize = 3,
            generate = changelogsGenerate,
            tooltip = menuOpenToolTip
        },
    }

    if dssmenu.ChangelogItems then
        for k, v in pairs(dssmenu.ChangelogItems) do
            dssdirectory[k] = v
        end
    end
	
    local dssdirectorykey = {
    	Item = dssdirectory.main,
        Main = 'main',
    	Idle = false,
    	MaskAlpha = 1,
        Settings = {},
        SettingsChanged = false,
    	Path = {},
    }

    local changelogFormatCodes = {
        "{CLR1",
        "{CLR2",
        "{CLR3",
        "{SYM",
        "{FSIZE2}",
        "{FSIZE3}",
        "}"
    }

    function dssmenu.AddChangelog(...)
        local args = {...}
        local changelogText, changelogTextIndex

        if #args < 2 then
            changelogTextIndex = 1
        else
            for i = #args, 2, -1 do
                if type(args[i]) == "string" then
                    changelogTextIndex = i
                    changelogText = args[i]
                    break
                end
            end
        end

        if changelogTextIndex == 1 then
            error("DeadSeaScrollsMenu.AddChangelog requires a category arg to be placed under, before changelog text.", 2)
        end

        local categories = {}
        for i = 1, changelogTextIndex - 1 do
            categories[#categories + 1] = string.lower(args[i])
        end

        local tooltip, notify, popup, defaultFSize = args[changelogTextIndex + 1] or menuOpenToolTip, args[changelogTextIndex + 2], args[changelogTextIndex + 3], args[changelogTextIndex + 4] or 1

        local lines = {}
        for line in stringLineIterator(changelogText) do
            lines[#lines + 1] = line
        end

        if not tooltip.strset then
            tooltip = {strset = tooltip}
        end

        local buttons = {}
        local changelogItem = {
            title = categories[#categories],
            tooltip = tooltip,
            fsize = defaultFSize,
            scroller = true,
            nocursor = true,
            buttons = buttons,
            wantspopup = popup,
            wantsnotify = notify,
            generate = function(item)
                if item.wantsnotify then
                    local notifies = DeadSeaScrollsMenu.GetMenusNotified()
                    if not notifies[item.keyindirectory] then
                        notifies[item.keyindirectory] = true
                        DeadSeaScrollsMenu.SaveMenusNotified(notifies)
                    end
                end
            end
        }

        for i, line in ipairs(lines) do
            local btn = {}
            local substrs = {}

            local formatParsingDone = false
            local strStart = 1
            local strEnd = string.len(line)
            local strIndex = strStart
            local fsize
            local subStrData = {Start = strIndex}

            while not formatParsingDone do
                local nextFormatCode, nextFormatCodeStart, nextFormatCodeEnd
                for _, code in ipairs(changelogFormatCodes) do
                    local startInd, endInd = string.find(line, code, strIndex)
                    if startInd then
                        if not nextFormatCode or nextFormatCodeStart > startInd then
                            nextFormatCode = code
                            nextFormatCodeStart = startInd
                            nextFormatCodeEnd = endInd
                        end
                    end
                end

                if nextFormatCode then
                    if nextFormatCode == "{FSIZE1}" then
                        fsize = 1
                    elseif nextFormatCode == "{FSIZE2}" then
                        fsize = 2
                    elseif nextFormatCode == "{FSIZE3}" then
                        fsize = 3
                    end

                    if string.sub(nextFormatCode, -1, -1) ~= "}" or nextFormatCode == "}" then -- substr, must be closed later
                        if subStrData then -- terminate existing substring
                            local subStrEnd = nextFormatCodeStart - 1
                            if subStrEnd > 0 then
                                local substr = string.sub(line, subStrData.Start, subStrEnd)
                                if not subStrData.NoLower then
                                    substr = string.lower(substr)
                                end

                                substrs[#substrs + 1] = {str = substr, color = subStrData.Color}
                            end
                        end

                        subStrData = {Start = nextFormatCodeStart} -- starts where code starts because code will be removed

                        if nextFormatCode == "{SYM" then
                            subStrData.NoLower = true
                        elseif nextFormatCode == "{CLR1" then
                            subStrData.Color = 1
                        elseif nextFormatCode == "{CLR2" then
                            subStrData.Color = 2
                        elseif nextFormatCode == "{CLR3" then
                            subStrData.Color = 3
                        end
                    end

                    line = string.sub(line, nextFormatCodeEnd + 1, -1) -- remove formatting code from line
                else
                    formatParsingDone = true
                end

                if subStrData.Start > string.len(line) then
                    formatParsingDone = true
                elseif formatParsingDone and subStrData.Start then
                    local substr = string.sub(line, subStrData.Start, -1)
                    if not subStrData.NoLower then
                        substr = string.lower(substr)
                    end

                    substrs[#substrs + 1] = {str = substr, color = subStrData.Color}
                end
            end

            btn.fsize = fsize
            if #substrs > 0 then
                btn.str = substrs
            else
                btn.str = ''
            end

            buttons[#buttons + 1] = btn
        end

        local changelogKey = '{'
        local changelogPath = dssmenu.Changelogs
        for i, v in ipairs(categories) do
            local found, foundIndex
            for i2, tbl in ipairs(changelogPath.List) do
                if tbl.Name == v then
                    found = tbl
                    foundIndex = i2
                    break
                end
            end

            if not found then
                changelogPath.List[#changelogPath.List + 1] = {
                    Name = v,
                    List = {}
                }
                foundIndex = #changelogPath.List
            end

            changelogPath = changelogPath.List[foundIndex]

            changelogKey = changelogKey .. v .. "{"

            if not changelogPath.Key then
                changelogPath.Key = changelogKey
            end

            if not dssdirectory[changelogKey] and i ~= #categories then
                dssdirectory[changelogKey] = {
                    title = v,
                    tooltip = menuOpenToolTip,
                    fsize = 2,
                    buttons = {},
                    logtable = changelogPath,
                    generate = changelogsGenerate
                }
                dssmenu.ChangelogItems[changelogKey] = dssdirectory[changelogKey]
            end
        end

        local ind = #changelogPath.List + 1
        for i, v in ipairs(changelogPath.List) do
            if v == changelogKey then
                ind = i
            end
        end

        changelogPath.List[ind] = changelogKey

        changelogItem.keyindirectory = changelogKey
        dssdirectory[changelogKey] = changelogItem
        dssmenu.ChangelogItems[changelogKey] = changelogItem
    end

    function dssmenu.DoesLogWantNotification(log)
        local menusNotified = dssmenu.GetMenusNotified()
        if type(log) == "string" then
            return dssdirectory[log] and dssdirectory[log].wantsnotify and not menusNotified[log]
        elseif log.List then
            for _, val in ipairs(log.List) do
                if dssmenu.DoesLogWantNotification(val) then
                    return true
                end
            end
        end
    end

    function dssmenu.AddMenu(name, tbl)
        tbl.Name = name
        if not dssmenu.Menus[name] then
            dssmenu.MenuCount = (dssmenu.MenuCount or 0) + 1

            if name ~= "Menu" then
                dssdirectory.main.buttons[#dssdirectory.main.buttons + 1] = {str = string.lower(name), action = "openmenu", menu = name}
            end
        end

        dssmenu.Menus[name] = tbl
    end

    if dssmenu.Menus then
        for k, v in pairs(dssmenu.Menus) do
            if k ~= "Menu" then
                dssdirectory.main.buttons[#dssdirectory.main.buttons + 1] = {str = string.lower(k), action = "openmenu", menu = k}
            end
        end
    end

    local openCalledRecently
    function dssmenu.OpenMenu(name)
        openCalledRecently = true

        local openFromNothing = not dssmenu.OpenedMenu
        if not openFromNothing then
            if dssmenu.OpenedMenu.Close then
                dssmenu.OpenedMenu.Close(dssmenu.OpenedMenu, false, true, true)
            end
        else
            PlaySound(menusounds.Open)
        end

        dssmenu.OpenedMenu = dssmenu.Menus[name]

        if dssmenu.OpenedMenu.Open then
            dssmenu.OpenedMenu.Open(dssmenu.OpenedMenu, openFromNothing)
        end
    end

    function dssmenu.CloseMenu(fullClose, noAnimate)
        local shouldFullClose = fullClose or dssmenu.MenuCount <= 2 or dssmenu.OpenedMenu.Name == "Menu"
        if dssmenu.OpenedMenu and dssmenu.OpenedMenu.Close then
            dssmenu.OpenedMenu.Close(dssmenu.OpenedMenu, shouldFullClose, noAnimate)
        end

        if shouldFullClose then
            PlaySound(menusounds.Close)
        end

        if not shouldFullClose and dssmenu.OpenedMenu and dssmenu.OpenedMenu.Name ~= "Menu" then
            dssmenu.OpenMenu("Menu")
        elseif dssmenu.OpenedMenu then
            if noAnimate or not dssmenu.OpenedMenu.Exiting then -- support for animating menus out
                dssmenu.OpenedMenu = nil
            else
                dssmenu.ExitingMenu = true
            end
        end
    end

    function dssmenu.IsOpen()
        return dssmenu.OpenedMenu and not dssmenu.OpenedMenu.Exiting
    end

    function dssmenu.CanOpenGlobalMenu()
        return dssmenu.MenuCount ~= 2 or #dssmenu.Changelogs.List > 1
    end

    function dssmenu.GetCoreInput() -- allows overriding the menu's input
        return MenuProvider.GetCoreInput()
    end

    function dssmenu.OpenMenuToPath(name, item, path, preventClosing)
        local menu = dssmenu.Menus[name]
        if not menu.Directory then
            error("Unsupported menu passed to DeadSeaScrollsMenu.OpenMenuToPath.", 2)
        end

        local dir, key = menu.Directory, menu.DirectoryKey
        DeadSeaScrollsMenu.OpenMenu(name)

        key.Item = dir[item]
        key.PreviousItem = nil

        if path then
            for i, v in ipairs(path) do
                if type(v) == "string" then
                    path[i] = dir[v]
                elseif v.menu then
                    local menu2 = dssmenu.Menus[v.menu]
                    if menu2.Directory then
                        path[i] = menu2.Directory[v.item]
                        path[i].menuname = v.menu
                    else
                        error("Unsupported menu passed to DeadSeaScrollsMenu.OpenMenuToPath.", 2)
                    end
                else
                    error("Invalid path passed to DeadSeaScrollsMenu.OpenMenuToPath.", 2)
                end
            end
        end

        key.Path = path or {}
        if preventClosing then
            key.PreventClosing = true
        end

        key.Main = item
    end

    function dssmenu.QueueMenuOpen(name, item, priority)
        local placeAt = #dssmenu.QueuedMenus + 1
        for i, menu in ipairs(dssmenu.QueuedMenus) do
            if priority > menu.priority then
                placeAt = i
                break
            end
        end

        table.insert(dssmenu.QueuedMenus, placeAt, {menu = name, item = item, priority = priority})
    end

    function dssmod:DisablePlayerControlsInMenu(player)
        local open = dssmenu.IsOpen()
        if open then
            player.ControlsCooldown = math.max(player.ControlsCooldown, 15)
            player:GetData().MenuDisabledControls = true
        else
            player:GetData().MenuDisabledControls = nil
        end
    end

    dssmod:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, dssmod.DisablePlayerControlsInMenu)

    function dssmod:CheckMenuOpen()
        openCalledRecently = false
        dssmod.checkMenu()
    end

    dssmod:AddCallback(ModCallbacks.MC_POST_RENDER, dssmod.CheckMenuOpen)

    local recentGameStart = false
    function dssmod:CloseMenuOnGameStart()
        if not openCalledRecently and dssmenu.IsOpen() then
            dssmenu.CloseMenu(true, true)
        end

        recentGameStart = true
    end

    dssmod:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, dssmod.CloseMenuOnGameStart)

    function dssmod:OpenQueuedMenus()
        if #dssmenu.QueuedMenus > 0 and not dssmenu.IsOpen() then
            local first, exceptFirst = dssmenu.QueuedMenus[1], nil
            if #dssmenu.QueuedMenus > 1 then
                exceptFirst = {}
                for i = #dssmenu.QueuedMenus, 2, -1 do
                    exceptFirst[#exceptFirst + 1] = dssmenu.QueuedMenus[i]
                end
            end

            dssmenu.OpenMenuToPath(first.menu, first.item, exceptFirst, false)
            dssmenu.QueuedMenus = {}
        end

        if recentGameStart then -- this is delayed from game start to allow mods to add changelogs on game start without breaking notifications / popups
            recentGameStart = nil

            local popups = dssmenu.GetMenusPoppedUp()
            local shouldSave
            for k, v in pairs(dssdirectory) do
                if v.wantspopup then
                    if not popups[k] then
                        popups[k] = true
                        dssmenu.QueueMenuOpen("Menu", k, 0)
                        shouldSave = true
                    end
                end
            end

            for k, v in pairs(popups) do
                if not dssdirectory[k] or not dssdirectory[k].wantspopup then
                    popups[k] = nil
                    shouldSave = true
                end
            end

            local notifies = dssmenu.GetMenusNotified()
            for k, v in pairs(notifies) do
                if not dssdirectory[k] or not dssdirectory[k].wantsnotify then
                    notifies[k] = nil
                    shouldSave = true
                end
            end

            if shouldSave then
                MenuProvider.SaveMenusPoppedUp(popups)
                MenuProvider.SaveMenusNotified(notifies)
                MenuProvider.SaveSaveData()
            end
        end
    end

    dssmod:AddCallback(ModCallbacks.MC_POST_UPDATE, dssmod.OpenQueuedMenus)

    function dssmenu.RemoveCallbacks()
        dssmod:RemoveCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, dssmod.DisablePlayerControlsInMenu)
        dssmod:RemoveCallback(ModCallbacks.MC_POST_RENDER, dssmod.CheckMenuOpen)
        dssmod:RemoveCallback(ModCallbacks.MC_POST_GAME_STARTED, dssmod.CloseMenuOnGameStart)
        dssmod:RemoveCallback(ModCallbacks.MC_POST_UPDATE, dssmod.OpenQueuedMenus)
    end

    dssmenu.AddMenu("Menu", {Run = dssmod.runMenu, Open = dssmod.openMenu, Close = dssmod.closeMenu, Directory = dssdirectory, DirectoryKey = dssdirectorykey})

    DeadSeaScrollsMenu = dssmenu
end

MenuProvider = {}

-- Encyclopedia exclusive stuff.
function MenuProvider.SaveSaveData()
    Encyclopedia.SaveEncyclopediaData()
end

function MenuProvider.GetPaletteSetting()
    return Encyclopedia.GetDSSData().MenuPalette
end

function MenuProvider.SavePaletteSetting(var)
    Encyclopedia.GetDSSData().MenuPalette = var
end

function MenuProvider.GetHudOffsetSetting()
    if not REPENTANCE then
        return Encyclopedia.GetDSSData().HudOffset
    else
        return Options.HUDOffset * 10
    end
end

function MenuProvider.SaveHudOffsetSetting(var)
    if not REPENTANCE then
        Encyclopedia.GetDSSData().HudOffset = var
    end
end

function MenuProvider.GetGamepadToggleSetting()
    return Encyclopedia.GetDSSData().MenuControllerToggle
end

function MenuProvider.SaveGamepadToggleSetting(var)
    Encyclopedia.GetDSSData().MenuControllerToggle = var
end

function MenuProvider.GetMenuKeybindSetting()
    return Encyclopedia.GetDSSData().MenuKeybind
end

function MenuProvider.SaveMenuKeybindSetting(var)
    Encyclopedia.GetDSSData().MenuKeybind = var
end

function MenuProvider.GetMenusNotified()
    return Encyclopedia.GetDSSData().MenusNotified
end

function MenuProvider.SaveMenusNotified(var)
    Encyclopedia.GetDSSData().MenusNotified = var
end

function MenuProvider.GetMenusPoppedUp()
    return Encyclopedia.GetDSSData().MenusPoppedUp
end

function MenuProvider.SaveMenusPoppedUp(var)
    Encyclopedia.GetDSSData().MenusPoppedUp = var
end

function MenuProvider.GetStartPopUpSetting()
    return Encyclopedia.GetDSSData().StartPopup
end

function MenuProvider.SaveStartPopUpSetting(var)
    Encyclopedia.GetDSSData().StartPopup = var
end

local dssCoreIncluded = 2
if not dssmenu or (dssmenu.CoreVersion < dssCoreIncluded) then
    if dssmenu then
        dssmenu.RemoveCallbacks()
    else
        dssmenu = {Menus = {}}
    end

    dssmenu.CoreVersion = dssCoreIncluded
    dssmenu.CoreMod = "Encyclopedia"
    DeadSeaScrollsMenu = dssmenu
end

function MenuProvider.IsMenuCore()
    return DeadSeaScrollsMenu.CoreMod == "Encyclopedia"
end

function MenuProvider.GetCoreInput()
    return menuinput
end

if MenuProvider.IsMenuCore() then
    InitializeMenuCore()
end

Encyclopedia.Encylopedia_dir = {}
local ItemSlot = Encyclopedia.CreateSprite("gfx/encyclopedia.slots.anm2", "Slot")
local ItemSelector = Encyclopedia.CreateSprite("gfx/encyclopedia.slots.anm2", "Selector")
local ItemSelector2 = Encyclopedia.CreateSprite("gfx/encyclopedia.slots2.anm2", "Selector")
local Chargebar = Encyclopedia.CreateSprite("gfx/ui/encyclopedia.chargebar.anm2", "BarFull")
local ChargebarOverlay = Encyclopedia.CreateSprite("gfx/ui/encyclopedia.chargebar.anm2", "BarOverlay1")
local QualityIcons = Encyclopedia.CreateSprite("gfx/encyclopedia.quality_icons.anm2", "Quality 0")

Encyclopedia.FilterTypes = {
	Type = {
		ID = 1,
		func = function(a, b)
			return a.ItemId < b.ItemId
		end,
	},
	TypeInv = {
		ID = 2,
		func = function(a, b)
			return b.ItemId < a.ItemId
		end,
	},
	
	Alphabetical = {
		ID = 3,
		func = function(a, b)
			return string.lower(a.Name) < string.lower(b.Name)
		end,
	},
	AlphabeticalInv = {
		ID = 4,
		func = function(a, b)
			return string.lower(b.Name) < string.lower(a.Name)
		end,
	},
	
	Quality = {
		ID = 5,
		func = function(a, b)
			local ConfigA = Encyclopedia.ItemConfig:GetCollectible(a.ItemId)
			local ConfigB = Encyclopedia.ItemConfig:GetCollectible(b.ItemId)
			
			if ConfigA.Quality == ConfigB.Quality then
				return string.lower(a.Name) < string.lower(b.Name)
			else
				return ConfigA.Quality < ConfigB.Quality
			end
		end,
	},
	QualityInv = {
		ID = 6,
		func = function(a, b)
			local ConfigA = Encyclopedia.ItemConfig:GetCollectible(a.ItemId)
			local ConfigB = Encyclopedia.ItemConfig:GetCollectible(b.ItemId)
			
			if ConfigA.Quality == ConfigB.Quality then
				return string.lower(b.Name) < string.lower(a.Name)
			else
				return ConfigB.Quality < ConfigA.Quality
			end
		end,
	},
}

Encyclopedia.NavigationData = {
	items = {
		all = {1, 1, 0},
		vanilla = {1, 1, 0},
	},
	trinkets = {
		all = {1, 1, 0},
		vanilla = {1, 1, 0},
	},
	pills = {
		all = {1, 1, 0},
		vanilla = {1, 1, 0},
	},
	cards = {
		all = {1, 1, 0},
		vanilla = {1, 1, 0},
	},
	runes = {
		all = {1, 1, 0},
		vanilla = {1, 1, 0},
	},
	souls = {
		all = {1, 1, 0},
		vanilla = {1, 1, 0},
	},
	status = {1, 1, 0},
	room = {1, 1, 0},
	characters_a = {
		all = {1, 1, 0},
		vanilla = {1, 1, 0},
	},
	characters_b = {
		all = {1, 1, 0},
		vanilla = {1, 1, 0},
	},
}

if REPENTANCE then

Encyclopedia.ItemPools = {
	POOL_TREASURE = 1,
	POOL_SHOP = 2,
	POOL_BOSS = 3,
	POOL_DEVIL = 4,
	POOL_ANGEL = 5,
	POOL_SECRET = 6,
	POOL_LIBRARY = 7,
	POOL_SHELL_GAME = 8, -- unused (for now)
	POOL_GOLDEN_CHEST = 9,
	POOL_RED_CHEST = 10,
	POOL_BEGGAR = 11,
	POOL_DEMON_BEGGAR = 12,
	POOL_CURSE = 13,
	POOL_KEY_MASTER = 14,
	POOL_BATTERY_BUM = 15,
	POOL_MOMS_CHEST = 16,
	
	POOL_GREED_TREASURE = 17,
	POOL_GREED_BOSS = 18,
	POOL_GREED_SHOP = 19,
	POOL_GREED_DEVIL = 20,
	POOL_GREED_ANGEL = 21,
	POOL_GREED_CURSE = 22,
	POOL_GREED_SECRET = 23,
	
	POOL_CRANE_GAME = 24,
	POOL_ULTRA_SECRET = 25,
	POOL_BOMB_BUM = 26,
	POOL_PLANETARIUM = 27,
	POOL_OLD_CHEST = 28,
	POOL_BABY_SHOP = 29,
	POOL_WOODEN_CHEST = 30,
	POOL_ROTTEN_BEGGAR = 31,
	
	NUM_ITEMPOOLS = 31
}

Encyclopedia.ItemPoolSprites = {
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_treasure.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_shop.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_boss.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_devil.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_angel.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_secret.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_library.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_beggar.png"), -- change later
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_golden_chest.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_red_chest.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_beggar.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_demon_beggar.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_curse.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_key_master.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_battery_bum.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_moms_chest.png"),
	
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_treasure.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_boss.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_shop.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_devil.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_angel.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_curse.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_secret.png"),
	
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_crane_game.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_ultrasecret.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_bomb_bum.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_planetarium.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_old_chest.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_baby_shop.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_wooden_chest.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_rotten_beggar.png"),
}

else

Encyclopedia.ItemPools = {
	POOL_TREASURE = 1,
	POOL_SHOP = 2,
	POOL_BOSS = 3,
	POOL_DEVIL = 4,
	POOL_ANGEL = 5,
	POOL_SECRET = 6,
	POOL_LIBRARY = 7,
	POOL_CHALLENGE = 8,
	POOL_GOLDEN_CHEST = 9,
	POOL_RED_CHEST = 10,
	POOL_BEGGAR = 11,
	POOL_DEMON_BEGGAR = 12,
	POOL_CURSE = 13,
	POOL_KEY_MASTER = 14,
	POOL_BOSSRUSH = 15,
	POOL_DUNGEON = 16,
	
	POOL_GREED_TREASURE = 17,
	POOL_GREED_BOSS = 18,
	POOL_GREED_SHOP = 19,
	POOL_GREED_DEVIL = 20,
	POOL_GREED_ANGEL = 21,
	POOL_GREED_CURSE = 22,
	POOL_GREED_SECRET = 23,
	POOL_GREED_LIBRARY = 24,
	POOL_GREED_GOLDEN_CHEST = 25,
	
	POOL_BOMB_BUM = 26,
	
	NUM_ITEM_POOLS = 26,
}

Encyclopedia.ItemPoolSprites = {
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_treasure.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_shop.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_boss.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_devil.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_angel.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_secret.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_library.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_challenge.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_golden_chest.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_red_chest.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_beggar.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_demon_beggar.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_curse.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_key_master.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_boss_rush.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_dungeon.png"),
	
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_treasure.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_boss.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_shop.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_devil.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_angel.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_curse.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_secret.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_library.png"),
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_greed_golden_chest.png"),
	
	Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_bomb_bum.png"),
}

end

Encyclopedia.UnknownItemPoolSpr = Encyclopedia.CreateSprite("gfx/encyclopedia.itempools.anm2", "Idle", nil, "gfx/itempools/pool_unknown.png")

Encyclopedia.itemsTable = {
	Total = 1,
	CurrentFilter = 1,
	Sorters = {
		Encyclopedia.FilterTypes.Type,
		Encyclopedia.FilterTypes.TypeInv,
		Encyclopedia.FilterTypes.Alphabetical,
		Encyclopedia.FilterTypes.AlphabeticalInv,
	},
	
	allinternal = {},
	all = {},
	vanilla = {},
}

if REPENTANCE then
	Encyclopedia.itemsTable.Sorters[#Encyclopedia.itemsTable.Sorters + 1] = Encyclopedia.FilterTypes.Quality
	Encyclopedia.itemsTable.Sorters[#Encyclopedia.itemsTable.Sorters + 1] = Encyclopedia.FilterTypes.QualityInv
end

Encyclopedia.trinketsTable = {
	Total = 1,
	CurrentFilter = 1,
	Sorters = {
		Encyclopedia.FilterTypes.Type,
		Encyclopedia.FilterTypes.TypeInv,
		Encyclopedia.FilterTypes.Alphabetical,
		Encyclopedia.FilterTypes.AlphabeticalInv,
	},
	
	allinternal = {},
	all = {},
	vanilla = {},
}

Encyclopedia.pocketitemsTable = {
	cards = {
		Total = 1,
		CurrentFilter = 1,
		Sorters = {
			Encyclopedia.FilterTypes.Type,
			Encyclopedia.FilterTypes.TypeInv,
			Encyclopedia.FilterTypes.Alphabetical,
			Encyclopedia.FilterTypes.AlphabeticalInv,
		},
		
		allinternal = {},
		all = {},
		vanilla = {},
		
		globalcards = {}
	},
	runes = {
		Total = 1,
		CurrentFilter = 1,
		Sorters = {
			Encyclopedia.FilterTypes.Type,
			Encyclopedia.FilterTypes.TypeInv,
			Encyclopedia.FilterTypes.Alphabetical,
			Encyclopedia.FilterTypes.AlphabeticalInv,
		},
		
		allinternal = {},
		all = {},
		vanilla = {},
	},
	souls = {
		Total = 1,
		CurrentFilter = 1,
		Sorters = {
			Encyclopedia.FilterTypes.Type,
			Encyclopedia.FilterTypes.TypeInv,
			Encyclopedia.FilterTypes.Alphabetical,
			Encyclopedia.FilterTypes.AlphabeticalInv,
		},
		
		allinternal = {},
		all = {},
		vanilla = {},
	},
	pills = {
		Total = 1,
		CurrentFilter = 1,
		Sorters = {
			Encyclopedia.FilterTypes.Type,
			Encyclopedia.FilterTypes.TypeInv,
			Encyclopedia.FilterTypes.Alphabetical,
			Encyclopedia.FilterTypes.AlphabeticalInv,
			Encyclopedia.FilterTypes.Positive,
			Encyclopedia.FilterTypes.PositiveInv,
		},
		
		allinternal = {},
		all = {},
		vanilla = {},
	},
}

Encyclopedia.statusTable = {}

Encyclopedia.roomItemsTable = {}

Encyclopedia.TrackerTables = {
	items = {},
	trinkets = {},
	cards = {},
	pills = {},
	characters_a = {},
	characters_b = {},
	
	itemsNames = {},
	trinketsNames = {},
	cardsNames = {},
	pillsNames = {},
	characters_aNames = {},
	characters_bNames = {},
}

Encyclopedia.AutoCardSprites = {}

Encyclopedia.CharacterTracker_a = {}
Encyclopedia.CharacterTracker_b = {}
Encyclopedia.TaintedsTable = {}

Encyclopedia.characters_aTable = {
	Total = 1,
	
	all = {},
	vanilla = {},
}

Encyclopedia.characters_bTable = {
	Total = 1,
	
	all = {},
	vanilla = {},
}

local ItemVectors = {
	Vector(-125, -25),
	Vector(-85, -25),
	Vector(-45, -25),
	Vector(-5, -25),
	Vector(35, -25),
	
	Vector(-125, 15),
	Vector(-85, 15),
	Vector(-45, 15),
	Vector(-5, 15),
	Vector(35, 15),
	
	Vector(-125, 55),
	Vector(-85, 55),
	Vector(-45, 55),
	Vector(-5, 55),
	Vector(35, 55),
}

local CharactersVector = {
	Vector(-124, -10),
	Vector(-73, -10),
	Vector(-21, -10),
	Vector(30, -10),
	
	Vector(-124, 50),
	Vector(-73, 50),
	Vector(-21, 50),
	Vector(30, 50),
}

function Encyclopedia.fitTextToWidth(str, fsize, textboxWidth)
    local formatedLines = {}
    local curLength = 0
    local text = ""
	
    for word in string.gmatch(str, "([^%s]+)") do
        local wordLength = getMenuStringLength(word, fsize)

        if curLength + wordLength <= textboxWidth or curLength < 12 then
            text = text .. word .. " "
            curLength = curLength + wordLength
        else
            table.insert(formatedLines, text)
            text = word .. " "
            curLength = wordLength
        end
    end
    table.insert(formatedLines, text)
    return formatedLines
end

local TooltipVecs = {
	RightPageOffset = Vector(132, 6),
	TextOffset = Vector(2, 0),
	TextOffsetSmall = Vector(1, 0),
	
	Vec40 = Vector(0, 40),
	Vec32 = Vector(0, 32),
	Vec24 = Vector(0, 24),
	Vec20 = Vector(0, 20),
	Vec18 = Vector(0, 18),
	Vec16 = Vector(0, 16),
	Vec15 = Vector(0, 15),
	Vec12 = Vector(0, 12),
	Vec8 = Vector(0, 8),
	Vec9 = Vector(0, 9),
	Vec4 = Vector(0, 4),
	
	Vec20X = Vector(20, 0),
	Vec24X = Vector(24, 0),
	Vec26X = Vector(26, 0),
	Vec36X = Vector(36, 0),
	
	PoolOffsets = {
		{
			Encyclopedia.ZeroV,
		},
		{
			Vector(-12, 0),
			Vector(12, 0),
		},
		{
			Vector(-24, 0),
			Encyclopedia.ZeroV,
			Vector(24, 0),
		},
		{
			Vector(-36, 0),
			Vector(-12, 0),
			Vector(12, 0),
			Vector(36, 0),
		},
		{
			Vector(-36, 0),
			Vector(-18, 0),
			Vector(0, 0),
			Vector(18, 0),
			Vector(36, 0),
		},
		{
			Vector(-45, 0),
			Vector(-27, 0),
			Vector(-9, 0),
			Vector(9, 0),
			Vector(27, 0),
			Vector(45, 0),
		},
		{
			Vector(-36, 0),
			Vector(-24, 0),
			Vector(-12, 0),
			Vector(0, 0),
			Vector(12, 0),
			Vector(24, 0),
			Vector(36, 0),
		},
		{
			Vector(-42, 0),
			Vector(-30, 0),
			Vector(-18, 0),
			Vector(-6, 0),
			Vector(6, 0),
			Vector(18, 0),
			Vector(30, 0),
			Vector(42, 0),
		},
	},
	
	SearchText = Vector(-54, -94),
	SearchBox = Vector(0, -84),
	
	SortersText = Vector(-18, 64),
	SortersBox = Vector(-2, 72),
	
	ScrollBar = Vector(-166, 11),
	
	PoolV = Vector(1, 1),
	PoolVHalf = Vector(0.5, 0.5),
	PoolVQuarts = Vector(0.75, 0.75),
}

local ChargeBarTable = {}

if REPENTANCE then

ChargeBarTable = {
	[1] = true,
	[2] = true,
	[3] = true,
	[4] = true,
	[5] = true,
	[6] = true,
	[8] = true,
	[12] = true,
}

else

ChargeBarTable = {
	[1] = true,
	[2] = true,
	[3] = true,
	[4] = true,
	[6] = true,
	[12] = true,
}

end

local ScrollBarSpr = {
	Encyclopedia.CreateSprite("gfx/ui/deadseascrolls/menu_scrollbar.anm2", "Idle", 0, "gfx/ui/deadseascrolls/menu_scrollbar_shadow.png"),
	Encyclopedia.CreateSprite("gfx/ui/deadseascrolls/menu_scrollbar.anm2", "Idle", 0, "gfx/ui/deadseascrolls/menu_scrollbar_back.png", true),
	Encyclopedia.CreateSprite("gfx/ui/deadseascrolls/menu_scrollbar.anm2", "Idle", 0, "gfx/ui/deadseascrolls/menu_scrollbar_face.png", true),
	Encyclopedia.CreateSprite("gfx/ui/deadseascrolls/menu_scrollbar.anm2", "Idle", 0, "gfx/ui/deadseascrolls/menu_scrollbar_border.png"),
}
local ScrollBarSpr2 = Encyclopedia.CreateSprite("gfx/ui/deadseascrolls/menu_scrollbar_bar.anm2")

function Encyclopedia.RenderItemTooltip(item, locked)
	local TargetV = TooltipVecs.RightPageOffset + Vector(0, -56)
	local CenterV = getScreenCenterPosition()
	local Offset = 0
	local NameTab = Encyclopedia.fitTextToWidth(item.Name, 2, 92)
	local DescTab = item.Desc and Encyclopedia.fitTextToWidth(item.Desc, 1, 92)
	local TitleTab = item.Title and Encyclopedia.fitTextToWidth(item.Title, 1, 92)
	
	if TitleTab then
		Offset = Offset + (#TitleTab * 9)
	end
	
	Offset = Offset + (#NameTab * 12) + 36
	
	if item.Desc then
		Offset = Offset + ((#DescTab - 1) * 9)
	end
	
	if item.Pools and not locked then
		local TotalPools = #item.Pools
	
		if TotalPools <= 4 then
			Offset = Offset + (math.ceil(#item.Pools / 4) * 24)
		elseif TotalPools <= 6 then
			Offset = Offset + (math.ceil(#item.Pools / 6) * 18) - 2
		else
			Offset = Offset + (math.ceil(#item.Pools / 8) * 12) - 2
		end
	end
	
	-- Render
	Offset = math.max(0, (95 - Offset) / 2)
	TargetV = TargetV + Vector(0, Offset)
	
	if TitleTab then
		for i, text in ipairs(TitleTab) do
			Encyclopedia.RenderText({str = text, size = 1, pos = TargetV + TooltipVecs.TextOffsetSmall})
			
			TargetV = TargetV + TooltipVecs.Vec9
		end
	end
	
	for i, text in ipairs(NameTab) do
		Encyclopedia.RenderText({str = text, size = 2, pos = TargetV + TooltipVecs.TextOffset})
		
		if i == #NameTab then
			TargetV = TargetV + TooltipVecs.Vec24
		else
			TargetV = TargetV + TooltipVecs.Vec12
		end
	end
	
	if item.typeString == "items" and not locked then
		local config = Encyclopedia.ItemConfig:GetCollectible(item.ItemId)
		local Charges = item.ActiveCharge or config.MaxCharges
		
		if config.Type == ItemType.ITEM_ACTIVE then
			if REPENTANCE then
				local Duration
				local EraserCharges
				
				if config.ChargeType == ItemConfig.CHARGE_TIMED then
					Duration = math.floor((Charges / 30) * 100) / 100
				elseif config.ChargeType == ItemConfig.CHARGE_SPECIAL then
					EraserCharges = true
				end
				
				Chargebar.Color = EraserCharges and Encyclopedia.EraserColor or Encyclopedia.VanillaColor
				Chargebar:Play("BarFull", true)
				
				if ChargeBarTable[Charges] then
					if EraserCharges then
						ChargebarOverlay:Play("BarOverlay" .. Charges, true)
					end
					Chargebar:PlayOverlay("BarOverlay" .. Charges, true)
				else
					if Charges == 0 then
						Chargebar.Color = Encyclopedia.ShadowColor
						Chargebar:Play("BarEmpty", true)
					end
					
					if EraserCharges then
						ChargebarOverlay:Play("BarOverlay1", true)
					end
					Chargebar:PlayOverlay("BarOverlay1", true)
				end
				
				Chargebar:Render(CenterV + TargetV + Encyclopedia.DoubleV - TooltipVecs.Vec20X, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
				if EraserCharges then
					ChargebarOverlay:Render(CenterV + TargetV + Encyclopedia.DoubleV - TooltipVecs.Vec20X, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
				end
				
				if Duration then
					Encyclopedia.RenderText({str = tostring(Duration), size = 1, pos = TargetV + TooltipVecs.TextOffsetSmall - TooltipVecs.Vec36X - TooltipVecs.Vec4})
					Encyclopedia.RenderText({str = "sec", size = 1, pos = TargetV + TooltipVecs.TextOffsetSmall - TooltipVecs.Vec36X + TooltipVecs.Vec4})
				elseif (not ChargeBarTable[Charges]) and Charges > 0 then
					Encyclopedia.RenderText({str = tostring(Charges), size = 1, pos = TargetV + TooltipVecs.TextOffsetSmall - TooltipVecs.Vec36X - TooltipVecs.Vec4})
					Encyclopedia.RenderText({str = "pips", size = 1, pos = TargetV + TooltipVecs.TextOffsetSmall - TooltipVecs.Vec36X + TooltipVecs.Vec4})
				end
			else
				Chargebar.Color = Encyclopedia.VanillaColor
				Chargebar:Play("BarFull", true)
				
				if ChargeBarTable[Charges] then
					Chargebar:PlayOverlay("BarOverlay" .. Charges, true)
				else
					if Charges == 0 then
						Chargebar.Color = Encyclopedia.ShadowColor
						Chargebar:Play("BarEmpty", true)
					else
						local Duration = math.floor((Charges / 30) * 100) / 100
						Encyclopedia.RenderText({str = tostring(Duration), size = 1, pos = TargetV + TooltipVecs.TextOffsetSmall - TooltipVecs.Vec36X - TooltipVecs.Vec4})
						Encyclopedia.RenderText({str = "sec", size = 1, pos = TargetV + TooltipVecs.TextOffsetSmall - TooltipVecs.Vec36X + TooltipVecs.Vec4})
					end
					Chargebar:PlayOverlay("BarOverlay1", true)
				end
				
				Chargebar:Render(CenterV + TargetV + Encyclopedia.DoubleV - TooltipVecs.Vec20X, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
			end
		end
		
		if REPENTANCE then
			local Quality = config.Quality
			
			QualityIcons:Play("Quality " .. Quality, true)
			QualityIcons.Color = Encyclopedia.ShadowColor
			QualityIcons:Render(CenterV + TargetV + TooltipVecs.Vec26X + Encyclopedia.DoubleV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
			QualityIcons:Update()
			QualityIcons.Color = Encyclopedia.VanillaColor
			QualityIcons:Render(CenterV + TargetV + TooltipVecs.Vec26X, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		end
	end
	
	local ToolTipSpr = Encyclopedia.GetCachedSprite("ToolTip", item.Spr)
	ToolTipSpr.Color = Encyclopedia.ShadowColor
	ToolTipSpr:Render(CenterV + TargetV + Encyclopedia.DoubleV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	ToolTipSpr:Update()
	ToolTipSpr.Color = item.TargetColor or Encyclopedia.VanillaColor
	ToolTipSpr:Render(CenterV + TargetV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	TargetV = TargetV + TooltipVecs.Vec24
	
	if item.Desc then
		for i, text in ipairs(DescTab) do
			Encyclopedia.RenderText({str = text, size = 1, pos = TargetV + TooltipVecs.TextOffsetSmall})
			
			if i < #DescTab then
				TargetV = TargetV + TooltipVecs.Vec9
			end
		end
	end
	
	if item.Pools and not locked then
		local TotalPools = #item.Pools
		local Modifier
		local ScaleMod
		local OffsetMod
		
		if TotalPools <= 4 then
			Modifier = 4
			ScaleMod = TooltipVecs.PoolV
			OffsetMod = TooltipVecs.Vec24
		elseif TotalPools <= 6 then
			Modifier = 6
			ScaleMod = TooltipVecs.PoolVQuarts
			OffsetMod = TooltipVecs.Vec18
		else
			Modifier = 8
			ScaleMod = TooltipVecs.PoolVHalf
			OffsetMod = TooltipVecs.Vec12
		end
		
		TargetV = TargetV + TooltipVecs.Vec15
	
		for i, pool in ipairs(item.Pools) do	
			local TargetOffsets = TotalPools >= Modifier and Modifier or TotalPools
			local OffsetEntry = ((i - 1) % Modifier) + 1
			local SprOffset = TooltipVecs.PoolOffsets[TargetOffsets][OffsetEntry]
			local SprEntry = Encyclopedia.ItemPoolSprites[pool]
			local UnknownPool = Encyclopedia.UnknownItemPoolSpr
			
			local PoolSpr = (pool == "Unknown" and UnknownPool or (SprEntry and SprEntry or UnknownPool))
			PoolSpr.Scale = ScaleMod
			PoolSpr.Color = Encyclopedia.ShadowColor
			PoolSpr:Render(CenterV + TargetV + SprOffset + Encyclopedia.DoubleV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
			PoolSpr:Update()
			PoolSpr.Color = Encyclopedia.VanillaColor
			PoolSpr:Render(CenterV + TargetV + SprOffset, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
			
			if i % Modifier == 0 then
				TotalPools = TotalPools - Modifier
				TargetV = TargetV + OffsetMod
			end
		end
	end
end

function Encyclopedia.RenderItems(Type, class)
	local Items = Encyclopedia[Type .. "Table"][tostring(class) .. "Temp"] or Encyclopedia[Type .. "Table"][tostring(class)]
	local NavData = Encyclopedia.NavigationData[Type][tostring(class)]
	local Slot, Pos, StartPoint = NavData[1], NavData[2], NavData[3]
	local CenterV = getScreenCenterPosition()
	local item = Items[NavData[2]]
	local menupal = dssmenu.GetPalette()
	local CurrentLocked = false
	
	local BoxV = CenterV + TooltipVecs.ScrollBar
    ScrollBarSpr[2].Color = menupal[1]
    ScrollBarSpr[3].Color = menupal[1]
	
	for i = 1, 4 do
		ScrollBarSpr[i]:Render(BoxV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	end
	
	local TotalSlides = math.ceil(#Items / 5)
	
	local CenterPos = 72.5
	local ScaleMod = 1 / math.max(1, TotalSlides)
	local ScaleOffset = 145 * ScaleMod
	local TopCap = -(145 - CenterPos)
	local BotCap = 145 - CenterPos - ScaleOffset
	local TargetPos = math.min(BotCap, TopCap + (ScaleOffset * math.floor(math.max(1, Pos - 1) / 5)))
	
	ScrollBarSpr2.Scale = Vector(1, ScaleMod)
	ScrollBarSpr2:Render(BoxV + Vector(0, (ScaleOffset / 2) + TargetPos), Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	
	for i = 1, 15 do
		local CurItem = Items[StartPoint + i]
		if not CurItem then break end
		
		local TargetV = CenterV + ItemVectors[i]
		
		ItemSlot.Color = menupal[1]
		ItemSlot:Render(TargetV + Encyclopedia.DoubleV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		
		local ItemSpr = Encyclopedia.GetCachedSprite(i, CurItem.Spr)
		
		ItemSpr.Color = Encyclopedia.ShadowColor
		ItemSpr:Render(TargetV + Encyclopedia.DoubleV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		ItemSpr:Update()
		
		local TempItem = {}
		local ItemLocked
		if CurItem.UnlockFunc then
			for i, v in pairs(CurItem) do
				TempItem[i] = CurItem[i]
			end
			
			TempItem.Name = "???"
			TempItem.Desc = "???"
			
			TempItem = TempItem:UnlockFunc()
			
			if TempItem then
				TempItem.TargetColor = TempItem.TargetColor or Encyclopedia.LockColor
				ItemLocked = true
			else
				TempItem = CurItem
				TempItem.TargetColor = TempItem.TargetColor or Encyclopedia.VanillaColor
			end
		else
			TempItem = CurItem
			TempItem.TargetColor = Encyclopedia.VanillaColor
		end
		
		ItemSpr.Color = TempItem.TargetColor
		ItemSpr:Render(TargetV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		
		if Slot == i then
			if ItemLocked then
				CurrentLocked = true
			end
			
			ItemSelector.Color = menupal[3]
			ItemSelector:Render(TargetV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
			Encyclopedia.RenderItemTooltip(TempItem, CurrentLocked)
		end
	end
	
	Encyclopedia.RenderText({str = Pos .. " / " .. #Items, size = 2, pos = Vector(-44, 90)})
	
	if Encyclopedia.SearchBoxOpen then return end
	
	local input = menuinput.menu
	if input.up then
		if Pos > 5 then
			if Slot > 5 then
				NavData[1] = Slot - 5
			else
				NavData[3] = StartPoint - 5
			end
			
			NavData[2] = Pos - 5
		end
	elseif input.down then
		if Pos <= #Items - 5 then
			if Slot <= 10 then
				NavData[1] = Slot + 5
			else
				NavData[3] = StartPoint + 5
				
				if Pos > #Items - 5 then
					NavData[1] = 10 + ((#Items - 1) % 5) + 1
				end
			end
			
			NavData[2] = math.min(#Items, Pos + 5)
		elseif (Pos - 1) % 5 > (#Items - 1) % 5 then
			local Difference = #Items - Pos
			if Slot <= 10 then
				NavData[1] = Slot + Difference
			else
				NavData[3] = StartPoint + 5
				
				if Pos > #Items - 5 then
					NavData[1] = Slot + Difference - 5
				end
			end
			
			NavData[2] = math.min(#Items, Pos + Difference)
		end
	elseif input.right then
		if Pos % 5 > 0 and Pos < #Items then
			NavData[1] = Slot + 1
			NavData[2] = Pos + 1
		end
	elseif input.left then
		if Pos % 5 ~= 1 then
			NavData[1] = Slot - 1
			NavData[2] = Pos - 1
		end
	end
	
	if input.confirm and not CurrentLocked then
		local dssmenu = DeadSeaScrollsMenu
		local tbl = dssmenu.OpenedMenu
		
		if not tbl then
			Isaac.DebugString("[Error] - DeadSeaScrollsMenu not initialized.")
			return
		end
		
		if item.WikiDesc then
			local directorykey = tbl.DirectoryKey
			local dest = Encyclopedia.Encylopedia_dir[Type .. "sub"]
			
			dest = {}
			dest.title = item.Name
			dest.collectible = item
			dest.update = function(self, tbl)
				Encyclopedia.RenderItemTooltip(self.collectible)
			end
			
			if #item.WikiDesc > 1 then
				dest.buttons = {}
				
				for i, but in ipairs(item.WikiDesc) do
					local Title = but[1].str
					local root = tostring(Type .. "sub_" .. Title)
					
					local button = { -- The button to access the subcategory.
						str = but[1].str,
						dest = root,
						collectible = item,
						
						update = function(self, tbl)
							--Encyclopedia.RenderItemTooltip(self.collectible)
						end,
					}
					
					Encyclopedia.Encylopedia_dir[root] = { -- The subcategory contents.
						title = item.Name,
						
						collectible = item,
						update = function(self, tbl)
							Encyclopedia.RenderItemTooltip(self.collectible)
						end,
						
						buttons = but,
						
						fsize = 1,
						nocursor = true,
						scroller = true,
						halign = Encyclopedia.WikiAlignment - 2,
					}
					
					table.insert(dest.buttons, button)
				end
			else
				dest.fsize = 1
				dest.nocursor = true
				dest.scroller = true
				dest.halign = Encyclopedia.WikiAlignment - 2
				dest.buttons = item.WikiDesc[1]
			end
			
			table.insert(directorykey.Path, directorykey.Item)
			directorykey.Item = dest
		end
	end
	
	if input.back then
		Encyclopedia[Type .. "Table"][tostring(class) .. "Temp"] = nil
	end
end

function Encyclopedia.UpdateItems(Type, class)
	local Items = Encyclopedia[Type .. "Table"][class]
	local OldNavData = Encyclopedia.NavigationData[Type][class]
	Encyclopedia[Type .. "Table"][tostring(class) .. "Temp"] = {}
	
	for i, item in ipairs(Items) do
		if string.find(string.lower(item.Name), Encyclopedia.SearchText) then
			table.insert(Encyclopedia[Type .. "Table"][tostring(class) .. "Temp"], item)
		end
	end
	
	local TotalEntries = #Encyclopedia[Type .. "Table"][tostring(class) .. "Temp"]
	
	if TotalEntries > 0 then
		if OldNavData[2] > TotalEntries then
			local NewIndex = math.floor(TotalEntries / 5) * 5
			NewIndex = math.max(0, NewIndex - 10)
			local NewPos = TotalEntries
			local NewSlot = TotalEntries - NewIndex
		
			Encyclopedia.NavigationData[Type][class] = {NewSlot, NewPos, NewIndex}
		end
	else
		Encyclopedia.NavigationData[Type][class] = {1, 1, 0}
	end
	
	local CurTab = Encyclopedia[Type .. "Table"]
	local CurFilter = CurTab.Sorters[CurTab.CurrentFilter].func
	table.sort(Encyclopedia[Type .. "Table"][tostring(class) .. "Temp"], CurFilter)
end

function Encyclopedia.RenderPocketitems(Type, class)
	local Items = Encyclopedia.pocketitemsTable[Type][tostring(class) .. "Temp"] or Encyclopedia.pocketitemsTable[Type][tostring(class)]
	local NavData = Encyclopedia.NavigationData[Type][tostring(class)]
	local Slot, Pos, StartPoint = NavData[1], NavData[2], NavData[3]
	local CenterV = getScreenCenterPosition()
	local item = Items[NavData[2]]
	local menupal = dssmenu.GetPalette()
	local CurrentLocked = false
	
	local BoxV = CenterV + TooltipVecs.ScrollBar
    ScrollBarSpr[2].Color = menupal[1]
    ScrollBarSpr[3].Color = menupal[1]
	
	for i = 1, 4 do
		ScrollBarSpr[i]:Render(BoxV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	end
	
	local TotalSlides = math.ceil(#Items / 5)
	
	local CenterPos = 72.5
	local ScaleMod = 1 / math.max(1, TotalSlides)
	local ScaleOffset = 145 * ScaleMod
	local TopCap = -(145 - CenterPos)
	local BotCap = 145 - CenterPos - ScaleOffset
	local TargetPos = math.min(BotCap, TopCap + (ScaleOffset * math.floor(math.max(1, Pos - 1) / 5)))
	
	ScrollBarSpr2.Scale = Vector(1, ScaleMod)
	ScrollBarSpr2:Render(BoxV + Vector(0, (ScaleOffset / 2) + TargetPos), Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	
	for i = 1, 15 do
		local CurItem = Items[StartPoint + i]
		if not CurItem then break end
		
		local TargetV = CenterV + ItemVectors[i]
		
		ItemSlot.Color = menupal[1]
		ItemSlot:Render(TargetV + Encyclopedia.DoubleV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		
		local ItemSpr = Encyclopedia.GetCachedSprite(i, CurItem.Spr)
		
		ItemSpr.Color = Encyclopedia.ShadowColor
		ItemSpr:Render(TargetV + Encyclopedia.DoubleV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		ItemSpr:Update()
		
		local TempItem = {}
		local ItemLocked
		if CurItem.UnlockFunc then
			for i, v in pairs(CurItem) do
				TempItem[i] = CurItem[i]
			end
			
			TempItem.Name = "???"
			TempItem.Desc = "???"
			
			TempItem = TempItem:UnlockFunc()
			
			if TempItem then
				TempItem.TargetColor = TempItem.TargetColor or Encyclopedia.LockColor
				ItemLocked = true
			else
				TempItem = CurItem
				TempItem.TargetColor = TempItem.TargetColor or Encyclopedia.VanillaColor
			end
		else
			TempItem = CurItem
			TempItem.TargetColor = Encyclopedia.VanillaColor
		end

		ItemSpr.Color = TempItem.TargetColor
		ItemSpr:Render(TargetV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		
		if Slot == i then
			if ItemLocked then
				CurrentLocked = true
			end
		
			ItemSelector.Color = menupal[3]
			ItemSelector:Render(TargetV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
			Encyclopedia.RenderItemTooltip(TempItem, CurrentLocked)
		end
	end
	
	Encyclopedia.RenderText({str = Pos .. " / " .. #Items, size = 2, pos = Vector(-44, 90)})
	
	if Encyclopedia.SearchBoxOpen then return end
	
	local input = menuinput.menu
	if input.up then
		if Pos > 5 then
			if Slot > 5 then
				NavData[1] = Slot - 5
			else
				NavData[3] = StartPoint - 5
			end
			
			NavData[2] = Pos - 5
		end
	elseif input.down then
		if Pos <= #Items - 5 then
			if Slot <= 10 then
				NavData[1] = Slot + 5
			else
				NavData[3] = StartPoint + 5
				
				if Pos > #Items - 5 then
					NavData[1] = 10 + ((#Items - 1) % 5) + 1
				end
			end
			
			NavData[2] = math.min(#Items, Pos + 5)
		elseif (Pos - 1) % 5 > (#Items - 1) % 5 then
			local Difference = #Items - Pos
			if Slot <= 10 then
				NavData[1] = Slot + Difference
			else
				NavData[3] = StartPoint + 5
				
				if Pos > #Items - 5 then
					NavData[1] = Slot + Difference - 5
				end
			end
			
			NavData[2] = math.min(#Items, Pos + Difference)
		end
	elseif input.right then
		if Pos % 5 > 0 and Pos < #Items then
			NavData[1] = Slot + 1
			NavData[2] = Pos + 1
		end
	elseif input.left then
		if Pos % 5 ~= 1 then
			NavData[1] = Slot - 1
			NavData[2] = Pos - 1
		end
	end
	
	if input.confirm and not CurrentLocked then
		local dssmenu = DeadSeaScrollsMenu
		local tbl = dssmenu.OpenedMenu
		
		if not tbl then
			Isaac.DebugString("[Error] - DeadSeaScrollsMenu not initialized.")
			return
		end
		
		if item.WikiDesc then
			local directorykey = tbl.DirectoryKey
			local dest = Encyclopedia.Encylopedia_dir.pocketitemssub
			
			dest = {}
			dest.title = item.Name
			dest.collectible = item
			dest.update = function(self, tbl)
				Encyclopedia.RenderItemTooltip(self.collectible)
			end
			
			if #item.WikiDesc > 1 then
				dest.buttons = {}
				
				for i, but in ipairs(item.WikiDesc) do
					local Title = but[1].str
					local root = tostring("pocketitemssub_" .. Title)
					
					local button = { -- The button to access the subcategory.
						str = but[1].str,
						dest = root,
						collectible = item,
						
						update = function(self, tbl)
							--Encyclopedia.RenderItemTooltip(self.collectible)
						end,
					}
					
					Encyclopedia.Encylopedia_dir[root] = { -- The subcategory contents.
						title = item.Name,
						
						collectible = item,
						update = function(self, tbl)
							Encyclopedia.RenderItemTooltip(self.collectible)
						end,
						
						buttons = but,
						
						fsize = 1,
						nocursor = true,
						scroller = true,
						halign = Encyclopedia.WikiAlignment - 2,
					}
					
					table.insert(dest.buttons, button)
				end
			else
				dest.fsize = 1
				dest.nocursor = true
				dest.scroller = true
				dest.halign = Encyclopedia.WikiAlignment - 2
				dest.buttons = item.WikiDesc[1]
			end
			
			table.insert(directorykey.Path, directorykey.Item)
			directorykey.Item = dest
		end
	end
	
	if input.back then
		Encyclopedia.pocketitemsTable[Type][tostring(class) .. "Temp"] = nil
	end
end

function Encyclopedia.UpdatePocketItems(Type, class)
	local Items = Encyclopedia.pocketitemsTable[Type][class]
	local OldNavData = Encyclopedia.NavigationData[Type][class]
	Encyclopedia.pocketitemsTable[Type][tostring(class) .. "Temp"] = {}
	
	for i, item in ipairs(Items) do
		if string.find(string.lower(item.Name), Encyclopedia.SearchText) then
			table.insert(Encyclopedia.pocketitemsTable[Type][tostring(class) .. "Temp"], item)
		end
	end
	
	local TotalEntries = #Encyclopedia.pocketitemsTable[Type][tostring(class) .. "Temp"]
	
	if TotalEntries > 0 then
		if OldNavData[2] > TotalEntries then
			local NewIndex = math.floor(TotalEntries / 5) * 5
			NewIndex = math.max(0, NewIndex - 10)
			local NewPos = TotalEntries
			local NewSlot = TotalEntries - NewIndex
		
			Encyclopedia.NavigationData[Type][class] = {NewSlot, NewPos, NewIndex}
		end
	else
		Encyclopedia.NavigationData[Type][class] = {1, 1, 0}
	end
	
	local CurTab = Encyclopedia.pocketitemsTable[Type]
	local CurFilter = CurTab.Sorters[CurTab.CurrentFilter].func
	table.sort(Encyclopedia.pocketitemsTable[Type][tostring(class) .. "Temp"], CurFilter)
end

function Encyclopedia.RenderStatus()
	local Items = Encyclopedia.statusTable
	local NavData = Encyclopedia.NavigationData.status
	local Slot, Pos, StartPoint = NavData[1], NavData[2], NavData[3]
	local CenterV = getScreenCenterPosition()
	local item = Items[NavData[2]]
	local menupal = dssmenu.GetPalette()
	local CurrentLocked = false
	
	local BoxV = CenterV + TooltipVecs.ScrollBar
    ScrollBarSpr[2].Color = menupal[1]
    ScrollBarSpr[3].Color = menupal[1]
	
	for i = 1, 4 do
		ScrollBarSpr[i]:Render(BoxV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	end
	
	local TotalSlides = math.ceil(#Items / 5)
	
	local CenterPos = 72.5
	local ScaleMod = 1 / math.max(1, TotalSlides)
	local ScaleOffset = 145 * ScaleMod
	local TopCap = -(145 - CenterPos)
	local BotCap = 145 - CenterPos - ScaleOffset
	local TargetPos = math.min(BotCap, TopCap + (ScaleOffset * math.floor(math.max(1, Pos - 1) / 5)))
	
	ScrollBarSpr2.Scale = Vector(1, ScaleMod)
	ScrollBarSpr2:Render(BoxV + Vector(0, (ScaleOffset / 2) + TargetPos), Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	
	for i = 1, 15 do
		local CurItem = Items[StartPoint + i]
		if not CurItem then break end
		
		local TargetV = CenterV + ItemVectors[i]
		
		ItemSlot.Color = menupal[1]
		ItemSlot:Render(TargetV + Encyclopedia.DoubleV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		
		local ItemSpr = Encyclopedia.GetCachedSprite(i, CurItem.Spr)
		
		ItemSpr.Color = Encyclopedia.ShadowColor
		ItemSpr:Render(TargetV + Encyclopedia.DoubleV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		ItemSpr:Update()
		
		local TempItem = {}
		local ItemLocked
		if CurItem.UnlockFunc then
			for i, v in pairs(CurItem) do
				TempItem[i] = CurItem[i]
			end
			
			TempItem.Name = "???"
			TempItem.Desc = "???"
			
			TempItem = TempItem:UnlockFunc()
			
			if TempItem then
				TempItem.TargetColor = TempItem.TargetColor or Encyclopedia.LockColor
				ItemLocked = true
			else
				TempItem = CurItem
				TempItem.TargetColor = TempItem.TargetColor or Encyclopedia.VanillaColor
			end
		else
			TempItem = CurItem
			TempItem.TargetColor = Encyclopedia.VanillaColor
		end

		ItemSpr.Color = TempItem.TargetColor
		ItemSpr:Render(TargetV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		
		if Slot == i then
			if ItemLocked then
				CurrentLocked = true
			end
			
			ItemSelector.Color = menupal[3]
			ItemSelector:Render(TargetV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
			Encyclopedia.RenderItemTooltip(TempItem, locked)
		end
	end
	
	Encyclopedia.RenderText({str = Pos .. " / " .. #Items, size = 2, pos = Vector(-44, 90)})
	
	if Encyclopedia.SearchBoxOpen then return end
	
	local input = menuinput.menu
	if input.up then
		if Pos > 5 then
			if Slot > 5 then
				NavData[1] = Slot - 5
			else
				NavData[3] = StartPoint - 5
			end
			
			NavData[2] = Pos - 5
		end
	elseif input.down then
		if Pos <= #Items - 5 then
			if Slot <= 10 then
				NavData[1] = Slot + 5
			else
				NavData[3] = StartPoint + 5
				
				if Pos > #Items - 5 then
					NavData[1] = 10 + ((#Items - 1) % 5) + 1
				end
			end
			
			NavData[2] = math.min(#Items, Pos + 5)
		elseif (Pos - 1) % 5 > (#Items - 1) % 5 then
			local Difference = #Items - Pos
			if Slot <= 10 then
				NavData[1] = Slot + Difference
			else
				NavData[3] = StartPoint + 5
				
				if Pos > #Items - 5 then
					NavData[1] = Slot + Difference - 5
				end
			end
			
			NavData[2] = math.min(#Items, Pos + Difference)
		end
	elseif input.right then
		if Pos % 5 > 0 and Pos < #Items then
			NavData[1] = Slot + 1
			NavData[2] = Pos + 1
		end
	elseif input.left then
		if Pos % 5 ~= 1 then
			NavData[1] = Slot - 1
			NavData[2] = Pos - 1
		end
	end
	
	if input.confirm and not CurrentLocked then
		local dssmenu = DeadSeaScrollsMenu
		local tbl = dssmenu.OpenedMenu
		
		if not tbl then
			Isaac.DebugString("[Error] - DeadSeaScrollsMenu not initialized.")
			return
		end
		
		if item.WikiDesc then
			local directorykey = tbl.DirectoryKey
			local dest = Encyclopedia.Encylopedia_dir[item.typeString .. "sub"]
			
			dest = {}
			dest.title = item.Name
			dest.collectible = item
			dest.update = function(self, tbl)
				Encyclopedia.RenderItemTooltip(self.collectible)
			end
			
			if #item.WikiDesc > 1 then
				dest.buttons = {}
				
				for i, but in ipairs(item.WikiDesc) do
					local Title = but[1].str
					local root = tostring(item.typeString .. "sub_" .. Title)
					
					local button = { -- The button to access the subcategory.
						str = but[1].str,
						dest = root,
						collectible = item,
						
						update = function(self, tbl)
							--Encyclopedia.RenderItemTooltip(self.collectible)
						end,
					}
					
					Encyclopedia.Encylopedia_dir[root] = { -- The subcategory contents.
						title = item.Name,
						
						collectible = item,
						update = function(self, tbl)
							Encyclopedia.RenderItemTooltip(self.collectible)
						end,
						
						buttons = but,
						
						fsize = 1,
						nocursor = true,
						scroller = true,
						halign = Encyclopedia.WikiAlignment - 2,
					}
					
					table.insert(dest.buttons, button)
				end
			else
				dest.fsize = 1
				dest.nocursor = true
				dest.scroller = true
				dest.halign = Encyclopedia.WikiAlignment - 2
				dest.buttons = item.WikiDesc[1]
			end
			
			table.insert(directorykey.Path, directorykey.Item)
			directorykey.Item = dest
		end
	end
end

function Encyclopedia.RenderRoomItems()
	local Items = Encyclopedia.roomItemsTable
	local NavData = Encyclopedia.NavigationData.room
	local Slot, Pos, StartPoint = NavData[1], NavData[2], NavData[3]
	local CenterV = getScreenCenterPosition()
	local item = Items[NavData[2]]
	local menupal = dssmenu.GetPalette()
	local CurrentLocked = false
	
	local BoxV = CenterV + TooltipVecs.ScrollBar
    ScrollBarSpr[2].Color = menupal[1]
    ScrollBarSpr[3].Color = menupal[1]
	
	for i = 1, 4 do
		ScrollBarSpr[i]:Render(BoxV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	end
	
	local TotalSlides = math.ceil(#Items / 5)
	
	local CenterPos = 72.5
	local ScaleMod = 1 / math.max(1, TotalSlides)
	local ScaleOffset = 145 * ScaleMod
	local TopCap = -(145 - CenterPos)
	local BotCap = 145 - CenterPos - ScaleOffset
	local TargetPos = math.min(BotCap, TopCap + (ScaleOffset * math.floor(math.max(1, Pos - 1) / 5)))
	
	ScrollBarSpr2.Scale = Vector(1, ScaleMod)
	ScrollBarSpr2:Render(BoxV + Vector(0, (ScaleOffset / 2) + TargetPos), Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	
	for i = 1, 15 do
		local CurItem = Items[StartPoint + i]
		if not CurItem then break end
		
		local TargetV = CenterV + ItemVectors[i]
		
		ItemSlot.Color = menupal[1]
		ItemSlot:Render(TargetV + Encyclopedia.DoubleV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		
		local ItemSpr = Encyclopedia.GetCachedSprite(i, CurItem.Spr)
		ItemSpr.Color = Encyclopedia.ShadowColor
		ItemSpr:Render(TargetV + Encyclopedia.DoubleV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		ItemSpr:Update()
		
		local TempItem = {}
		local ItemLocked
		if CurItem.UnlockFunc then
			for i, v in pairs(CurItem) do
				TempItem[i] = CurItem[i]
			end
			
			TempItem.Name = "???"
			TempItem.Desc = "???"
			
			TempItem = TempItem:UnlockFunc()
			
			if TempItem then
				TempItem.TargetColor = TempItem.TargetColor or Encyclopedia.LockColor
				ItemLocked = true
			else
				TempItem = CurItem
				TempItem.TargetColor = TempItem.TargetColor or Encyclopedia.VanillaColor
			end
		else
			TempItem = CurItem
			TempItem.TargetColor = Encyclopedia.VanillaColor
		end

		ItemSpr.Color = TempItem.TargetColor
		ItemSpr:Render(TargetV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		
		if Slot == i then
			if ItemLocked then
				CurrentLocked = true
			end
			
			ItemSelector.Color = menupal[3]
			ItemSelector:Render(TargetV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
			Encyclopedia.RenderItemTooltip(TempItem, locked)
		end
	end
	
	Encyclopedia.RenderText({str = Pos .. " / " .. #Items, size = 2, pos = Vector(-44, 90)})
	
	if Encyclopedia.SearchBoxOpen then return end
	
	local input = menuinput.menu
	if input.up then
		if Pos > 5 then
			if Slot > 5 then
				NavData[1] = Slot - 5
			else
				NavData[3] = StartPoint - 5
			end
			
			NavData[2] = Pos - 5
		end
	elseif input.down then
		if Pos <= #Items - 5 then
			if Slot <= 10 then
				NavData[1] = Slot + 5
			else
				NavData[3] = StartPoint + 5
				
				if Pos > #Items - 5 then
					NavData[1] = 10 + ((#Items - 1) % 5) + 1
				end
			end
			
			NavData[2] = math.min(#Items, Pos + 5)
		elseif (Pos - 1) % 5 > (#Items - 1) % 5 then
			local Difference = #Items - Pos
			if Slot <= 10 then
				NavData[1] = Slot + Difference
			else
				NavData[3] = StartPoint + 5
				
				if Pos > #Items - 5 then
					NavData[1] = Slot + Difference - 5
				end
			end
			
			NavData[2] = math.min(#Items, Pos + Difference)
		end
	elseif input.right then
		if Pos % 5 > 0 and Pos < #Items then
			NavData[1] = Slot + 1
			NavData[2] = Pos + 1
		end
	elseif input.left then
		if Pos % 5 ~= 1 then
			NavData[1] = Slot - 1
			NavData[2] = Pos - 1
		end
	end
	
	if input.confirm and not CurrentLocked then
		local dssmenu = DeadSeaScrollsMenu
		local tbl = dssmenu.OpenedMenu
		
		if not tbl then
			Isaac.DebugString("[Error] - DeadSeaScrollsMenu not initialized.")
			return
		end
		
		if item.WikiDesc then
			local directorykey = tbl.DirectoryKey
			local dest = Encyclopedia.Encylopedia_dir[item.typeString .. "sub"]
			
			dest = {}
			dest.title = item.Name
			dest.collectible = item
			dest.update = function(self, tbl)
				Encyclopedia.RenderItemTooltip(self.collectible)
			end
			
			if #item.WikiDesc > 1 then
				dest.buttons = {}
				
				for i, but in ipairs(item.WikiDesc) do
					local Title = but[1].str
					local root = tostring(item.typeString .. "sub_" .. Title)
					
					local button = { -- The button to access the subcategory.
						str = but[1].str,
						dest = root,
						collectible = item,
						
						update = function(self, tbl)
							--Encyclopedia.RenderItemTooltip(self.collectible)
						end,
					}
					
					Encyclopedia.Encylopedia_dir[root] = { -- The subcategory contents.
						title = item.Name,
						
						collectible = item,
						update = function(self, tbl)
							Encyclopedia.RenderItemTooltip(self.collectible)
						end,
						
						buttons = but,
						
						fsize = 1,
						nocursor = true,
						scroller = true,
						halign = Encyclopedia.WikiAlignment - 2,
					}
					
					table.insert(dest.buttons, button)
				end
			else
				dest.fsize = 1
				dest.nocursor = true
				dest.scroller = true
				dest.halign = Encyclopedia.WikiAlignment - 2
				dest.buttons = item.WikiDesc[1]
			end
			
			table.insert(directorykey.Path, directorykey.Item)
			directorykey.Item = dest
		end
	end
end

function Encyclopedia.RenderCharacterTooltip(item, Type, locked)
	local TargetV = TooltipVecs.RightPageOffset + Vector(0, -56)
	local CenterV = getScreenCenterPosition()
	local Offset = 0
	local NameTab = Encyclopedia.fitTextToWidth(item.Name, 2, 92)
	local DescTab = item.Desc and Encyclopedia.fitTextToWidth(item.Desc, 1, 92)
	local TitleTab = item.Title and Encyclopedia.fitTextToWidth(item.Title, 1, 92)
	
	if TitleTab then
		Offset = Offset + (#TitleTab * 9)
	end
	
	Offset = Offset + (#NameTab * 12)
	
	if item.Desc then
		Offset = Offset + ((#DescTab - 1) * 9)
	end
	
	if locked then
		Offset = Offset + 42
	else
		Offset = Offset + 84
	end
	-- Render
	Offset = math.max(0, (95 - Offset) / 2)
	TargetV = TargetV + Vector(0, Offset)
	
	if TitleTab then
		for i, text in ipairs(TitleTab) do
			Encyclopedia.RenderText({str = text, size = 1, pos = TargetV + TooltipVecs.TextOffsetSmall})
			
			TargetV = TargetV + TooltipVecs.Vec9
		end
	end
	
	for i, text in ipairs(NameTab) do
		Encyclopedia.RenderText({str = text, size = 2, pos = TargetV + TooltipVecs.TextOffset})
		
		TargetV = TargetV + TooltipVecs.Vec12
	end
	
	if item.Desc then
		for i, text in ipairs(DescTab) do
			Encyclopedia.RenderText({str = text, size = 1, pos = TargetV + TooltipVecs.TextOffsetSmall})
			
			TargetV = TargetV + TooltipVecs.Vec9
		end
	end
	
	if locked then
		TargetV = TargetV + TooltipVecs.Vec20
		
		local ItemSpr = Encyclopedia.GetCachedSprite("ToolTip", item.Spr)
		ItemSpr.Color = Encyclopedia.ShadowColor
		ItemSpr:Render(CenterV + TargetV + Encyclopedia.DoubleV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		ItemSpr:Update()
		ItemSpr.Color = item.TargetColor or Encyclopedia.VanillaColor
		ItemSpr:Render(CenterV + TargetV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	else
		TargetV = TargetV + TooltipVecs.Vec40
		
		local TotalNotes = {}
		for i = 1, #item.CompletionTrackerFuncs do
			local Notes = item.CompletionTrackerFuncs[i](item, Type)
			
			for boss, tab in pairs(Notes) do
				TotalNotes[boss] = Notes[boss]
			end
		end
		
		for i = 1, #item.CompletionRenderFuncs do
			item.CompletionRenderFuncs[i](CenterV + TargetV, TotalNotes, Type)
		end
	end
end

local CharacterOffset = Vector(0, -5)
function Encyclopedia.RenderCharacters(Type, class)
	local Items = Encyclopedia[Type .. "Table"][class]
	local NavData = Encyclopedia.NavigationData[Type][class]
	local Slot, Pos, StartPoint = NavData[1], NavData[2], NavData[3]
	local CenterV = getScreenCenterPosition()
	local item = Items[NavData[2]]
	local menupal = dssmenu.GetPalette()
	local CurrentLocked = false
	
	local BoxV = CenterV + TooltipVecs.ScrollBar
    ScrollBarSpr[2].Color = menupal[1]
    ScrollBarSpr[3].Color = menupal[1]
	
	for i = 1, 4 do
		ScrollBarSpr[i]:Render(BoxV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	end
	
	local TotalSlides = math.ceil(#Items / 5)
	
	local CenterPos = 72.5
	local ScaleMod = 1 / math.max(1, TotalSlides)
	local ScaleOffset = 145 * ScaleMod
	local TopCap = -(145 - CenterPos)
	local BotCap = 145 - CenterPos - ScaleOffset
	local TargetPos = math.min(BotCap, TopCap + (ScaleOffset * math.floor(math.max(1, Pos - 1) / 5)))
	
	ScrollBarSpr2.Scale = Vector(1, ScaleMod)
	ScrollBarSpr2:Render(BoxV + Vector(0, (ScaleOffset / 2) + TargetPos), Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	
	for i = 1, 8 do
		local CurItem = Items[StartPoint + i]
		if not CurItem then break end
		
		local TargetV = CenterV + CharactersVector[i]
		
		ItemSlot.Color = menupal[1]
		ItemSlot:Render(TargetV + Encyclopedia.DoubleV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		
		local ItemSpr = Encyclopedia.GetCachedSprite(i, CurItem.Spr)
		ItemSpr.Color = Encyclopedia.ShadowColor
		ItemSpr:Render(TargetV + Encyclopedia.DoubleV + CharacterOffset, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		ItemSpr:Update()
		
		local TempItem = {}
		local ItemLocked
		if CurItem.UnlockFunc then
			for i, v in pairs(CurItem) do
				TempItem[i] = CurItem[i]
			end
			
			TempItem.Name = "???"
			TempItem.Desc = "???"
			
			TempItem = TempItem:UnlockFunc()
			
			if TempItem then
				TempItem.TargetColor = TempItem.TargetColor or Encyclopedia.LockColor
				ItemLocked = true
			else
				TempItem = CurItem
				TempItem.TargetColor = TempItem.TargetColor or Encyclopedia.VanillaColor
			end
		else
			TempItem = CurItem
			TempItem.TargetColor = Encyclopedia.VanillaColor
		end
		
		ItemSpr.Color = TempItem.TargetColor
		ItemSpr:Render(TargetV + CharacterOffset, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
		
		if Slot == i then
			if ItemLocked then
				CurrentLocked = true
			end
			
			ItemSelector2.Color = menupal[3]
			ItemSelector2:Render(TargetV + Encyclopedia.SingleV + CharacterOffset, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
			Encyclopedia.RenderCharacterTooltip(TempItem, string.gsub(Type, "characters_", ""), CurrentLocked)
		end
	end
	
	Encyclopedia.RenderText({str = Pos .. " / " .. #Items, size = 2, pos = Vector(-44, 90)})
	
	if Encyclopedia.SearchBoxOpen then return end
	
	local input = menuinput.menu
	if input.up then
		if Pos > 4 then
			if Slot > 4 then
				NavData[1] = Slot - 4
			else
				NavData[3] = StartPoint - 4
			end
			
			NavData[2] = Pos - 4
		end
	elseif input.down then
		if Pos <= #Items - 4 then
			if Slot <= 4 then
				NavData[1] = Slot + 4
			else
				NavData[3] = StartPoint + 4
				
				if Pos > #Items - 4 then
					NavData[1] = 4 + ((#Items - 1) % 4) + 1
				end
			end
			
			NavData[2] = math.min(#Items, Pos + 4)
		elseif (Pos - 1) % 4 > (#Items - 1) % 4 then
			local Difference = #Items - Pos
			if Slot <= 4 then
				NavData[1] = Slot + Difference
			else
				NavData[3] = StartPoint + 4
				
				if Pos > #Items - 4 then
					NavData[1] = Slot + Difference - 4
				end
			end
			
			NavData[2] = math.min(#Items, Pos + Difference)
		end
	elseif input.right then
		if Pos % 4 > 0 and Pos < #Items then
			NavData[1] = Slot + 1
			NavData[2] = Pos + 1
		end
	elseif input.left then
		if Pos % 4 ~= 1 then
			NavData[1] = Slot - 1
			NavData[2] = Pos - 1
		end
	end
	
	if input.confirm and not CurrentLocked then
		local dssmenu = DeadSeaScrollsMenu
		local tbl = dssmenu.OpenedMenu
		
		if not tbl then
			Isaac.DebugString("[Error] - DeadSeaScrollsMenu not initialized.")
			return
		end
		
		if item.WikiDesc then
			local directorykey = tbl.DirectoryKey
			local dest = Encyclopedia.Encylopedia_dir.characterssub
			local SubType = string.gsub(Type, "characters_", "")
		
			dest = {}
			dest.title = item.Name
			dest.collectible = item
			dest.Type = SubType
			dest.update = function(self, tbl)
				Encyclopedia.RenderCharacterTooltip(self.collectible, self.Type)
			end
			
			if #item.WikiDesc > 1 then
				dest.buttons = {}
				
				for i, but in ipairs(item.WikiDesc) do
					local Title = but[1].str
					local root = tostring("characterssub_" .. Title)
					
					local button = { -- The button to access the subcategory.
						str = but[1].str,
						dest = root,
						collectible = item,
						
						Type = SubType,
						update = function(self, tbl)
							--Encyclopedia.RenderCharacterTooltip(self.collectible, self.Type)
						end,
					}
					
					Encyclopedia.Encylopedia_dir[root] = { -- The subcategory contents.
						title = item.Name,
						
						collectible = item,
						
						Type = SubType,
						update = function(self, tbl)
							Encyclopedia.RenderCharacterTooltip(self.collectible, self.Type)
						end,
						
						buttons = but,
						
						fsize = 1,
						nocursor = true,
						scroller = true,
						halign = Encyclopedia.WikiAlignment - 2,
					}
					
					table.insert(dest.buttons, button)
				end
			else
				dest.fsize = 1
				dest.nocursor = true
				dest.scroller = true
				dest.halign = Encyclopedia.WikiAlignment - 2
				dest.buttons = item.WikiDesc[1]
			end
			
			table.insert(directorykey.Path, directorykey.Item)
			directorykey.Item = dest
		end
	end
end

local ItemPool = game:GetItemPool()
function Encyclopedia.UpdatePlayerStatus(search)
	local player = Isaac.GetPlayer(0)
	local Trinkets = {player:GetTrinket(0), player:GetTrinket(1)}
	local Cards = {player:GetCard(0), player:GetCard(1)}
	local Pills = {player:GetPill(0), player:GetPill(1)}
	local OldNavData = Encyclopedia.NavigationData.status
	--local TableTag = search and "statusTableTemp" or "statusTable"
	
	Encyclopedia.statusTable = {}
	
	local Counter = 0
	local CurrentCards = {}
	
	for i = 1, 4 do
		local Card = player:GetCard(i - 1)
		CurrentCards[i] = Card
		
		if Card > 0 then
			Counter = Counter + 1
		end
	end
	
	if Counter > 0 then
		for _, item in ipairs(Encyclopedia.pocketitemsTable.cards.globalcards) do
			local HasCard = false
			
			for _, card in ipairs(CurrentCards) do
				if card == item.ItemId then
					HasCard = true
					Counter = Counter - 1
					break
				end
			end
			
			if HasCard then
				local itemTable = {}
			
				if item.StatusFunc then
					for i, v in pairs(item) do
						itemTable[i] = item[i]
					end
					
					itemTable = itemTable:StatusFunc() or itemTable
				else
					itemTable = item
				end
				table.insert(Encyclopedia.statusTable, itemTable)
			end
			
			if Counter == 0 then
				break
			end
		end
	end
	
	local Counter = 0
	local CurrentPills = {}
	
	for i = 1, 4 do
		local Pill = player:GetPill(i - 1)
		
		if Pill > 0 then
			CurrentPills[i] = {Color = Pill, Effect = ItemPool:GetPillEffect(Pill)}
			Counter = Counter + 1
		else
			CurrentPills[i] = {Color = 1, Effect = 0}
		end
	end
	
	if Counter > 0 then
		for _, item in ipairs(Encyclopedia.pocketitemsTable.pills.allinternal) do
			local HasPill = false
			
			for _, pill in ipairs(CurrentPills) do
				if pill.Effect == item.ItemId then
					HasPill = pill.Color
					Counter = Counter - 1
					break
				end
			end
			
			if HasPill then
				local itemTable = {}
			
				if item.StatusFunc then
					for i, v in pairs(item) do
						itemTable[i] = item[i]
					end
					
					itemTable = itemTable:StatusFunc(HasPill) or itemTable
				else
					itemTable = item
				end
				if not ItemPool:IsPillIdentified(HasPill) then
					itemTable.Title = nil
					itemTable.Name = "Unidentified Pill"
					itemTable.Desc = "???"
					itemTable.WikiDesc = nil
				end
				table.insert(Encyclopedia.statusTable, itemTable)
				
				if Counter == 0 then
					break
				end
			end
		end
	end
	
	local CurrentIndex = #Encyclopedia.statusTable + 1
	for _, item in ipairs(Encyclopedia.trinketsTable.allinternal) do
		local HasTrinket = false
		if item.ItemId == Trinkets[1] or item.ItemId == Trinkets[2] then
			HasTrinket = 1
		elseif player:HasTrinket(item.ItemId) then
			HasTrinket = 2
		end
		
		if HasTrinket then
			local itemTable = {}
		
			if item.StatusFunc then
				for i, v in pairs(item) do
					itemTable[i] = item[i]
				end
				
				itemTable = itemTable:StatusFunc() or itemTable
			else
				itemTable = item
			end
			
			if HasTrinket == 1 then
				table.insert(Encyclopedia.statusTable, CurrentIndex, itemTable)
			else
				table.insert(Encyclopedia.statusTable, itemTable)
			end
		end
	end
	
	local Counter = 0
	local CurrentActives = {}
	
	if REPENTANCE then
		for i = 1, 6 do
			local Active = player:GetActiveItem(i - 1)
			CurrentActives[i] = Active
			
			if Active > 0 then
				Counter = Counter + 1
			end
		end
	else
		local MainActive = player:GetActiveItem(0)
		local SecondActive = player.SecondaryActiveItem.Item
		
		if MainActive ~= 0 then
			CurrentActives[#CurrentActives + 1] = MainActive
		end
		if SecondActive ~= 0 then
			CurrentActives[#CurrentActives + 1] = SecondActive
		end
	end
	
	for _, item in ipairs(Encyclopedia.itemsTable.allinternal) do
		if player:HasCollectible(item.ItemId) then
			local itemTable = {}
		
			if item.StatusFunc then
				for i, v in pairs(item) do
					itemTable[i] = item[i]
				end
				
				itemTable = itemTable:StatusFunc() or itemTable
			else
				itemTable = item
			end
			
			local IsActive = false
			if Counter > 0 then
				for _, active in ipairs(CurrentActives) do
					if active == item.ItemId then
						IsActive = true
						table.insert(Encyclopedia.statusTable, CurrentIndex, itemTable)
					end
				end
			end
			
			if not IsActive then
				table.insert(Encyclopedia.statusTable, itemTable)
			end
		end
	end
	
	-- Search bar stuff.
	local Items = Encyclopedia.statusTable
	local OldNavData = Encyclopedia.NavigationData.status
	Encyclopedia.statusTable = {}
	
	for i, item in ipairs(Items) do
		if string.find(string.lower(item.Name), Encyclopedia.SearchText) then
			table.insert(Encyclopedia.statusTable, item)
		end
	end
	
	local TotalEntries = #Encyclopedia.statusTable
	
	if TotalEntries > 0 then
		if OldNavData[2] > TotalEntries then
			local NewIndex = math.floor(TotalEntries / 5) * 5
			NewIndex = math.max(0, NewIndex - 10)
			local NewPos = TotalEntries
			local NewSlot = TotalEntries - NewIndex
		
			Encyclopedia.NavigationData.status = {NewSlot, NewPos, NewIndex}
		end
	else
		Encyclopedia.NavigationData.status = {1, 1, 0}
	end
end

local questionMarkSprite = Sprite()
questionMarkSprite:Load("gfx/005.100_collectible.anm2", true)
questionMarkSprite:ReplaceSpritesheet(1, "gfx/items/collectibles/questionmark.png")
questionMarkSprite:LoadGraphics()
local function IsAltChoice(pickup)
	if pickup:GetData() == nil then
		return false
	end
	
	if EID and EID:getEntityData(pickup, "EID_IsAltChoice") ~= nil then
		return EID:getEntityData(pickup, "EID_IsAltChoice")
	end

	if not REPENTANCE or game:GetLevel():GetStageType() < 4 or game:GetRoom():GetType() ~= RoomType.ROOM_TREASURE then
		pickup:GetData()["EID_IsAltChoice"] = false
		return false
	end

	local entitySprite = pickup:GetSprite()
	local name = entitySprite:GetAnimation()

	if name ~= "Idle" and name ~= "ShopIdle" then
		-- Collectible can be ignored. its definetly not hidden
		pickup:GetData()["EID_IsAltChoice"] = false
		return false
	end
	
	questionMarkSprite:SetFrame(name, entitySprite:GetFrame())
	-- check some point in entitySprite
	for i = -70, 0, 2 do
		local qcolor = questionMarkSprite:GetTexel(Vector(0, i), Encyclopedia.ZeroV, 1, 1)
		local ecolor = entitySprite:GetTexel(Vector(0, i), Encyclopedia.ZeroV, 1, 1)
		if qcolor.Red ~= ecolor.Red or qcolor.Green ~= ecolor.Green or qcolor.Blue ~= ecolor.Blue then
			-- it is not same with question mark sprite
			pickup:GetData()["EID_IsAltChoice"] = false
			return false
		end
	end

	--this may be a question mark, however, we will check it again to ensure it
	for j = -3, 3, 2 do
		for i = -71, 0, 2 do
			local qcolor = questionMarkSprite:GetTexel(Vector(j, i), Encyclopedia.ZeroV, 1, 1)
			local ecolor = entitySprite:GetTexel(Vector(j, i), Encyclopedia.ZeroV, 1, 1)
			if qcolor.Red ~= ecolor.Red or qcolor.Green ~= ecolor.Green or qcolor.Blue ~= ecolor.Blue then
				pickup:GetData()["EID_IsAltChoice"] = false
				return false
			end
		end
	end
	pickup:GetData()["EID_IsAltChoice"] = true
	return true
end

local function isBlindPickup(pickup)
	return (game:GetLevel():GetCurses() & LevelCurse.CURSE_OF_BLIND ~= 0 and not pickup.Touched) or (not pickup.Touched and IsAltChoice(pickup)) or (game.Challenge == Challenge.CHALLENGE_APRILS_FOOL)
end

function Encyclopedia.UpdateRoomItems(search)
	local Items = Isaac.FindByType(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, -1)
	local Trinkets = Isaac.FindByType(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TRINKET, -1)
	local Cards = Isaac.FindByType(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TAROTCARD, -1)
	local Pills = Isaac.FindByType(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_PILL, -1)
	local OldNavData = Encyclopedia.NavigationData.room
	
	Encyclopedia.roomItemsTable = {}
	
	for _, card in ipairs(Cards) do
		if not card:ToPickup():IsShopItem() then
			for _, item in ipairs(Encyclopedia.pocketitemsTable.cards.globalcards) do
				if card.SubType == item.ItemId then
					local itemTable = {}
				
					if item.StatusFunc then
						for i, v in pairs(item) do
							itemTable[i] = item[i]
						end
						
						itemTable = itemTable:StatusFunc() or itemTable
					else
						itemTable = item
					end
					
					table.insert(Encyclopedia.roomItemsTable, itemTable)
					break
				end
			end
		end
	end
	
	for _, pill in ipairs(Pills) do
		local pillEffect = ItemPool:GetPillEffect(pill.SubType)
		
		for _, item in ipairs(Encyclopedia.pocketitemsTable.pills.allinternal) do
			if pillEffect == item.ItemId then
				local itemTable = {}
			
				if item.StatusFunc then
					for i, v in pairs(item) do
						itemTable[i] = item[i]
					end
					
					itemTable = itemTable:StatusFunc(pill.SubType) or itemTable
				else
					itemTable = item
				end
				if not ItemPool:IsPillIdentified(pill.SubType) then
					itemTable.Title = nil
					itemTable.Name = "Unidentified Pill"
					itemTable.Desc = "???"
					itemTable.WikiDesc = nil
				end
				table.insert(Encyclopedia.roomItemsTable, itemTable)
			end
		end
	end
	
	for _, trinket in ipairs(Trinkets) do
		for _, item in ipairs(Encyclopedia.trinketsTable.allinternal) do
			if trinket.SubType == item.ItemId then
				local itemTable = {}
			
				if item.StatusFunc then
					for i, v in pairs(item) do
						itemTable[i] = item[i]
					end
					
					itemTable = itemTable:StatusFunc() or itemTable
				else
					itemTable = item
				end
				
				table.insert(Encyclopedia.roomItemsTable, itemTable)
				break
			end
		end
	end
	
	for _, coll in ipairs(Items) do
		if not isBlindPickup(coll) then
			for _, item in ipairs(Encyclopedia.itemsTable.allinternal) do
				if coll.SubType == item.ItemId then
					local itemTable = {}
				
					if item.StatusFunc then
						for i, v in pairs(item) do
							itemTable[i] = item[i]
						end
						
						itemTable = itemTable:StatusFunc() or itemTable
					else
						itemTable = item
					end
					
					table.insert(Encyclopedia.roomItemsTable, itemTable)
					break
				end
			end
		end
	end
	
	-- Search bar stuff.
	local Items = Encyclopedia.roomItemsTable
	local OldNavData = Encyclopedia.NavigationData.room
	Encyclopedia.roomItemsTable = {}
	
	for i, item in ipairs(Items) do
		if string.find(string.lower(item.Name), Encyclopedia.SearchText) then
			table.insert(Encyclopedia.roomItemsTable, item)
		end
	end
	
	local TotalEntries = #Encyclopedia.roomItemsTable
	
	if TotalEntries > 0 then
		if OldNavData[2] > TotalEntries then
			local NewIndex = math.floor(TotalEntries / 5) * 5
			NewIndex = math.max(0, NewIndex - 10)
			local NewPos = TotalEntries
			local NewSlot = TotalEntries - NewIndex
		
			Encyclopedia.NavigationData.room = {NewSlot, NewPos, NewIndex}
		end
	else
		Encyclopedia.NavigationData.room = {1, 1, 0}
	end
end

Encyclopedia.AddedInputCallback = false
function Encyclopedia:InputHandler(player, hook, action)
	if not Encyclopedia.AddedInputCallback then return nil end

	if Encyclopedia.SearchBoxOpen then
		if action == ButtonAction.ACTION_MENUBACK
		or action == ButtonAction.ACTION_PAUSE
		or action == ButtonAction.ACTION_RESTART
		or action == ButtonAction.ACTION_FULLSCREEN
		or action == ButtonAction.ACTION_MUTE then
			return false
		end
	end
end
Encyclopedia:AddCallback(ModCallbacks.MC_INPUT_ACTION, Encyclopedia.InputHandler, InputHook.IS_ACTION_TRIGGERED)

local SearchBoxSpr = {
	Encyclopedia.CreateSprite("gfx/ui/deadseascrolls/menu_searchbar.anm2", "Idle", 0, "gfx/ui/deadseascrolls/menu_searchbar_shadow.png"),
	Encyclopedia.CreateSprite("gfx/ui/deadseascrolls/menu_searchbar.anm2", "Idle", 0, "gfx/ui/deadseascrolls/menu_searchbar_back.png"),
	Encyclopedia.CreateSprite("gfx/ui/deadseascrolls/menu_searchbar.anm2", "Idle", 0, "gfx/ui/deadseascrolls/menu_searchbar_face.png"),
	Encyclopedia.CreateSprite("gfx/ui/deadseascrolls/menu_searchbar.anm2", "Idle", 0, "gfx/ui/deadseascrolls/menu_searchbar_border.png"),
}

local SortersSpr = {
	Encyclopedia.CreateSprite("gfx/ui/deadseascrolls/menu_sorters.anm2", "Idle", 0, "gfx/ui/deadseascrolls/menu_sorters_shadow.png"),
	Encyclopedia.CreateSprite("gfx/ui/deadseascrolls/menu_sorters.anm2", "Idle", 0, "gfx/ui/deadseascrolls/menu_sorters_back.png", true),
	Encyclopedia.CreateSprite("gfx/ui/deadseascrolls/menu_sorters.anm2", "Idle", 0, "gfx/ui/deadseascrolls/menu_sorters_face.png", true),
	Encyclopedia.CreateSprite("gfx/ui/deadseascrolls/menu_sorters.anm2", "Idle", 0, "gfx/ui/deadseascrolls/menu_sorters_border.png"),
}

local BackspaceTimer = -15
local ValidKeys = {
	[Keyboard.KEY_SPACE] = " ",
	[Keyboard.KEY_APOSTROPHE] = "'",
	[Keyboard.KEY_COMMA] = ",",
	[Keyboard.KEY_MINUS] = "-",
	[Keyboard.KEY_PERIOD] = ".",
	[Keyboard.KEY_SLASH] = "/",
	[Keyboard.KEY_0] = "0",
	[Keyboard.KEY_1] = "1",
	[Keyboard.KEY_2] = "2",
	[Keyboard.KEY_3] = "3",
	[Keyboard.KEY_4] = "4",
	[Keyboard.KEY_5] = "5",
	[Keyboard.KEY_6] = "6",
	[Keyboard.KEY_7] = "7",
	[Keyboard.KEY_8] = "8",
	[Keyboard.KEY_9] = "9",
	[Keyboard.KEY_SEMICOLON] = ";",
	[Keyboard.KEY_EQUAL] = "=",
	[Keyboard.KEY_A] = "a",
	[Keyboard.KEY_B] = "b",
	[Keyboard.KEY_C] = "c",
	[Keyboard.KEY_D] = "d",
	[Keyboard.KEY_E] = "e",
	[Keyboard.KEY_F] = "f",
	[Keyboard.KEY_G] = "g",
	[Keyboard.KEY_H] = "h",
	[Keyboard.KEY_I] = "i",
	[Keyboard.KEY_J] = "j",
	[Keyboard.KEY_K] = "k",
	[Keyboard.KEY_L] = "l",
	[Keyboard.KEY_M] = "m",
	[Keyboard.KEY_N] = "n",
	[Keyboard.KEY_O] = "o",
	[Keyboard.KEY_P] = "p",
	[Keyboard.KEY_Q] = "q",
	[Keyboard.KEY_R] = "r",
	[Keyboard.KEY_S] = "s",
	[Keyboard.KEY_T] = "t",
	[Keyboard.KEY_U] = "u",
	[Keyboard.KEY_V] = "v",
	[Keyboard.KEY_W] = "w",
	[Keyboard.KEY_X] = "x",
	[Keyboard.KEY_Y] = "y",
	[Keyboard.KEY_Z] = "z",
	[Keyboard.KEY_LEFT_BRACKET] = "[",
	[Keyboard.KEY_RIGHT_BRACKET] = "]",
	[Keyboard.KEY_GRAVE_ACCENT] = "`",
	[Keyboard.KEY_KP_0] = "0",
	[Keyboard.KEY_KP_1] = "1",
	[Keyboard.KEY_KP_2] = "2",
	[Keyboard.KEY_KP_3] = "3",
	[Keyboard.KEY_KP_4] = "4",
	[Keyboard.KEY_KP_5] = "5",
	[Keyboard.KEY_KP_6] = "6",
	[Keyboard.KEY_KP_7] = "7",
	[Keyboard.KEY_KP_8] = "8",
	[Keyboard.KEY_KP_9] = "9",
	[Keyboard.KEY_KP_DECIMAL] = ".",
	[Keyboard.KEY_KP_DIVIDE] = "/",
	[Keyboard.KEY_KP_MULTIPLY] = "*",
	[Keyboard.KEY_KP_SUBTRACT] = "-",
	[Keyboard.KEY_KP_ADD] = "+",
	[Keyboard.KEY_KP_EQUAL] = "=",
}
function Encyclopedia.UseItemsSearchBox(Type, class)
	local CenterV = getScreenCenterPosition()
	local TargetV = CenterV + TooltipVecs.RightPageOffset + TooltipVecs.SearchText
	local BoxV = CenterV + TooltipVecs.RightPageOffset + TooltipVecs.SearchBox
	local CtrlClick
	local paused = game:IsPaused()
	local menupal = dssmenu.GetPalette()
    SearchBoxSpr[2].Color = menupal[1]
    SearchBoxSpr[3].Color = menupal[1]
	
	for i = 1, 4 do
		SearchBoxSpr[i]:Render(BoxV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	end
	
	if not Encyclopedia.KeyboardIndex then
		local pIndex = Isaac.GetPlayer(0).ControllerIndex
		CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_CONTROL, pIndex)
	
		if not CtrlClick then
			for i = 0, 4 do
				CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_CONTROL, i)

				if CtrlClick then
					Encyclopedia.KeyboardIndex = i
					break
				end
			end
		else
			Encyclopedia.KeyboardIndex = pIndex
		end
	else
		CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_CONTROL, Encyclopedia.KeyboardIndex)
	end
	
	if CtrlClick and not paused then
		Encyclopedia.SearchBoxOpen = not Encyclopedia.SearchBoxOpen
		
		if Encyclopedia.SearchBoxOpen == false then
			Encyclopedia.AddedInputCallback = false
		end
	end
	
	if Encyclopedia.SearchBoxOpen then
		if not Encyclopedia.AddedInputCallback then
			Encyclopedia.AddedInputCallback = true
		end
	
		for key, chr in pairs(ValidKeys) do
			if Input.IsButtonTriggered(key, Encyclopedia.KeyboardIndex) then
				Isaac.GetPlayer(0):GetData().LastResetPress = 0
				Encyclopedia.SearchText = Encyclopedia.SearchText .. chr
				Encyclopedia.UpdateItems(Type, class)
			end
		end
		
		if Input.IsButtonPressed(Keyboard.KEY_BACKSPACE, Encyclopedia.KeyboardIndex) then
			if BackspaceTimer == -15 or (BackspaceTimer > 0 and BackspaceTimer % 5 == 0) then
				Encyclopedia.SearchText = string.sub(Encyclopedia.SearchText, 1, string.len(Encyclopedia.SearchText) - 1)
				Encyclopedia.UpdateItems(Type, class)
			end
			BackspaceTimer = BackspaceTimer + 1
		else
			BackspaceTimer = -15
		end
	
		local String = Encyclopedia.SearchText
		
		TempestaFont:DrawString(String, TargetV.X, TargetV.Y, KColor(1, 1, 1, 1), 0, true)
		
		if game:GetFrameCount() % 20 > 10 and not paused then
			local Offset = TempestaFont:GetStringWidthUTF8(String)
			
			TempestaFont:DrawString("|", TargetV.X + Offset, TargetV.Y, KColor(1, 1, 1, 1), 0, true)
		end
	else
		local String = "[LCtrl] - Search"
		
		if Encyclopedia.SearchText ~= "" then
			String = Encyclopedia.SearchText
		end
		
		TempestaFont:DrawString(String, TargetV.X, TargetV.Y, KColor(1, 1, 1, 0.5), 0, true)
	end
end

function Encyclopedia.UsePocketitemsSearchBox(Type, class)
	local CenterV = getScreenCenterPosition()
	local TargetV = CenterV + TooltipVecs.RightPageOffset + TooltipVecs.SearchText
	local BoxV = CenterV + TooltipVecs.RightPageOffset + TooltipVecs.SearchBox
	local CtrlClick
	local paused = game:IsPaused()
	local menupal = dssmenu.GetPalette()
    SearchBoxSpr[2].Color = menupal[1]
    SearchBoxSpr[3].Color = menupal[1]
	
	for i = 1, 4 do
		SearchBoxSpr[i]:Render(BoxV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	end
	
	if not Encyclopedia.KeyboardIndex then
		local pIndex = Isaac.GetPlayer(0).ControllerIndex
		CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_CONTROL, pIndex)
	
		if not CtrlClick then
			for i = 0, 4 do
				CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_CONTROL, i)

				if CtrlClick then
					Encyclopedia.KeyboardIndex = i
					break
				end
			end
		else
			Encyclopedia.KeyboardIndex = pIndex
		end
	else
		CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_CONTROL, Encyclopedia.KeyboardIndex)
	end
	
	if CtrlClick and not paused then
		Encyclopedia.SearchBoxOpen = not Encyclopedia.SearchBoxOpen
		
		if Encyclopedia.SearchBoxOpen == false then
			Encyclopedia.AddedInputCallback = false
		end
	end
	
	if Encyclopedia.SearchBoxOpen then
		if not Encyclopedia.AddedInputCallback then
			Encyclopedia.AddedInputCallback = true
		end
	
		for key, chr in pairs(ValidKeys) do
			if Input.IsButtonTriggered(key, Encyclopedia.KeyboardIndex) then
				Isaac.GetPlayer(0):GetData().LastResetPress = 0
				Encyclopedia.SearchText = Encyclopedia.SearchText .. chr
				Encyclopedia.UpdatePocketItems(Type, class)
			end
		end
		
		if Input.IsButtonPressed(Keyboard.KEY_BACKSPACE, Encyclopedia.KeyboardIndex) then
			if BackspaceTimer == -15 or (BackspaceTimer > 0 and BackspaceTimer % 5 == 0) then
				Encyclopedia.SearchText = string.sub(Encyclopedia.SearchText, 1, string.len(Encyclopedia.SearchText) - 1)
				Encyclopedia.UpdatePocketItems(Type, class)
			end
			BackspaceTimer = BackspaceTimer + 1
		else
			BackspaceTimer = -15
		end
	
		local String = Encyclopedia.SearchText
		
		TempestaFont:DrawString(String, TargetV.X, TargetV.Y, KColor(1, 1, 1, 1), 0, true)
		
		if game:GetFrameCount() % 20 > 10 and not paused then
			local Offset = TempestaFont:GetStringWidthUTF8(String)
			
			TempestaFont:DrawString("|", TargetV.X + Offset, TargetV.Y, KColor(1, 1, 1, 1), 0, true)
		end
	else
		local String = "[LCtrl] - Search"
		
		if Encyclopedia.SearchText ~= "" then
			String = Encyclopedia.SearchText
		end
		
		TempestaFont:DrawString(String, TargetV.X, TargetV.Y, KColor(1, 1, 1, 0.5), 0, true)
	end
end

function Encyclopedia.UseStatusSearchBox()
	local CenterV = getScreenCenterPosition()
	local TargetV = CenterV + TooltipVecs.RightPageOffset + TooltipVecs.SearchText
	local BoxV = CenterV + TooltipVecs.RightPageOffset + TooltipVecs.SearchBox
	local CtrlClick
	local paused = game:IsPaused()
	local menupal = dssmenu.GetPalette()
    SearchBoxSpr[2].Color = menupal[1]
    SearchBoxSpr[3].Color = menupal[1]
	
	for i = 1, 4 do
		SearchBoxSpr[i]:Render(BoxV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	end
	
	if not Encyclopedia.KeyboardIndex then
		local pIndex = Isaac.GetPlayer(0).ControllerIndex
		CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_CONTROL, pIndex)
	
		if not CtrlClick then
			for i = 0, 4 do
				CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_CONTROL, i)

				if CtrlClick then
					Encyclopedia.KeyboardIndex = i
					break
				end
			end
		else
			Encyclopedia.KeyboardIndex = pIndex
		end
	else
		CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_CONTROL, Encyclopedia.KeyboardIndex)
	end
	
	if CtrlClick and not paused then
		Encyclopedia.SearchBoxOpen = not Encyclopedia.SearchBoxOpen
		
		if Encyclopedia.SearchBoxOpen == false then
			Encyclopedia.AddedInputCallback = false
		end
	end
	
	if Encyclopedia.SearchBoxOpen then
		if not Encyclopedia.AddedInputCallback then
			Encyclopedia.AddedInputCallback = true
		end
	
		for key, chr in pairs(ValidKeys) do
			if Input.IsButtonTriggered(key, Encyclopedia.KeyboardIndex) then
				Isaac.GetPlayer(0):GetData().LastResetPress = 0
				Encyclopedia.SearchText = Encyclopedia.SearchText .. chr
				Encyclopedia.UpdatePlayerStatus(true)
			end
		end
		
		if Input.IsButtonPressed(Keyboard.KEY_BACKSPACE, Encyclopedia.KeyboardIndex) then
			if BackspaceTimer == -15 or (BackspaceTimer > 0 and BackspaceTimer % 5 == 0) then
				Encyclopedia.SearchText = string.sub(Encyclopedia.SearchText, 1, string.len(Encyclopedia.SearchText) - 1)
				Encyclopedia.UpdatePlayerStatus(true)
			end
			BackspaceTimer = BackspaceTimer + 1
		else
			BackspaceTimer = -15
		end
	
		local String = Encyclopedia.SearchText
		
		TempestaFont:DrawString(String, TargetV.X, TargetV.Y, KColor(1, 1, 1, 1), 0, true)
		
		if game:GetFrameCount() % 20 > 10 and not paused then
			local Offset = TempestaFont:GetStringWidthUTF8(String)
			
			TempestaFont:DrawString("|", TargetV.X + Offset, TargetV.Y, KColor(1, 1, 1, 1), 0, true)
		end
	else
		local String = "[LCtrl] - Search"
		
		if Encyclopedia.SearchText ~= "" then
			String = Encyclopedia.SearchText
		end
		
		TempestaFont:DrawString(String, TargetV.X, TargetV.Y, KColor(1, 1, 1, 0.5), 0, true)
	end
end

function Encyclopedia.UseRoomItemsSearchBox()
	local CenterV = getScreenCenterPosition()
	local TargetV = CenterV + TooltipVecs.RightPageOffset + TooltipVecs.SearchText
	local BoxV = CenterV + TooltipVecs.RightPageOffset + TooltipVecs.SearchBox
	local CtrlClick
	local paused = game:IsPaused()
	local menupal = dssmenu.GetPalette()
    SearchBoxSpr[2].Color = menupal[1]
    SearchBoxSpr[3].Color = menupal[1]
	
	for i = 1, 4 do
		SearchBoxSpr[i]:Render(BoxV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	end
	
	if not Encyclopedia.KeyboardIndex then
		local pIndex = Isaac.GetPlayer(0).ControllerIndex
		CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_CONTROL, pIndex)
	
		if not CtrlClick then
			for i = 0, 4 do
				CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_CONTROL, i)

				if CtrlClick then
					Encyclopedia.KeyboardIndex = i
					break
				end
			end
		else
			Encyclopedia.KeyboardIndex = pIndex
		end
	else
		CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_CONTROL, Encyclopedia.KeyboardIndex)
	end
	
	if CtrlClick and not paused then
		Encyclopedia.SearchBoxOpen = not Encyclopedia.SearchBoxOpen
		
		if Encyclopedia.SearchBoxOpen == false then
			Encyclopedia.AddedInputCallback = false
		end
	end
	
	if Encyclopedia.SearchBoxOpen then
		if not Encyclopedia.AddedInputCallback then
			Encyclopedia.AddedInputCallback = true
		end
	
		for key, chr in pairs(ValidKeys) do
			if Input.IsButtonTriggered(key, Encyclopedia.KeyboardIndex) then
				Isaac.GetPlayer(0):GetData().LastResetPress = 0
				Encyclopedia.SearchText = Encyclopedia.SearchText .. chr
				Encyclopedia.UpdateRoomItems(true)
			end
		end
		
		if Input.IsButtonPressed(Keyboard.KEY_BACKSPACE, Encyclopedia.KeyboardIndex) then
			if BackspaceTimer == -15 or (BackspaceTimer > 0 and BackspaceTimer % 5 == 0) then
				Encyclopedia.SearchText = string.sub(Encyclopedia.SearchText, 1, string.len(Encyclopedia.SearchText) - 1)
				Encyclopedia.UpdateRoomItems(true)
			end
			BackspaceTimer = BackspaceTimer + 1
		else
			BackspaceTimer = -15
		end
	
		local String = Encyclopedia.SearchText
		
		TempestaFont:DrawString(String, TargetV.X, TargetV.Y, KColor(1, 1, 1, 1), 0, true)
		
		if game:GetFrameCount() % 20 > 10 and not paused then
			local Offset = TempestaFont:GetStringWidthUTF8(String)
			
			TempestaFont:DrawString("|", TargetV.X + Offset, TargetV.Y, KColor(1, 1, 1, 1), 0, true)
		end
	else
		local String = "[LCtrl] - Search"
		
		if Encyclopedia.SearchText ~= "" then
			String = Encyclopedia.SearchText
		end
		
		TempestaFont:DrawString(String, TargetV.X, TargetV.Y, KColor(1, 1, 1, 0.5), 0, true)
	end
end

function Encyclopedia.UseItemsSorters(Type, class)
	local CurTab = Encyclopedia[Type .. "Table"]
	local CenterV = getScreenCenterPosition()
	local TargetV = CenterV + TooltipVecs.RightPageOffset + TooltipVecs.SortersText
	local BoxV = CenterV + TooltipVecs.RightPageOffset + TooltipVecs.SortersBox
	local CtrlClick
	local paused = game:IsPaused()
	local menupal = dssmenu.GetPalette()
    SortersSpr[2].Color = menupal[1]
    SortersSpr[3].Color = menupal[1]
	
	for i = 1, 4 do
		SortersSpr[i]:SetFrame("Idle", CurTab.Sorters[CurTab.CurrentFilter].ID - 1)
		SortersSpr[i]:Render(BoxV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	end
	
	if not Encyclopedia.KeyboardIndex then
		local pIndex = Isaac.GetPlayer(0).ControllerIndex
		CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_SHIFT, pIndex)
	
		if not CtrlClick then
			for i = 0, 4 do
				CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_SHIFT, i)

				if CtrlClick then
					Encyclopedia.KeyboardIndex = i
					break
				end
			end
		else
			Encyclopedia.KeyboardIndex = pIndex
		end
	else
		CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_SHIFT, Encyclopedia.KeyboardIndex)
	end
	
	if CtrlClick and not paused then
		if CurTab.Sorters[CurTab.CurrentFilter + 1] == nil then
			CurTab.CurrentFilter = 1
		else
			CurTab.CurrentFilter = CurTab.CurrentFilter + 1
		end
		
		local CurFilter = CurTab.Sorters[CurTab.CurrentFilter].func
		for i, v in pairs(CurTab) do
			if type(v) == "table" and i ~= "Sorters" then
				table.sort(Encyclopedia[Type .. "Table"][i], CurFilter)
			end
		end
	end
		
	TempestaFont:DrawString("[LShift] - Sort", TargetV.X, TargetV.Y, KColor(1, 1, 1, 0.5), 0, true)
end

function Encyclopedia.UsePocketitemsSorters(Type, class)
	local CurTab = Encyclopedia.pocketitemsTable[Type]
	local CenterV = getScreenCenterPosition()
	local TargetV = CenterV + TooltipVecs.RightPageOffset + TooltipVecs.SortersText
	local BoxV = CenterV + TooltipVecs.RightPageOffset + TooltipVecs.SortersBox
	local CtrlClick
	local paused = game:IsPaused()
	local menupal = dssmenu.GetPalette()
    SortersSpr[2].Color = menupal[1]
    SortersSpr[3].Color = menupal[1]
	
	for i = 1, 4 do
		SortersSpr[i]:SetFrame("Idle", CurTab.Sorters[CurTab.CurrentFilter].ID - 1)
		SortersSpr[i]:Render(BoxV, Encyclopedia.ZeroV, Encyclopedia.ZeroV)
	end
	
	if not Encyclopedia.KeyboardIndex then
		local pIndex = Isaac.GetPlayer(0).ControllerIndex
		CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_SHIFT, pIndex)
	
		if not CtrlClick then
			for i = 0, 4 do
				CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_SHIFT, i)

				if CtrlClick then
					Encyclopedia.KeyboardIndex = i
					break
				end
			end
		else
			Encyclopedia.KeyboardIndex = pIndex
		end
	else
		CtrlClick = SafeKeyboardTriggered(Keyboard.KEY_LEFT_SHIFT, Encyclopedia.KeyboardIndex)
	end
	
	if CtrlClick and not paused then
		if CurTab.Sorters[CurTab.CurrentFilter + 1] == nil then
			CurTab.CurrentFilter = 1
		else
			CurTab.CurrentFilter = CurTab.CurrentFilter + 1
		end
		
		local CurFilter = CurTab.Sorters[CurTab.CurrentFilter].func
		for i, v in pairs(CurTab) do
			if type(v) == "table" and i ~= "Sorters" then
				table.sort(Encyclopedia.pocketitemsTable[Type][i], CurFilter)
			end
		end
	end
		
	TempestaFont:DrawString("[LShift] - Sort", TargetV.X, TargetV.Y, KColor(1, 1, 1, 0.5), 0, true)
end

Encyclopedia.Encylopedia_dir = {
    main = {
        title = 'encyclopedia',
		generate = function()
			if Encyclopedia.AddedInputCallback then
				Encyclopedia.AddedInputCallback = false
			end
			Encyclopedia.KeyboardIndex = nil
			Encyclopedia.SearchText = ""
		end,
        buttons = {
            {str = 'resume game', action = 'resume'},
            {str = 'settings', dest = 'settings'},
            {str = '', fsize = 2, nosel = true},
			{str = 'inventory', dest = 'status'},
			{str = 'find in room', dest = 'room'},
            {str = '', fsize = 2, nosel = true},
			{str = 'items', dest = 'items'},
			{str = 'trinkets', dest = 'trinkets'},
			{str = 'pocket items', dest = 'pocketitems'},
            {str = '', fsize = 2, nosel = true},
			{str = 'characters', dest = REPENTANCE and 'characters' or 'characters_a'},
		--	{str = 'enemies', dest = 'enemies'},
		--	{str = 'bosses', dest = 'bosses'},
            {str = '', fsize = 2, nosel = true},
			{str = 'credits', dest = 'credits'},
            {str = '', fsize = 2, nosel = true},
        },
        tooltip = menuOpenToolTip
    },
    settings = {
        title = 'settings',
        buttons = {
            gamepadToggleButton,
            paletteButton,
            startPopupButton,
			{
				str = "Wiki Alignment",
                choices = {'left', 'center', 'right'},
                variable = "WikiAlignment",
                setting = 1,
                load = function()
                    return Encyclopedia.WikiAlignment
                end,
                store = function(var)
                    Encyclopedia.WikiAlignment = var
                end,
				changefunc = function(button)
					Encyclopedia.WikiAlignment = button.setting
				end,
            },
			{
				str = "Autogeneration",
                choices = {'enable', 'disable'},
                variable = "AutogenerateDescs",
                setting = 1,
                load = function()
                    return Encyclopedia.AutogenerateDescs
                end,
                store = function(var)
                    Encyclopedia.AutogenerateDescs = var
                end,
                changefunc = function(button)
                    Encyclopedia.AutogenerateDescs = button.setting
                end,
				tooltip = {strset = {"autogenerate", "mod items", "", "relaunch game", "to delete", "autogenerated", "items"}}
            },
        },
        tooltip = menuOpenToolTip
    },
	status = {
		title = "current status",
		generate = function()
			if Encyclopedia.AddedInputCallback then
				Encyclopedia.AddedInputCallback = false
			end
			Encyclopedia.KeyboardIndex = nil
			Encyclopedia.SearchText = ""
			
			Encyclopedia.AutogenerateUnknownItems()
			Encyclopedia.UpdatePlayerStatus()
		end,
		update = function(item, tbl)
			if #Encyclopedia.statusTable == 0 and Encyclopedia.SearchText == "" then
				Encyclopedia.RenderText({str = "Your items will be displayed here", fsize = 1, pos = Vector(-44, 0)})
			else
				Encyclopedia.UseStatusSearchBox()
				Encyclopedia.RenderStatus()
			end
		end,
	},
	room = {
		title = "items in room",
		generate = function()
			if Encyclopedia.AddedInputCallback then
				Encyclopedia.AddedInputCallback = false
			end
			Encyclopedia.KeyboardIndex = nil
			Encyclopedia.SearchText = ""
			
			Encyclopedia.AutogenerateUnknownItems()
			Encyclopedia.UpdateRoomItems()
		end,
		update = function(item, tbl)
			if #Encyclopedia.roomItemsTable == 0 and Encyclopedia.SearchText == "" then
				Encyclopedia.RenderText({str = "Room items will be displayed here", fsize = 1, pos = Vector(-44, 0)})
			else
				Encyclopedia.UseRoomItemsSearchBox()
				Encyclopedia.RenderRoomItems()
			end
		end,
	},
	items = {
		title = "item collection",
		buttons = {},
		generate = function()
			if Encyclopedia.itemsTable.Total > 1 then
				if Encyclopedia.AddedInputCallback then
					Encyclopedia.AddedInputCallback = false
				end
				Encyclopedia.KeyboardIndex = nil
				Encyclopedia.SearchText = ""
			end
			Encyclopedia.AutogenerateUnknownItems()
		end,
		update = function(item, tbl)
			if Encyclopedia.itemsTable.Total == 1 then
				Encyclopedia.UseItemsSearchBox("items", "vanilla")
				Encyclopedia.UseItemsSorters("items", "vanilla")
				Encyclopedia.RenderItems("items", "vanilla")
			end
		end,
	},
	itemssub = {},
	trinkets = {
		title = "trinket collection",
		buttons = {},
		generate = function()
			if Encyclopedia.trinketsTable.Total > 1 then
				if Encyclopedia.AddedInputCallback then
					Encyclopedia.AddedInputCallback = false
				end
				Encyclopedia.KeyboardIndex = nil
				Encyclopedia.SearchText = ""
			end
			Encyclopedia.AutogenerateUnknownItems()
		end,
		update = function(item, tbl)
			if Encyclopedia.trinketsTable.Total == 1 then
				Encyclopedia.UseItemsSearchBox("trinkets", "vanilla")
				Encyclopedia.UseItemsSorters("trinkets", "vanilla")
				Encyclopedia.RenderItems("trinkets", "vanilla")
			end
		end,
	},
	trinketssub = {},
	pocketitems = {
		title = "pocket items",
		buttons = {
			{str = "cards", dest = "cards"},
			{str = "runes", dest = "runes"},
			REPENTANCE and {str = "souls", dest = "souls"} or nil,
			{str = "pills", dest = "pills"},
		},
		generate = function()
			if Encyclopedia.AddedInputCallback then
				Encyclopedia.AddedInputCallback = false
			end
			Encyclopedia.KeyboardIndex = nil
			Encyclopedia.SearchText = ""
		end,
        tooltip = menuOpenToolTip
	},
	pocketitemssub = {},
	cards = {
		title = "cards collection",
		buttons = {},
		generate = function()
			if Encyclopedia.pocketitemsTable.cards.Total > 1 then
				if Encyclopedia.AddedInputCallback then
					Encyclopedia.AddedInputCallback = false
				end
				Encyclopedia.KeyboardIndex = nil
				Encyclopedia.SearchText = ""
			end
			Encyclopedia.AutogenerateUnknownItems()
		end,
		update = function(item, tbl)
			if Encyclopedia.pocketitemsTable.cards.Total == 1 then
				Encyclopedia.UsePocketitemsSearchBox("cards", "vanilla")
				Encyclopedia.UsePocketitemsSorters("cards", "vanilla")
				Encyclopedia.RenderPocketitems("cards", "vanilla")
			end
		end,
	},
	runes = {
		title = "runes collection",
		buttons = {},
		generate = function()
			if Encyclopedia.pocketitemsTable.runes.Total > 1 then
				if Encyclopedia.AddedInputCallback then
					Encyclopedia.AddedInputCallback = false
				end
				Encyclopedia.KeyboardIndex = nil
				Encyclopedia.SearchText = ""
			end
			Encyclopedia.AutogenerateUnknownItems()
		end,
		update = function(item, tbl)
			if Encyclopedia.pocketitemsTable.runes.Total == 1 then
				Encyclopedia.UsePocketitemsSearchBox("runes", "vanilla")
				Encyclopedia.UsePocketitemsSorters("runes", "vanilla")
				Encyclopedia.RenderPocketitems("runes", "vanilla")
			end
		end,
	},
	souls = {
		title = "souls collection",
		buttons = {},
		generate = function()
			if Encyclopedia.pocketitemsTable.souls.Total > 1 then
				if Encyclopedia.AddedInputCallback then
					Encyclopedia.AddedInputCallback = false
				end
				Encyclopedia.KeyboardIndex = nil
				Encyclopedia.SearchText = ""
			end
			Encyclopedia.AutogenerateUnknownItems()
		end,
		update = function(item, tbl)
			if Encyclopedia.pocketitemsTable.souls.Total == 1 then
				Encyclopedia.UsePocketitemsSearchBox("souls", "vanilla")
				Encyclopedia.UsePocketitemsSorters("souls", "vanilla")
				Encyclopedia.RenderPocketitems("souls", "vanilla")
			end
		end,
	},
	pills = {
		title = "pills collection",
		buttons = {},
		generate = function()
			if Encyclopedia.pocketitemsTable.pills.Total > 1 then
				if Encyclopedia.AddedInputCallback then
					Encyclopedia.AddedInputCallback = false
				end
				Encyclopedia.KeyboardIndex = nil
				Encyclopedia.SearchText = ""
			end
			Encyclopedia.AutogenerateUnknownItems()
		end,
		update = function(item, tbl)
			if Encyclopedia.pocketitemsTable.pills.Total == 1 then
				Encyclopedia.UsePocketitemsSearchBox("pills", "vanilla")
				Encyclopedia.UsePocketitemsSorters("pills", "vanilla")
				Encyclopedia.RenderPocketitems("pills", "vanilla")
			end
		end,
	},
	
	characters = {
		title = "characters",
		buttons = {
			{str = "normal", dest = "characters_a"},
			REPENTANCE and {str = "tainted", dest = "characters_b"} or nil,
		},
		generate = function()
			if Encyclopedia.AddedInputCallback then
				Encyclopedia.AddedInputCallback = false
			end
			Encyclopedia.KeyboardIndex = nil
			Encyclopedia.SearchText = ""
			
			Encyclopedia.AutogenerateUnknownCharacters()
		end,
        tooltip = menuOpenToolTip
	},
	characters_a = {
		title = "characters",
		buttons = {},
		generate = function()
			if Encyclopedia.characters_aTable.Total > 1 then
				if Encyclopedia.AddedInputCallback then
					Encyclopedia.AddedInputCallback = false
				end
				Encyclopedia.KeyboardIndex = nil
				Encyclopedia.SearchText = ""
			end
			Encyclopedia.AutogenerateUnknownCharacters()
		end,
		update = function(item, tbl)
			if Encyclopedia.characters_aTable.Total == 1 then
				Encyclopedia.RenderCharacters("characters_a", "vanilla")
			end
		end,
	},
	characters_b = {
		title = "characters (b)",
		buttons = {},
		generate = function()
			if Encyclopedia.characters_bTable.Total > 1 then
				if Encyclopedia.AddedInputCallback then
					Encyclopedia.AddedInputCallback = false
				end
				Encyclopedia.KeyboardIndex = nil
				Encyclopedia.SearchText = ""
			end
			Encyclopedia.AutogenerateUnknownCharacters()
		end,
		update = function(item, tbl)
			if Encyclopedia.characters_bTable.Total == 1 then
				Encyclopedia.RenderCharacters("characters_b", "vanilla")
			end
		end,
	},
	characterssub = {},
	
	enemies = {
		title = "enemy collection",
	},
	bosses = {
		title = "boss collection",
	},
	credits = {
        title = 'credits',
        fsize = 1,
        buttons = {
			{str = "dev team", nosel = true}
		}
    },
}

if not REPENTANCE then
	Encyclopedia.Encylopedia_dir.souls = nil
	table.remove(Encyclopedia.Encylopedia_dir.pocketitems.buttons, 3)
end

local credits = {
    {"agentcucco", "coder", tooltip = {"I had to", "learn python", "for this.", "", "p a i n"}},
    {"surrealdude", "artist", tooltip = {"h"}},
    {"ryon", "wiki reviewer", tooltip = {"does anyone", "know if", "modding is", "enabled yet"}},
    {"connor", "coder"},
    "",
    "open source buddies",
    {"kittenchilly", "structure polish"},
    "",
    "special thanks to",
    "",
    {"wofsauge", "EID Integration"},
    {"deadinfinity", "DSS' API guidance"},
    {"piber20", "Overwriting the API"},
    {"jsg", "support"},
    {"kataga", "Reviewing descs"},
	"",
    "A big part of the Isaac community",
	"for maintaining the Wiki!!!",
	"",
}

for _, credit in ipairs(credits) do
    if type(credit) == "string" then
        Encyclopedia.Encylopedia_dir.credits.buttons[#Encyclopedia.Encylopedia_dir.credits.buttons + 1] = {str = credit, nosel = true}
    else
        for i, part in ipairs(credit) do
            if i ~= 1 then
                if i == 2 then
                    local button = {strpair = {{str = credit[1]}, {str = part}}}
                    if credit.tooltip then
                        if type(credit.tooltip) == "string" then
                            credit.tooltip = {credit.tooltip}
                        end
                        button.tooltip = {strset = credit.tooltip}
                    end
                    Encyclopedia.Encylopedia_dir.credits.buttons[#Encyclopedia.Encylopedia_dir.credits.buttons + 1] = button
                else
                    Encyclopedia.Encylopedia_dir.credits.buttons[#Encyclopedia.Encylopedia_dir.credits.buttons + 1] = {strpair = {{str = ''}, {str = part}}, nosel = true}
                end
            end
        end
    end
end

local Encyclopediadirectorykey = {
    Item = Encyclopedia.Encylopedia_dir.main,
    Main = 'main',
    Idle = false,
    MaskAlpha = 1,
    Settings = {},
    SettingsChanged = false,
    Path = {},
}

dssmenu.AddMenu("Encylopedia", {
	Run = dssmod.runMenu,
	Open = dssmod.openMenu,
	Close = dssmod.closeMenu,
	Directory = Encyclopedia.Encylopedia_dir,
	DirectoryKey = Encyclopediadirectorykey
})

local LastModName = "unknown mod"
local LastModRoot = ""
local LastItem = CollectibleType.NUM_COLLECTIBLES
local LastTrinket = TrinketType.NUM_TRINKETS
local LastCard = Card.NUM_CARDS
local LastPill = PillEffect.NUM_PILL_EFFECTS

local registerModOld = Isaac.RegisterMod
local function registerModNew(ref, modName, apiVersion)
	local registeredCorrectly, returned = pcall(registerModOld, ref, modName, apiVersion)
    
    if registeredCorrectly then
		local MaxCollectible = Encyclopedia.GetMaxCollectibleID()
		local MaxTrinket = Encyclopedia.GetMaxTrinketID()
		local MaxCard = Encyclopedia.GetMaxCardID()
		local MaxPill = Encyclopedia.GetMaxPillID()
		LastModName = modName
		LastModRoot = tostring(ref.path)
		
		if LastItem <= MaxCollectible then
			for i = LastItem, MaxCollectible do
				Encyclopedia.TrackerTables.itemsNames[i] = LastModName
			end
			LastItem = MaxCollectible + 1
		end
		
		if LastTrinket <= MaxTrinket then
			for i = LastTrinket, MaxTrinket do
				Encyclopedia.TrackerTables.trinketsNames[i] = LastModName
			end
			LastTrinket = MaxTrinket + 1
		end
		
		if LastCard <= MaxCard then
			for i = LastCard, MaxCard do
				local item = Encyclopedia.ItemConfig:GetCard(i)
				
				Encyclopedia.TrackerTables.cardsNames[i] = LastModName
				Encyclopedia.AutoCardSprites[i] = Encyclopedia.RegisterSprite(ref.path .. "content/gfx/ui_cardfronts.anm2", item.HudAnim)
			end
			LastCard = MaxCard + 1
		end
		
		if LastPill <= MaxPill then
			for i = LastPill, MaxPill do
				Encyclopedia.TrackerTables.pillsNames[i] = LastModName
			end
			LastPill = MaxPill + 1
		end
	
        return returned
    else
        error(returned, 2)
    end 
end
Isaac.RegisterMod = registerModNew

local getPlayerTypeByNameOld = Isaac.GetPlayerTypeByName
local function getPlayerTypeByNameNew(name, tainted)
	local registeredCorrectly, returned = pcall(getPlayerTypeByNameOld, name, tainted)
	
	if registeredCorrectly then
		if returned == -1 then return returned end
		
		if tainted then
			if not Encyclopedia.TrackerTables.characters_bNames[returned] then
				--Encyclopedia.TrackerTables.characters_bNames[returned] = {Path = LastModRoot, Mod = LastModName, Char = string.gsub(name, "Tainted ", "")}
				Encyclopedia.TrackerTables.characters_bNames[returned] = {Path = LastModRoot, Mod = LastModName, Char = name}
			end
		else
			if not Encyclopedia.TrackerTables.characters_aNames[returned] then
				Encyclopedia.TrackerTables.characters_aNames[returned] = {Path = LastModRoot, Mod = LastModName, Char = name}
			end
		end
		
		return returned
    else
        error(returned, 2)
	end
end
Isaac.GetPlayerTypeByName = getPlayerTypeByNameNew