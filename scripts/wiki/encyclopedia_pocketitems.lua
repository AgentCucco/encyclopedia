Encyclopedia.CardsWiki = {
	CARD_FOOL = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Teleports Isaac back to the starting room of a floor."},
		},
	},
	CARD_MAGICIAN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Grants homing tears for the current room."},
			{str = "[REP] Grants +3 range for the current room."},
		},
	},
	CARD_HIGH_PRIESTESS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's dismembered leg is called down to deal 300 damage to anything it stomps."},
			{str = "It will always stomp on the enemy with the most health."},
			{str = "If used in an empty room or during the fight with Mom, it will stomp Isaac instead."},
		},
	},
	CARD_EMPRESS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Triggers the Whore of Babylon effect (+1.5 damage, +0.3 speed) for the current room, but without the need for Isaac to have a red heart or less of health."},
		},
	},
	CARD_EMPEROR = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Teleports Isaac into the Boss Room of a floor."},
			{str = "[REP] In Home, this will act as a random teleport."},
		},
	},
	CARD_HIEROPHANT = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns 2 Soul Hearts."},
		},
	},
	CARD_LOVERS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns 2 Red Hearts."},
			{str = "If used in a Black Heart or Eternal Heart-containing Super Secret Room, it will spawn those types of hearts instead."},
		},
	},
	CARD_CHARIOT = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Grants the temporary invulnerability effect of My Little Unicorn."},
		},
	},
	CARD_JUSTICE = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns one of each consumable drop (a Red Heart, a Key, a Bomb, and a Penny)."},
			{str = "Hearts have a chance to spawn as either a Half Red Heart or a Double Heart."},
		},
	},
	CARD_HERMIT = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Teleports Isaac to the Shop."},
			{str = "If there is no Shop, this will act as a random teleport."},
			{str = "If the Shop has not yet been accessed, this card will unlock the door without requiring a key (as long as the player exits through the main entrance)."},
		},
	},
	CARD_WHEEL_OF_FORTUNE = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns an Arcade machine (Slot Machine or Fortune Telling Machine)."},
		},
	},
	CARD_STRENGTH = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Grants the effect of Magic Mushroom (increases all of Isaac's stats except tears)"},
			{str = "Adds one (temporary) Red Heart container for the current room as opposed to a permanent one."},
		},
	},
	CARD_HANGED_MAN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Removes Isaac's body, allowing him to fly around for the duration of the room (same effect as Transcendence)."},
		},
	},
	CARD_DEATH = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Deals 40 damage to all enemies in the room (same effect as The Necronomicon)."},
		},
	},
	CARD_TEMPERANCE = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a Blood Donation Machine."},
			{str = "[AB] Spawns a Devil Beggar in Greed mode."},
		},
	},
	CARD_DEVIL = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Grants Isaac a temporary +2 increase to damage for the current room (same effect as The Book of Belial)."},
		},
	},
	CARD_TOWER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns 6 Troll Bombs randomly around the room (same effect as the Anarchist Cookbook)."},
		},
	},
	CARD_STARS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Teleports Isaac to the Treasure Room."},
			{str = "If there is no Treasure Room, this will act as a random teleport."},
			{str = "Randomly chooses a Treasure Room on Curse of the Labyrinth."},
			{str = "If the Treasure Room has not yet been accessed, this card will unlock the door without requiring a key (as long as the player exits through the main entrance)."},
			{str = "[REP] Can also teleport Isaac to the Planetarium."},
		},
	},
	CARD_SUN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Restores Isaac's full health, deals 100 damage to all enemies in the room, and reveals the map for the current floor, except the Super Secret Room."},
			{str = "[REP] Removes Curse of Darkness for the current floor."},
		},
	},
	CARD_MOON = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Teleports the player to the Secret Room."},
			{str = "This card will open one random wall once inside the Secret Room, so you can leave even if you don't have any bombs available."},
			{str = "[AB] Does not work in Greed and Greedier mode."},
		},
	},
	CARD_JUDGEMENT = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a Beggar."},
		},
	},
	CARD_WORLD = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Reveals the entire floor, except for the Super Secret Room."},
			{str = "Has no effect if under the effect of Curse of the Lost or Amnesia."},
		},
	},
	CARD_CLUBS_2 = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Doubles your bombs."},
			{str = "If you have no bombs, gives you 2 instead."},
		},
	},
	CARD_DIAMONDS_2 = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Doubles your coins."},
			{str = "If you have no coins, gives you 2 instead."},
		},
	},
	CARD_SPADES_2 = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Doubles your keys."},
			{str = "If you have no keys, gives you 2 instead."},
		},
	},
	CARD_HEARTS_2 = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Doubles Isaac's current number of Red Hearts."},
			{str = "Does not affect Eternal Hearts, Soul Hearts, or Black Hearts."},
			{str = "Doesn't add new Red Heart containers, just fills empty ones."},
		},
	},
	CARD_ACE_OF_CLUBS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Turns all pickups and chests in the room into Bombs."},
			{str = "[REP] Turns all non-boss enemies in the room into Bombs."},
		},
	},
	CARD_ACE_OF_DIAMONDS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Turns all pickups and chests in the room into Pennies."},
			{str = "[REP] Turns all non-boss enemies in the room into Pennies."},
		},
	},
	CARD_ACE_OF_SPADES = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Turns all pickups and chests in the room into Keys."},
			{str = "[REP] Turns all non-boss enemies in the room into Keys."},
		},
	},
	CARD_ACE_OF_HEARTS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Turns all pickups and chests in the room into random heart pickups."},
			{str = "[REP] Turns all non-boss enemies in the room into random heart pickups."},
		},
	},
	CARD_JOKER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Teleports you to the Devil Room or Angel Room."},
		},
	},
	CARD_CHAOS = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Upon activation, Isaac throws the card in front of himself."},
			{str = "- The card is thrown in the direction Isaac's body is facing, meaning the card will be thrown in the direction Isaac is moving. If he is stationary, it will be thrown in the last direction that Isaac moved."},
			{str = "The thrown card kills anything in its path (including end-game bosses like ??? and otherwise indestructible entities like Stone Grimaces and Retractable Spikes) and gets stuck in the ground/wall after being thrown."},
			{str = "- The only exceptions to this are Delirium and The Beast."},
			{str = "The card can open doors as if a bomb had exploded them, although it can not open any locked doors or open the big door in the starting room of Dark Room and The Chest."},
			{str = "The card can extinguish fires and break rocks, potentially revealing Crawl Spaces."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Using it on multi-phase bosses like Isaac, ??? and The Lamb will instantly end the boss fight."},
			{str = "- Using it on Satan during his first form will end the fight, despite him having multiple forms and health bars. Using it on The Fallen instead of Satan himself will not end the fight."},
			{str = "-- However, using it on Mega Satan will only skip one phase."},
			{str = "- [-REP] It will only kill one phase of Hush unless you throw it from the bottom side. However, even if you throw it from the bottom, it is not guaranteed to kill both phases."},
			{str = "-- [REP] Only one phase of Hush can be skipped."},
			{str = "- [AB+] It will only kill one phase of Ultra Greed in Greedier mode."},
			{str = "- [REP] It will only kill one phase of Mother and Dogma."},
			{str = "To aim the card correctly, make sure to move in the direction that it needs to be thrown, shooting in a direction will not fire it in that direction."},
			{str = "Does not kill beggars or shopkeepers."},
			{str = "Can kill normally indestructible enemies like the Masks of Mask + Hearts (though this will not kill the heart), Hosts while they're invulnerable, the little chubs in Chubbers, and Death's Heads"},
			{str = "It can also be used to kill things typically not considered alive like Wall Huggers and Stone Grimaces."},
			{str = "The Chaos Card can also open Secret Rooms if you don't have a spare bomb."},
			{str = "If it is used against Scolex while he's jumping, it will not instantly kill him and will instead cut off the bottom of his tail, dealing some damage. Although his weak point will no longer be visible, he will still be vulnerable."},
			{str = "If used on Pin or The Frail segments can be cut off causing both bosses to lose length like Larry Jr.."},
			{str = "[REP] Great Gideon has a special death animation if killed using it, which also reveals a crawl space under him containing several chests and items."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Angelic Prism: If the Chaos Card passes through the prism, it will replicate the Chaos Card four times each in their respective colors."},
			{str = "Blank Card: Allows for an easy run by allowing unlimited usage of the card."},
			{str = "- Items such as Habit, 9 Volt, and Charged Baby give more opportunities to use the card since all these items decrease the charge time of Blank Card."},
			{str = "- The Battery allows Blank Card to be used twice in a room, allowing multi-phase bosses to be killed instantly."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The icon, name and functionality are all a reference to the Magic: The Gathering card Chaos Orb."},
			{str = "This card is famous for its unusual effect: the card is flipped from a height of over one foot above the board and all cards it touches as it lands are destroyed."},
		},
	},
	CARD_CREDIT = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Using the Credit Card in a Shop instantly removes the cost of all items in that shop, allowing Isaac to take all of them for free."},
			{str = "Using the Credit Card in a Devil Room or Black Market allows Isaac to take all the items without any health cost."},
			{str = "- Taking Devil Room items with the Credit Card does not prevent Angel Rooms from appearing."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Free items will remain free, even if Isaac leaves the current room."},
			{str = "Works with the Blank Card, allowing Isaac to purchase all items from every shop, if used correctly."},
			{str = "[AB] Using the Credit Card in a Shop prevents Restock from working in this shop only. However, Restock Machines will work, creating new items."},
			{str = "[REP] Can be used in the starting room of Dark Room, allowing all four items to be taken for free."},
		},
	},
	CARD_RULES = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Upon activation, displays one of the following messages:"},
			{str = "CHUB DISLIKES SMOKE!"},
			{str = "A PIECE OF PAPER IS YOUR GUIDE"},
			{str = "THE WALLS WILL HARDEN OVER TIME. TIME IS THE ESSENCE"},
			{str = "A DARK MARKET LIES UNDER YOUR FEET	"},
			{str = "SOME DOORS REQUIRE A BLESSING. CARRY THEM WITH YOU"},
			{str = "DENY HIS GIFTS TO ATTAIN YOUR REWARD"},
			{str = "SLEEPING GATEKEEPERS WILL NEED TO BE AWOKEN WITH A LOUD SOUND"},
			{str = "ROOMS MAY YIELD MORE THAN YOU EXPECT. EXPERIMENTATION IS KEY"},
			{str = "One of the following Seeds may also be displayed:"},
			{str = "BOOB TOOB"},
			{str = "BRWN SNKE"},
			{str = "B911 TCZL"},
			{str = "CAMO K1DD"},
			{str = "CAMO DROP"},
			{str = "CHAM P1ON"},
			{str = "CLST RPHO"},
			{str = "COCK FGHT"},
			{str = "COME BACK"},
			{str = "CONF ETTI"},
			{str = "DONT STOP"},
			{str = "DRAW KCAB"},
			{str = "DYSL EX1A"},
			{str = "FACE DOWN"},
			{str = "FART SNDS"},
			{str = "FREE 2PAY"},
			{str = "IMNO BODY"},
			{str = "GGGG GGGG"},
			{str = "HART BEAT"},
			{str = "ISAA AACE"},
			{str = "KEEP AWAY"},
			{str = "NICA LISY"},
			{str = "PAC1 F1SM"},
			{str = "SLOW 4ME2"},
			{str = "TARO TARJ"},
			{str = "THEG HOST"},
			{str = "XXXX XXZX"},
			{str = "8AJJ AASE"},
		},
	},
	CARD_HUMANITY = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Fills the entire room with Poop."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Poop will not fill tiles with obstacles, even if they have already been destroyed."},
			{str = "The process is not instant and the room will not get filled if Isaac leaves before it's done filling."},
			{str = "It is more beneficial to use the card in a large-sized room as it will create more Poop piles."},
			{str = "Using A Card Against Humanity in the Ultra Greedier fight will result in Golden Poops spawning after defeating the boss."},
			{str = "- [REP] Results in Yellow Poops, which only have normal drops."},
			{str = "Using A Card Against Humanity in the Ultra Greed fight can trap the Gapers after Key Coins appear, preventing Ultra Greed from attacking."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Hallowed Ground: Spawned Poops have a high chance to turn into White Poop."},
			{str = "Infested! pill: Every pile of Poop spawns a Blue Spider."},
			{str = "Leo/The Nail/Thunder Thighs/Tech X/Brimstone: Allows all the Poop to be easily destroyed."},
			{str = "Lil Larva: Every pile of Poop spawns a Blue Fly."},
			{str = "Midas' Touch: Spawned Poops have a great chance to turn into Golden Poops."},
			{str = "Petrified Poop: Greatly increases the chance of finding a pickup when destroying Poop. It's extremely likely to be obtained when using the card in larger rooms."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Brown Cap: All the Poop explodes in a chain reaction once one is destroyed."},
			{str = "Dr. Fetus/ Ipecac: If Isaac has no other means of destroying Poop than his explosive tears, he will have to hurt himself to clear out a path through the Poop unless he has acquired Pyromaniac or Host Hat."},
			{str = "[REP] Home: If used in the living room, the poop will cause the couch and TV to move erratically."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The item is a reference to Cards Against Humanity, a satirical card game whose name in turn references ''crimes against humanity''. Specifically, it refers to the way the first player is chosen: Whoever was the last to poop goes first."},
			{str = "In the pop-up (if not disabled) for unlocking this card, the text on it reads ''A room full of poop''."},
			{str = "- This actually contradicts the original card game, as black cards are prompts while the white cards are answers to the prompt."},
		},
	},
	CARD_SUICIDE_KING = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Kills Isaac and spawns 10 consumables and items around the location of use."},
			{str = "The spawned collectibles will come from the Item Pool related to the Room where Suicide King was used (e.g. Angel Room items if used in an Angel Room)."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The following items will not protect Isaac from dying: Isaac's Heart, Book of Shadows, Holy Mantle, Unicorn Stump, My Little Unicorn and Power Pill."},
			{str = "If Lazarus' Rags are used to resurrect, Isaac will only have one heart. This can be detrimental if used with more than one heart container."},
		},
		{ -- Uses
			{str = "Uses", fsize = 2, clr = 3, halign = 0},
			{str = "Since using Suicide King means death, Isaac will need to have a resurrection item to make a good use of it, such as 1up!, the Ankh, the Dead Cat, or being Lazarus."},
			{str = "- When combined with Blank Card and Dead Cat, the card can be used nine times (spawning a total of 90 consumables)."},
			{str = "Suicide King won't always drop a pedestal item, but often drops numerous Chests, meaning that using Suicide King on a Special Room or in the Dark Room/The Chest can be useful to get good items."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "When Suicide King causes a Game Over (i.e. is used with no revive items), a picture of the last cause of damage will appear on Isaac's last will as the cause of death."},
			{str = "The background color for the Suicide King achievement has the same color as all the Boss Rush unlocks, suggesting that the Suicide King card was originally intended to be unlocked via completing a Boss Rush, with Lazarus."},
			{str = "- This is further evidenced by the fact that Missing No., Lazarus' Boss Rush unlock has a Challenge background color. This implies that the Suicide King challenge originally unlocked Missing No."},
			{str = "The Suicide King is the king of hearts in a standard card deck. It is called this because the character on the card is seemingly thrusting his sword into his own head."},
			{str = "The card quote reinforces the idea proposed by Reddit user Jayborino. That the only way for Isaac to get out of the chest in the ''real world'', is to die in the game."},
			{str = "- This user was praised by Edmund himself for his article being ''mind blowingly accurate''."},
		},
	},
	CARD_GET_OUT_OF_JAIL = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Opens all doors in the current room, just like Dad's Key."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Can be used to open the door to the Mega Satan boss fight. The door will remain open after leaving the room."},
			{str = "Can be used during the fight in Boss Rush to leave early, but cannot be used to get into the Boss Rush room if the time limit has already passed."},
			{str = "On XL floors, can open the door from the first boss room to the next."},
			{str = "Does not open the door of a Challenge Room after the fight has started."},
			{str = "Does not open the door to ???."},
			{str = "[REP] Does not open the Strange Door."},
			{str = "[REP] Does not open the flesh door to the Mom's Heart in Mausoleum/Gehenna II."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "''Get out of Jail Free'' is a card found in the popular board game ''Monopoly'', allowing its user to escape jail early. The phrase has since spread and a ''Get out of Jail Free Card'' is also an idiomatic expression for something that would alleviate an undesirable situation."},
		},
	},
	CARD_QUESTIONMARK = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Replicates the effect of Isaac's activated collectible."},
			{str = "- [-REP] Single-use items, such as Pandora's Box, will not replicate but instead activate and disappear."},
			{str = "- [REP] Single-use items do not disappear after using ? Card, allowing for potential multiple uses of the items."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The ID of this card was 43 before the Afterbirth+ expansion."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Car Battery: Does not duplicate the effect of the active item, activates it only once."},
			{str = "Jar of Flies: Expends both the card and all charges with no additional benefit."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Blank Card: Teleports the player to the I AM ERROR room of the current floor. This will consume both the Blank Card and the? Card. If done on The Chest or Dark Room, will instead spawn an I AM ERROR shopkeeper."},
			{str = "Glowing Hour Glass: Since the card replicates the effect of moving back one room to the previous state, you keep the card having not used it as an item until the room later. This effectively gives you charge-free uses of the Glowing Hour Glass."},
			{str = "- [AB+] The card is no longer refunded."},
			{str = "[AB+] Tarot Cloth:? Card is used twice, giving the active item the same effect as if you had Car Battery."},
			{str = "[REP] Diplopia: Duplicates all pedestal items and pickups in the room, without expending Diplopia."},
			{str = "- If used with another ? Card dropped on the floor in the room, allows for infinite duplication."},
			{str = "[REP] Alabaster Box: Causes the Alabaster Box to pay out with two angel room items without destroying the item or depleting any of it's current changes."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The interaction with Blank Card presents a paradox, as Blank Card and? Card try to replicate the effects of each other. This would result in an endless loop, so a special case was put in."},
			{str = "The Blank Card interaction is one of the three reliable ways to reach the I AM ERROR room in game, alongside Teleport 2.0 in certain circumstances and Undefined."},
			{str = "[AB+] Before Booster Pack no.5, Glowing Hour Glass was limited to 5 uses, and uses of the? Card counted against that limit."},
		},
	},
	CARD_DICE_SHARD = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Upon use, activates the effects of The D6 and the D20 at the same time."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] Like runes, and despite having a different appearance, this item is counted as a card. Therefore, it will work with Blank Card as an effective combo."},
			{str = "[REP] This item cannot be mimicked by either Blank Card or Clear Rune, and Tarot Cloth has no effect on it."},
			{str = "[AB+] Dice Shard's ID was changed to 49 from 44."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The item's appearance is similar to The D6, but broken in half."},
		},
	},
	CARD_EMERGENCY_CONTACT = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns up to two of Mom's hands that fall down onto two random enemies in the room, dealing 40 damage."},
			{str = "- Enemies with more than 40 HP and bosses will be pinned to the ground for some time, unable to do anything until the hand releases them."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "When used on Hush, damage resistance is briefly disabled. This can be used to quickly deal large amounts of damage to it."},
			{str = "Grabs doors when used on Mom."},
			{str = "Isaac, ???, and first phase of Hush will not retaliate with tears while grabbed."},
			{str = "The card will have no effect if used in a room without enemies."},
			{str = "[REP] This item cannot be mimicked by either Blank Card or Clear Rune, and Tarot Cloth has no effect on it."},
		},
	},
	CARD_HOLY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Grants a one-time Holy Mantle effect."},
			{str = "- [-REP] This only applies for the current room."},
			{str = "- [REP] The effect lasts until damage is taken."},
			{str = "[-REP] Has a 25% chance to display the message ''You Feel Blessed!'' and spawn another Holy Card."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "[REP] Tainted Lost: Starts with this card. Additionally, Holy Cards are significantly more common when playing as Tainted Lost and can be found even if they have not been unlocked."},
			{str = "Blanket: Holy Card's effect takes priority over the shield in boss rooms."},
			{str = "Holy Mantle: Effect stacks with Holy Card."},
			{str = "Wooden Cross: Does not stack with Holy Card, both are treated as the same shield."},
		},
	},
	CARD_HUGE_GROWTH = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Makes Isaac large for the current room, increases damage by 7, increases range by 30, and allows Isaac to walk over obstacles to destroy them."},
			{str = "- [REP] Range is increased by 3."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "This effect doesn't stack if used multiple times in the same room."},
			{str = "To unlock, Isaac must have a constant or permanent growth applied through Magic Mushroom, XI - Strength, or One Makes You Large."},
			{str = "You can unlock it using Blank Card and XI - Strength, but as the Strength card isn't a permanent growth effect, you would need to grow 5 times in a single room."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Huge Growth has been added in Booster Pack no.1, after the initial release of the DLC."},
			{str = "Huge Growth is based on a Steam Workshop mod by aeiou."},
			{str = "Huge Growth is a reference to the card Giant Growth from Magic: The Gathering. The quote is a reference to the card Become Immense, which has a similar effect to Giant Growth, both making a creature larger."},
			{str = "Despite increasing size, Leo does not contribute towards unlocking Huge Growth."},
		},
	},
	CARD_ANCIENT_RECALL = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns 3 random cards."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Special ''cards'' like Dice Shard can be spawned, but runes cannot spawn."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Ancient Recall has been added in Booster Pack no.1, after the initial release of the DLC."},
			{str = "Ancient Recall is based on a Steam Workshop mod by aeiou."},
			{str = "Ancient Recall is a reference to the card Ancestral Recall from Magic: The Gathering."},
		},
	},
	CARD_ERA_WALK = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "For the current room, enemies are slowed, Isaac's speed is increased by 0.50, and his shot speed is decreased by 1.00."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The card's effect does not stack if it is used multiple times in the same room."},
			{str = "While the effect is active, Isaac will appear to have Stop Watch, although this is purely cosmetic."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Era Walk has been added in Booster Pack no.1, after the initial release of the DLC."},
			{str = "Era Walk is based on a Steam Workshop mod by aeiou."},
			{str = "Era Walk is a reference to the card Time Walk from Magic: The Gathering. The Pickup Quote is a reference to the card Savor the Moment which has a similar effect."},
			{str = "- Unlike Ancient Recall and Huge Growth, Era Walk's sprite is based on Time Walk's Vintage Masters reprint art instead of the original art."},
		},
	},
	CARD_REVERSE_FOOL = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Drops all hearts and pickups on the floor, leaving Isaac at half a heart."},
			{str = "Pickups may be spawned as items such as A Quarter and Boom!."},
		},
	},
	CARD_REVERSE_MAGICIAN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Grants a repelling blue aura for one minute, like The Soul but stronger and with a wider area of effect."},
		},
	},
	CARD_REVERSE_HIGH_PRIESTESS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's leg starts stomping down repeatedly for one minute, like holding Broken Shovel."},
		},
	},
	CARD_REVERSE_EMPRESS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Grants 2 temporary red heart containers and +1.35 tears for one minute, along with Magdalene's hairstyle. Does not stack."},
		},
	},
	CARD_REVERSE_EMPEROR = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Teleports Isaac into an extra Boss Room from the boss pool two floors deeper than the current floor."},
			{str = "The boss will drop a reward, but the room has no trapdoor so Isaac cannot enter the next floor."},
			{str = "The room has an exit door, which leads Isaac back to where he used IV - The Emperor?."},
			{str = "It has unique effect for some levels (needs more investigation):"},
			{str = "- For Blue Womb Isaac is teleported to special Blue Womb boss room with Monstro."},
			{str = "-- Killing this boss will count as Hush kill."},
			{str = "-- In addition to normal exit door ( when Isaac will use it, he will be teleported back to normal Hush fight ), there is exit for Sheol and Cathedral."},
		},
	},
	CARD_REVERSE_HIEROPHANT = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns 2 Bone Hearts."},
		},
	},
	CARD_REVERSE_LOVERS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a random item from the current room's item pool."},
			{str = "Turns 1 red heart container or 2 soul hearts into a broken heart, potentially fatally, no downside for any character who has no health bar, Tainted Keeper locked to 1 HP."},
		},
	},
	CARD_REVERSE_CHARIOT = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac becomes an invincible statue with extreme fire rate for 10 seconds."},
			{str = "The statue can be nudged slightly by moving and is affected by knockback."},
			{str = "Sometimes the statue looks like Edith."},
		},
	},
	CARD_REVERSE_JUSTICE = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns 2 to 4 Golden Chests."},
		},
	},
	CARD_REVERSE_HERMIT = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "''Sells'' items and pickups in the room for their usual shop price, spawning a matching amount of coins."},
			{str = "If there is nothing to sell, spawns a penny."},
		},
	},
	CARD_REVERSE_WHEEL_OF_FORTUNE = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Invokes a random dice room effect."},
		},
	},
	CARD_REVERSE_STRENGTH = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "All enemies in the room are weakened, becoming slow and take double damage."},
			{str = "The effect lasts up to one minute or until the room is left."},
		},
	},
	CARD_REVERSE_HANGED_MAN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac turns visually into Keeper for 30 seconds, with a triple shot, no damage bonus and -0.1 speed. Killed enemies drop random coins."},
		},
	},
	CARD_REVERSE_DEATH = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Invokes a Book of the Dead effect."},
		},
	},
	CARD_REVERSE_TEMPERANCE = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Forces Isaac to eat 5 random pills in quick succession."},
		},
	},
	CARD_REVERSE_DEVIL = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Invokes The Bible, giving Isaac flight, kills Mom and grants a Seraphim familiar for 30 seconds."},
		},
	},
	CARD_REVERSE_TOWER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns 6 clusters of random rocks and other obstacles."},
			{str = "Spawned clusters will often contain tinted rocks."},
			{str = "The pathways between exits can't be blocked."},
		},
	},
	CARD_REVERSE_STARS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Removes Isaac's oldest passive item and spawns 2 random items from the current room's item pool."},
			{str = "Doesn't spawn anything if Isaac has no items."},
			{str = "Ignores character starting items."},
		},
	},
	CARD_REVERSE_SUN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Gain Spirit of the Night and +1.5 damage for the current level."},
			{str = "Turns Isaac's red hearts into bone hearts, but they revert once the effect ends."},
			{str = "Also causes Curse of Darkness that cannot be dispelled."},
		},
	},
	CARD_REVERSE_MOON = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Teleports the player to the Ultra Secret Room."},
			{str = "The pathway back to the normal rooms will be made of red rooms."},
		},
	},
	CARD_REVERSE_JUDGEMENT = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a Restock Machine."},
		},
	},
	CARD_REVERSE_WORLD = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a trap door to a crawl space."},
		},
	},
	CARD_CRACKED_KEY = {
        { -- Effects
            {str = "Effects", fsize = 2, clr = 3, halign = 0},
            {str = "Acts as a single-use  Red Key, allowing Isaac to create new rooms by using the key next to a wall where there would be no conditions that could prevent a (secret) room from spawning there. This is indicated by walls that have red door outlines on them."},
            {str = "- These rooms are most commonly normal rooms, but have a chance to be various special rooms instead, including Devil Rooms, Angel Rooms, Shops, Treasure Rooms, or Sacrifice Rooms. The red rooms can be connected to Ultra Secret Rooms."},
        },
        { -- Notes
            {str = "Notes", fsize = 2, clr = 3, halign = 0},
            {str = "The Cracked Key's main purpose is to allow unlocking Tainted characters in  Home without having to rely on finding the Red Key."},
            {str = "- To create a Cracked Key (once unlocked), drop any trinket in a Boss Room or a Treasure Room on any floor before acquiring  Dad's Note, including the room you get Dad's Note in. After starting the Ascent by collecting Dad's Note, the trinket left behind will have become a Cracked Key in the corresponding room."},
            {str = "-- While trinkets are fairly common, there is always a chance one doesn't appear naturally. This chance can be avoided by clearing three boss rooms without taking damage, which is guaranteed to cause  Perfection to spawn."},
            {str = "-- Be careful about holding  Myosotis, as the trinkets left in boss rooms may instead be brought to the next floor, ruining your chance to get a cracked key, though you COULD just drop Myosotis instead."},
            {str = "Cracked Keys can also spawn in the same condition as any Card."},
            {str = "This item cannot be mimicked by either  Blank Card or  Clear Rune, and  Tarot Cloth has no effect on it."},
            {str = "Little Baggy will turn all Cracked Keys into pills instead, rendering them unavailable, even during the Ascent."},
            {str = "- This does not happen with  Starter Deck though, as a Cracked Key is considered a card by the game."},
            {str = "Using the Cracked Key in  Home as a Tainted character will spawn the item  Inner Child in the closet where the Tainted character would normally be found as long as Inner Child was unlocked before."},
            {str = "If found before the Ascent, the Cracked Key can serve as a secret room finder, as secret room entrances do not have red door outlines."},
            {str = "An easy way to obtain cracked keys for the Ascent is if you have found  Mom's Box, in which case you can produce a surplus of trinkets to leave in Boss rooms or Treasure rooms for later use."},
        },
    },
	CARD_QUEEN_OF_HEARTS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns 1-12 full red hearts."},
		},
	},
	CARD_WILD = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "When used, copies the effect of the most recently used pill, card, rune, soul stone or activated item."},
		},
	},
}

Encyclopedia.RunesWiki = {
	RUNE_HAGALAZ = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Destroys all breakable objects in the room."},
			{str = "Does not affect Key Blocks."},
			{str = "[REP] Does affect Key Blocks."},
		},
	},
	RUNE_JERA = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Clones all pickups and chests in a room (including Shop pickups, the clones can then be picked up for free)."},
			{str = "Cloned chests will contain exactly the same type of items: if the original contains a pickup, so will the clone, the same is true for collectibles."},
			{str = "Cannot clone other Jera runes and trinkets."},
		},
	},
	RUNE_EHWAZ = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Creates a trapdoor leading to the next floor."},
			{str = "Has an estimated 1/12 (8%) chance to spawn a door to a Crawl Space instead."},
		},
	},
	RUNE_DAGAZ = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Removes any curses from the current floor and grants a Soul Heart. (This will not remove Curse of the Labyrinth.)"},
		},
	},
	RUNE_ANSUZ = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Reveals the whole map, including the Secret Room and the Super Secret Room. (This will not remove Curse of the Lost.)"},
		},
	},
	RUNE_PERTHRO = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Rerolls all pedestal items in the room. (This is the same effect as The D6.)"},
		},
	},
	RUNE_BERKANO = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Summons 3 friendly Blue Flies and 3 friendly Blue Spiders."},
		},
	},
	RUNE_ALGIZ = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] Grants a shield which lasts for 30 seconds."},
			{str = "[REP] Grants a shield which lasts for 20 seconds."},
		},
	},
	RUNE_BLANK = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Grants a random rune effect."},
			{str = "[-REP] This excludes the Black Rune)."},
			{str = "Has a 25% chance to spawn another Blank Rune upon use."},
		},
	},
	RUNE_BLACK = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Deals 40 damage to all enemies in the room."},
			{str = "Consumes all pickups in the room and turns them into Blue Flies and Spiders."},
			{str = "Consumes all pedestal items in the room and turns them into random stat upgrades."},
			{str = "No effect on shop items."},
		},
	},
	RUNE_SHARD = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Activates a random, weak rune effect:"},
			{str = "- Algiz: Grants a 3 second shield."},
			{str = "- Ansuz: Reveals the map up to 3 rooms away."},
			{str = "- Berkano: Gives 1 blue fly and spider."},
			{str = "- Dagaz: Gives half a soul heart."},
			{str = "- Ehwaz: Makes a trapdoor."},
			{str = "- Hagalaz: Destroys some rocks."},
			{str = "- Jera: Duplicates one pickup."},
			{str = "- Perthro: Rerolls one item."},
		},
	},
}

Encyclopedia.SoulsWiki = {
	CARD_SOUL_ISAAC = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Rerolls the items in the room like The D6, but they cycle back to their original form after one second, repeating."},
			{str = "If more uses are stacked, an additional item per use will be cycled through."},
		},
	},
	CARD_SOUL_MAGDALENE = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Surrounds Isaac with a bubbling red aura for the current room."},
			{str = "All enemies killed drop red half hearts that disappear in 2 seconds."},
		},
	},
	CARD_SOUL_CAIN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Invokes Dad's Key, opening all existing doors in the room."},
			{str = "Additionally, creates a red room like the Red Key for every possible red exit."},
			{str = "Also opens the Strange Door on Depths II."},
		},
	},
	CARD_SOUL_JUDAS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Stops time for 3 seconds and invokes Dark Arts, turning Isaac into a phantom that can pass through enemies to paralyze them, then attack them for quickly increasing chained damage and damage bonuses."},
		},
	},
	CARD_SOUL_BLUEBABY = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Causes 8 poison farts that leave lingering brown creep, then plants a trail of 7 Butt Bombs in quick succession."},
			{str = "Standing on the creep gives +1.35 tears and +1 damage."},
		},
	},
	CARD_SOUL_EVE = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "14 Dead Bird familiars fly in and attack enemies."},
			{str = "They leave after all victims are dead or Isaac leaves the room."},
		},
	},
	CARD_SOUL_SAMSON = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac turns berserk (similar to Tainted Samson) for 10 seconds, gaining +0.4 speed, some tears and +3 flat damage along with his melee attack."},
		},
	},
	CARD_SOUL_AZAZEL = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Activates Mega Blast for 7.5 seconds."},
		},
	},
	CARD_SOUL_LAZARUS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac dies, and is immediately revived at half a heart remaining in the same room, with some invincibility time."},
			{str = "The soul stone is automatically used upon taking fatal damage, functioning as a minimal extra life."},
		},
	},
	CARD_SOUL_EDEN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Uses The D6 and D20."},
			{str = "The rerolled items use random pools, as if Chaos were active."},
		},
	},
	CARD_SOUL_LOST = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac ''dies'' and turns into a ghost (similar to The Lost) for the current room, with all The Lost's benefits and weaknesses."},
			{str = "He returns to normal upon leaving the room."},
			{str = "Allows you to take Devil Deals for free, however taking one will cause the other deals to vanish, much like The Lost."},
			{str = "Allows you to enter The Mausoleum door for free."},
			{str = "Cannot obtain Bad Trip or Health Down pills in The Lost form."},
		},
	},
	CARD_SOUL_LILITH = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Adds one permanent familiar from the Baby Shop pool, except Stitches."},
			{str = "Unique familiars can be received only once."},
		},
	},
	CARD_SOUL_KEEPER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns around 2-25 random coins."},
		},
	},
	CARD_SOUL_APOLLYON = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns 15 random locusts."},
		},
	},
	CARD_SOUL_FORGOTTEN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns The Forgotten as a temporary secondary character, controlled at the same time as Isaac."},
			{str = "He dies if his two bone hearts run out, with no further consequence, and disappears when Isaac leaves the room."},
		},
	},
	CARD_SOUL_BETHANY = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns 6 Book of Virtues wisps with random properties."},
		},
	},
	CARD_SOUL_JACOB = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns Esau as a temporary secondary character, controlled at the same time as Isaac."},
			{str = "He dies if his health runs out, with no further consequence, and disappears when Isaac leaves the room."},
			{str = "Esau also has items equal to the number of items the player who spawned him has."},
		},
	},
}

Encyclopedia.PillsWiki = {
	PILLEFFECT_BAD_GAS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac farts, dealing 5 damage and poisoning enemies around him."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "The poison applies to the entire room."},
		},
	},
	PILLEFFECT_BAD_TRIP = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Injures Isaac by a full heart."},
			{str = "If a Bad Trip pill is used while Isaac only has a single heart left or less, the pill will turn into a Full Health pill instead."},
			{str = "[REP] This does not count as damage for the purposes of Devil/Angel Room generation odds or score reduction."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Two hearts of damage."},
			{str = "If Isaac would not survive, the pill becomes Full Health."},
		},
	},
	PILLEFFECT_BALLS_OF_STEEL = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Grants Isaac two soul hearts."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Grants four soul hearts."},
		},
	},
	PILLEFFECT_BOMBS_ARE_KEYS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Swaps the values of Isaac's bombs and keys."},
			{str = "[REP] The effects of Golden Bombs and Golden Keys are also swapped."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "The number of bombs and keys is increased by 50% while swapping."},
		},
	},
	PILLEFFECT_EXPLOSIVE_DIARRHEA = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns five active bombs behind Isaac over a period of five seconds, one per second."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns more bombs, boosted with Mr. Mega and Bobby-Bomb."},
		},
	},
	PILLEFFECT_FULL_HEALTH = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Refills every empty red heart container."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Fully heals and grants 3 soul hearts."},
		},
	},
	PILLEFFECT_HEALTH_DOWN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Removes one red heart container upon use."},
			{str = "While Isaac has one or fewer red heart containers, it will act as a Health Up pill instead."},
			{str = "??? can encounter Health Down pills, even though he can't have red heart containers."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Removes two heart containers."},
			{str = "If this is not possible, the pill becomes Health Up."},
		},
	},
	PILLEFFECT_HEALTH_UP = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Adds one empty red heart container upon use."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Adds two empty red heart containers."},
		},
	},
	PILLEFFECT_I_FOUND_PILLS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Changes the appearance of Isaac's face."},
			{str = "This pill is cosmetic only."},
			{str = "[-AB] The effect disappears when you leave the room."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac's face gets a permanent, very exaggerated expression."},
		},
	},
	PILLEFFECT_PUBERTY = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Causes Isaac to grow four hairs from the top of his head and zits on his face."},
			{str = "This pill is cosmetic only and will stay for the rest of the run."},
			{str = "Consuming three Puberty pills will grant the Adult transformation, granting one red heart container and making Isaac's voice slightly deeper (one time effect)."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac gains very fluffy hair covering his entire head and even face."},
		},
	},
	PILLEFFECT_PRETTY_FLY = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Grants a fly orbital, up to 3 maximum, that circles the player, blocking projectiles and damaging fly-type enemies on touch."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a Big Fan instead with no upper limit."},
		},
	},
	PILLEFFECT_RANGE_DOWN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] Decreases Isaac's range by 2.0."},
			{str = "[REP] Decreases Isaac's range by 0.60."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "-1.20 range."},
		},
	},
	PILLEFFECT_RANGE_UP = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] Increases Isaac's range by 2.5."},
			{str = "[REP] Increases Isaac's range by 0.75."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "+1.50 range."},
		},
	},
	PILLEFFECT_SPEED_DOWN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Decreases Isaac's movement speed by 0.12."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "	-0.24 speed."},
		},
	},
	PILLEFFECT_SPEED_UP = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Increases Isaac's movement speed by 0.15."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "+0.30 speed."},
		},
	},
	PILLEFFECT_TEARS_DOWN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Decreases Isaac's tears by 0.28."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "-0.56 tears."},
		},
	},
	PILLEFFECT_TEARS_UP = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Increases Isaac's tears by 0.35."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "+0.70 tears."},
		},
	},
	PILLEFFECT_LUCK_DOWN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Decreases Isaac's luck by 1."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "-2 luck."},
		},
	},
	PILLEFFECT_LUCK_UP = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Increases Isaac's luck by 1."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "+2 luck."},
		},
	},
	PILLEFFECT_TELEPILLS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac gets teleported to a random room."},
			{str = "Has a small chance to teleport Isaac to the I AM ERROR room, and an extremely small chance to teleport him to the Black Market, like Undefined"},
			{str = "[REP] Now has a chance to invoke the XVIII - The Moon? effect, teleporting the player to the Ultra Secret Room."},
			{str = "[-REP] Unobtainable in Greed Mode."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Unknown."},
		},
	},
	PILLEFFECT_48HOUR_ENERGY = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Drops one to two little batteries and fully charges the spacebar item."},
			{str = "Can be used with the Placebo or D1 to generate infinite batteries."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns more batteries."},
		},
	},
	PILLEFFECT_HEMATEMESIS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Empties all but one red heart container and spawns 1-4 red hearts."},
			{str = "If Isaac only has half of a heart left, it will heal him to one heart."},
			{str = "If used without any red heart containers, no health will be deducted, but red hearts will still spawn."},
			{str = "If used in a black heart or eternal heart-containing Super Secret Room, it will spawn those types of hearts instead."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns more hearts."},
		},
	},
	PILLEFFECT_PARALYSIS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Prevents Isaac from moving and shooting for 2 seconds."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "The effect lasts 4 seconds."},
		},
	},
	PILLEFFECT_SEE_FOREVER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Opens all entrances to the Secret Room and Super Secret Room for the current floor."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "In addition to the normal effect, fully reveals the map like The Mind."},
		},
	},
	PILLEFFECT_PHEROMONES = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Charms all enemies in the room for a short period of time."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "The charm is permanent, turning the enemies friendly."},
		},
	},
	PILLEFFECT_AMNESIA = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Replaces the floor map with a? for the rest of the floor."},
			{str = "If no other curses are active for the floor, the Curse of the Lost will appear under the level name when the map key is held."},
			{str = "This effect cannot be negated by Black Candle."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Unknown."},
		},
	},
	PILLEFFECT_LEMON_PARTY = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a large yellow pool (similar to Lemon Mishap) around your character."},
			{str = "Greatly damages any enemy that steps on it, for 22 damage per tick or 66 damage per second."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "The puddle covers nearly the entire room."},
		},
	},
	PILLEFFECT_WIZARD = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Tears are shot diagonally outwards for about 30 seconds."},
			{str = "This effect does not reset upon exiting the room."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "The effect lasts 60 seconds."},
		},
	},
	PILLEFFECT_PERCS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Gives The Wafer effect while in the room: reduces all damage taken to half a heart."},
			{str = "Unlocked by completing Challenge no.24: PAY TO PLAY."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Unknown."},
		},
	},
	PILLEFFECT_ADDICTED = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Makes Isaac take a full heart of damage from all sources for the current room"},
			{str = "Also changes his appearance for the room."},
			{str = "Unlocked by completing Challenge no.24: PAY TO PLAY."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Unknown."},
		},
	},
	PILLEFFECT_RELAX = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "For a short time, poop spawns wherever Isaac moves, like Explosive Diarrhea but with poop."},
			{str = "A higher move speed means you can touch more tiles in the same time."},
			{str = "A pun on laxatives."},
			{str = "[AB] Unlocked by completing Challenge no.25: Have a Heart."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "The effect lasts much longer, with high speed long enough to cover nearly the entire room."},
		},
	},
	PILLEFFECT_QUESTIONMARK = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Gives the Curse of the Maze effect for the remainder of the floor."},
			{str = "The effect can stack with other curses."},
			{str = "If no other curses are active for the floor, Curse of the Maze will appear under the level name when Tab is held."},
			{str = "This effect cannot be negated by Black Candle."},
			{str = "As the pill's name is ''???'', the same as what unidentified pills are referred to, it is easy to accidentally use it more than once per run"},
			{str = "It is recommended to memorize its color upon first using it."},
			{str = "Unlocked by completing Challenge no.25: Have a Heart."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Unknown."},
		},
	},
	PILLEFFECT_LARGER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Increases Isaac's size and decreases the pitch of his voice"},
			{str = "This effect does not increase the size of Isaac's hitbox but will increase the size of The Forgotten's bone club"},
			{str = "[AB+] These pills contribute to the Stompy transformation."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Size increases by two steps."},
		},
	},
	PILLEFFECT_SMALLER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Decreases Isaac's size and increases the pitch of his voice"},
			{str = "This effect decreases the size of Isaac's hitbox."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Size decreases by two steps."},
		},
	},
	PILLEFFECT_INFESTED_EXCLAMATION = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Destroys all poop in the room and spawns a single blue spider under Isaac and on each destroyed poop."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Double the number of spiders spawned."},
		},
	},
	PILLEFFECT_INFESTED_QUESTION = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns blue spiders equal to the number of enemies in the room."},
			{str = "If there are no enemies in the room, spawns 1-3 Blue Spiders."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Double the number of spiders spawned."},
		},
	},
	PILLEFFECT_POWER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Temporarily grants invulnerability, inflicts fear on enemies in the room, and lets Isaac deal contact damage, similar to The Gamekid."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "In addition to the normal effect, grants Added in Huge Growth for +7 flat damage and +3 range."},
			{str = "The Huge Growth bonus persists after the Gamekid wears off."},
		},
	},
	PILLEFFECT_RETRO_VISION = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] Pixelates the screen 3 times over 30 seconds."},
			{str = "[REP] Downscales all sprites for 30 seconds once."},
			{str = "The effect continues between floors."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Lasts 90 seconds, with no breaks."},
		},
	},
	PILLEFFECT_FRIENDS_TILL_THE_END = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns three blue flies."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Double the number of flies spawned."},
		},
	},
	PILLEFFECT_X_LAX = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Creates a pool of slippery brown creep underneath Isaac."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "The pool lasts much longer."},
		},
	},
	PILLEFFECT_IM_DROWSY = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] Slows all enemies for the duration of the room."},
			{str = "[REP] Slows all enemies, Isaac, and the background music for the duration of the room."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Unknown."},
		},
	},
	PILLEFFECT_SOMETHINGS_WRONG = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Creates a pool of black creep beneath Isaac that slows enemies."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "The pool lasts much longer."},
		},
	},
	PILLEFFECT_IM_EXCITED = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] Increases the speed of all enemies for the duration of the room."},
			{str = "[REP] Increases the speed of all enemies, Isaac, and the background music for the duration of the room."},
			{str = "The effect triggers again 30 seconds after use and 60 seconds after use."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Unknown."},
		},
	},
	PILLEFFECT_GULP = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Any trinkets that the player is holding are destroyed, and their effects are permanently applied to the player."},
			{str = "Unlocked by acquiring the Once More With Feeling! achievement."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Unknown."},
		},
	},
	PILLEFFECT_HORF = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac shoots an explosive tear with the Ipecac effect that always deals 200 damage."},
			{str = "Unlocked by acquiring the Dedication achievement."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac shoots a small cluster of Horf! tears, with varying power above normal Horf!."},
		},
	},
	PILLEFFECT_SUNSHINE = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Temporarily makes Isaac invincible, similar to Unicorn Stump."},
			{str = "Unlocked by acquiring the Sin Collector achievement."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "The effect is upgraded to The Gamekid."},
		},
	},
	PILLEFFECT_VURP = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Throws up the last pill used."},
			{str = "Will spawn itself if no other pills were taken beforehand."},
			{str = "Unlocked by acquiring the U Broke It! achievement."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "If the last pill was a normal pill, it becomes a horse pill."},
			{str = "No further difference on horse pills themselves."},
		},
	},
	PILLEFFECT_SHOT_SPEED_DOWN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Decreases Isaac's shot speed by 0.15."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "-0.30 shot speed."},
		},
	},
	PILLEFFECT_SHOT_SPEED_UP = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Increases Isaac's shot speed by 0.15."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "+0.30 shot speed."},
		},
	},
	PILLEFFECT_EXPERIMENTAL = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Increases one attribute and decreases another randomly, with the same power as normal attribute up/down pills."},
			{str = "If taken with PHD, it will not reduce an attribute, while if taken with False PHD, it will not increase one."},
			{str = "Effect (Horse)", fsize = 2, clr = 3, halign = 0},
			{str = "The two effects are doubled in power."},
		},
	},
}
